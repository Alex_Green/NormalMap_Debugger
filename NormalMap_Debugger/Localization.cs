﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace NormalMap_Debugger
{
    [XmlRoot(ElementName = "Localization")]
    public class LocalizationXML
    {
        public const string XmlFile = "Localization.xml";

        public static bool Save(LocalizationXML LocalizationXml, string FullPath)
        {
            try
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(FullPath, new XmlWriterSettings()
                { Indent = true, IndentChars = "\t", OmitXmlDeclaration = true }))
                {
                    #region Remove 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ...'
                    XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);
                    #endregion

                    XmlSerializer serializer = new XmlSerializer(typeof(LocalizationXML));
                    serializer.Serialize(xmlWriter, LocalizationXml, namespaces);
                    xmlWriter.Close();
                    return File.Exists(XmlFile);
                }
            }
            catch
            {
                return false;
            }
        }

        public static LocalizationXML Load(string XmlFile)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(LocalizationXML));
            if (File.Exists(XmlFile)) //If file exist
            {
                try
                {
                    using (FileStream LocalizationFile = new FileStream(XmlFile, FileMode.Open))
                    {
                        LocalizationXML L = (LocalizationXML)serializer.Deserialize(LocalizationFile);
                        LocalizationFile.Close();
                        return L;
                    }
                }
                catch
                {
                    return null;
                }
            }

            return null;
        }

        #region Localization
        public _MenuButtons MenuButtons = new _MenuButtons();
        public class _MenuButtons
        {
            public string
                Open = "Open",
                SaveAs = "Save As...",
                Undo = "Undo",
                Redo = "Redo",
                Copy = "Copy",
                Paste = "Paste",
                InvertR = "Invert Red channel: X",
                InvertG = "Invert Green channel: Y",
                InvertB = "Invert Blue channel: Z",
                Normalize = "Normalize",
                RestoreB = "Restore Blue channel",
                ClearB = "Clear Blue channel",
                FlipRG = "Flip Red and Green channels",
                FlipGB = "Flip Green and Blue channels",
                FlipBR = "Flip Blue and Red channels",
                Rotate270 = "Rotate normal map -90°",
                Rotate90 = "Rotate normal map +90°",
                Rotate180 = "Rotate normal map 180°",
                FlipHorizontally = "Flip normal map horizontally",
                FlipVertically = "Flip normal map vertically",
                Help = "About...";
        }

        public _Info Info = new _Info();
        public class _Info
        {
            public string
                CheckBox_IgnoreBlueChannel = "Ignore B-Channel",
                Label_Info = "Info",
                Label_Length = "Length",
                RadioButton_BadPixels = "Bad Pixels";
        }

        public _NormalMapGeneration NormalMapGeneration = new _NormalMapGeneration();
        public class _NormalMapGeneration
        {
            public string
                TabPage_GenerateNormalMap = "Generate NormalMap",
                CheckBox_GenInvX = "Invert X",
                CheckBox_GenInvY = "Invert Y",
                CheckBox_GenNormalize = "Normalize",
                Button_Generate = "Generate",
                Slider_Kernel = "Kernel",
                Slider_Scale = "Scale";
        }

        public _Settings Settings = new _Settings();
        public class _Settings
        {
            public string
                TabPage_Settings = "Settings",
                Label_SettingsInterpolationMode = "Viewer Interpolation Mode:",
                Button_SettingsSave = "Save";
        }
        #endregion
    }
}
