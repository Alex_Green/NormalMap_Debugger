﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using FreeImageAPI;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.Diagnostics;
using System.Threading;

namespace NormalMap_Debugger
{
    public partial class Form1 : Form
    {
        string StartupPath;
        SettingsXML Settings = new SettingsXML();
        LocalizationXML Localization = new LocalizationXML();
        string Form1TextDefault = "NormalMap Debugger v{0}.{1}";
        Mat NormalMap, NormalMap_Alpha;
        Color NMapColor = Color.FromArgb(128, 128, 255);
        WayBack<Mat> Wayback;

        public Form1()
        {
            InitializeComponent();
            Assembly Exe = Assembly.GetExecutingAssembly();
            StartupPath = Path.GetDirectoryName(Exe.Location);

            Form1TextDefault = String.Format(Form1TextDefault, Exe.GetName().Version.Major, Exe.GetName().Version.Minor);
            base.Text = Form1TextDefault;

            comboBoxSettingsInterpolationMode.DataSource = Array.FindAll((InterpolationMode[])Enum.GetValues(typeof(InterpolationMode)), (InterpolationMode IM) => { return IM != InterpolationMode.Invalid; });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NormalMapBMP = Properties.Resources.TestNormalMap;

            #region Load and Set Settings
            Settings = SettingsXML.Load(Path.Combine(StartupPath, SettingsXML.XmlFile));
            if (Settings == null)
            {
                Settings = new SettingsXML();
                MessageBox.Show("Error loading settings!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            WindowState = Settings.WindowState;
            radioButtonSettingsPosG.Checked = Settings.PositiveGreenChannel;
            comboBoxSettingsInterpolationMode.SelectedItem = Settings.Graphics.NormalMap.InterpolationMode;

            Wayback = new WayBack<Mat>(Settings.UndoLevels, toolStripButtonUndo, toolStripButtonRedo);
            #endregion

            #region Localization
            Localization = LocalizationXML.Load(Path.Combine(StartupPath, LocalizationXML.XmlFile));
            if (Localization == null)
            {
                buttonSaveLocalizationFile.Visible = true;
                Localization = new LocalizationXML();
            }

            toolStripButtonOpen.Text = Localization.MenuButtons.Open + toolStripButtonOpen.Text;
            toolStripButtonSaveAs.Text = Localization.MenuButtons.SaveAs + toolStripButtonSaveAs.Text;
            toolStripButtonUndo.Text = Localization.MenuButtons.Undo + toolStripButtonUndo.Text;
            toolStripButtonRedo.Text = Localization.MenuButtons.Redo + toolStripButtonRedo.Text;
            toolStripButtonCopy.Text = Localization.MenuButtons.Copy + toolStripButtonCopy.Text;
            toolStripButtonPaste.Text = Localization.MenuButtons.Paste + toolStripButtonPaste.Text;
            toolStripButtonInvR.Text = Localization.MenuButtons.InvertR + toolStripButtonInvR.Text;
            toolStripButtonInvG.Text = Localization.MenuButtons.InvertG + toolStripButtonInvG.Text;
            toolStripButtonInvB.Text = Localization.MenuButtons.InvertB + toolStripButtonInvB.Text;
            toolStripButtonNormalize.Text = Localization.MenuButtons.Normalize + toolStripButtonNormalize.Text;
            toolStripButtonRestoreB.Text = Localization.MenuButtons.RestoreB + toolStripButtonRestoreB.Text;
            toolStripButtonClearB.Text = Localization.MenuButtons.ClearB + toolStripButtonClearB.Text;
            toolStripButtonFlipRG.Text = Localization.MenuButtons.FlipRG + toolStripButtonFlipRG.Text;
            toolStripButtonFlipGB.Text = Localization.MenuButtons.FlipGB + toolStripButtonFlipGB.Text;
            toolStripButtonFlipBR.Text = Localization.MenuButtons.FlipBR + toolStripButtonFlipBR.Text;
            toolStripButtonRotate270.Text = Localization.MenuButtons.Rotate270 + toolStripButtonRotate270.Text;
            toolStripButtonRotate90.Text = Localization.MenuButtons.Rotate90 + toolStripButtonRotate90.Text;
            toolStripButtonRotate180.Text = Localization.MenuButtons.Rotate180 + toolStripButtonRotate180.Text;
            toolStripButtonFlipH.Text = Localization.MenuButtons.FlipHorizontally + toolStripButtonFlipH.Text;
            toolStripButtonFlipV.Text = Localization.MenuButtons.FlipVertically + toolStripButtonFlipV.Text;
            toolStripButtonHelp.Text = Localization.MenuButtons.Help + toolStripButtonHelp.Text;

            checkBox_IgnoreB.Text = Localization.Info.CheckBox_IgnoreBlueChannel;
            groupBoxInfo.Text = Localization.Info.Label_Info;
            radioButtonBadPixels.Text = Localization.Info.RadioButton_BadPixels;

            tabPageGenNMap.Text = Localization.NormalMapGeneration.TabPage_GenerateNormalMap;
            checkBoxGenInvX.Text = Localization.NormalMapGeneration.CheckBox_GenInvX;
            checkBoxGenInvY.Text = Localization.NormalMapGeneration.CheckBox_GenInvY;
            checkBoxGenNormalize.Text = Localization.NormalMapGeneration.CheckBox_GenNormalize;
            buttonGenerate.Text = Localization.NormalMapGeneration.Button_Generate;

            tabPageSettings.Text = Localization.Settings.TabPage_Settings;
            label_SettingsInterpolationMode.Text = Localization.Settings.Label_SettingsInterpolationMode;
            buttonSettingsSave.Text = Localization.Settings.Button_SettingsSave;
            #endregion

            #region SliderTrackbar List
            controlsListKernels.SetButtonAddFunc(() =>
            {
                SliderTrackbar SliderTrackbar1 = new SliderTrackbar();
                SliderTrackbar SliderTrackbar2 = new SliderTrackbar();
                SliderTrackbar1.Name = SliderTrackbar1.Text = nameof(SliderTrackbar1);
                SliderTrackbar2.Name = SliderTrackbar2.Text = nameof(SliderTrackbar2);
                SliderTrackbar1.Dock = SliderTrackbar2.Dock = DockStyle.Fill;
                SliderTrackbar1.Margin = SliderTrackbar2.Margin = Padding.Empty;

                SliderTrackbar1.Minimum = SliderTrackbar1.SliderMinimum = SliderTrackbar1.ValueDefault = 3;
                SliderTrackbar1.Maximum = 499;
                SliderTrackbar1.SliderMaximum = 199;
                SliderTrackbar1.Step = 2m;
                SliderTrackbar1.KeyboardInputStep = true;
                SliderTrackbar1.TextStringFormat = Localization.NormalMapGeneration.Slider_Kernel + ": {0:0}";
                SliderTrackbar1.TextInputStringFormat = "0";

                SliderTrackbar2.Minimum = SliderTrackbar2.SliderMinimum = 0m;
                SliderTrackbar2.Maximum = 100m;
                SliderTrackbar2.SliderMaximum = 5m;
                SliderTrackbar2.ValueDefault = 1m;
                SliderTrackbar2.Step = 0.1m;
                SliderTrackbar2.KeyboardInputStep = false;
                SliderTrackbar2.TextStringFormat = Localization.NormalMapGeneration.Slider_Scale + ": {0:0.##}";
                SliderTrackbar2.TextInputStringFormat = "0.##";

                TableLayoutPanel TLP = new TableLayoutPanel();
                TLP.Height = Math.Max(SliderTrackbar1.Height, SliderTrackbar2.Height);
                TLP.ColumnCount = 2;
                TLP.Margin = Padding.Empty;
                TLP.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
                TLP.Controls.Add(SliderTrackbar1, 0, 0);
                TLP.Controls.Add(SliderTrackbar2, 1, 0);
                return TLP;
            });

            int[] Kernels = new int[] { 1, 7, 15, 35 };
            double[] Scales = new double[] { 1.0, 1.1, 2.0, 2.0 };
            for (int i = 0; i < Math.Min(Kernels.Length, Scales.Length); i++)
            {
                controlsListKernels.AddControl();
                SliderTrackbar ST1 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[0];
                SliderTrackbar ST2 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[1];
                ST1.Value = ST1.ValueDefault = Kernels[i];
                ST2.Value = ST2.ValueDefault = (decimal)Scales[i];
            }
            #endregion

            OpenFirstImage(Program.Args);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Close();
                    break;
                case Keys.O:
                    if (e.Control && toolStripButtonOpen.Enabled)
                        toolStripButtonOpen_Click(sender, e);
                    break;
                case Keys.S:
                    if (e.Control && toolStripButtonSaveAs.Enabled)
                        toolStripButtonSaveAs_Click(sender, e);
                    break;
                case Keys.Z:
                    if (e.Control && toolStripButtonUndo.Enabled)
                        toolStripButtonUndo_Click(sender, e);
                    break;
                case Keys.Y:
                    if (e.Control && toolStripButtonRedo.Enabled)
                        toolStripButtonRedo_Click(sender, e);
                    break;
                case Keys.C:
                    if (e.Control && toolStripButtonCopy.Enabled)
                        toolStripButtonCopy_Click(sender, e);
                    break;
                case Keys.V:
                    if (e.Control && toolStripButtonPaste.Enabled)
                        toolStripButtonPaste_Click(sender, e);
                    break;
                case Keys.R:
                    if (e.Control && toolStripButtonInvR.Enabled)
                        toolStripButtonInvR_Click(sender, e);
                    break;
                case Keys.G:
                    if (e.Control && toolStripButtonInvG.Enabled)
                        toolStripButtonInvG_Click(sender, e);
                    break;
                case Keys.B:
                    if (e.Control && toolStripButtonInvB.Enabled)
                        toolStripButtonInvB_Click(sender, e);
                    if (e.Shift && toolStripButtonRestoreB.Enabled)
                        toolStripButtonRestoreB_Click(sender, e);
                    if (e.Alt && toolStripButtonClearB.Enabled)
                        toolStripButtonClearB_Click(sender, e);
                    break;
                case Keys.N:
                    if (e.Control && toolStripButtonNormalize.Enabled)
                        toolStripButtonNormalize_Click(sender, e);
                    break;
                case Keys.F1:
                    if (toolStripButtonHelp.Enabled)
                        toolStripButtonHelp_Click(sender, e);
                    break;
            }
        }

        void OpenFirstImage(string[] Args)
        {
            foreach (string Arg in Args)
                if (OpenImage(Arg))
                {
                    base.Text = Path.GetFileName(Arg);
                    break;
                }
        }

        bool OpenImage(string ImageFile)
        {
            if (!File.Exists(ImageFile))
                return false;

            try
            {
                NormalMapBMP = new Bitmap(ImageFile);
                Garbage.Clear();
                return true;
            }
            catch
            {
                try
                {
                    FREE_IMAGE_FORMAT format = FREE_IMAGE_FORMAT.FIF_UNKNOWN;
                    NormalMapBMP = FreeImage.LoadBitmap(ImageFile, FREE_IMAGE_LOAD_FLAGS.DEFAULT, ref format);
                    Garbage.Clear();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        void UpdateNormalMap(bool SaveZoom = false)
        {
            if (SaveZoom)
            {
                PointF ZoomCenter = pictureBoxZ1.ZoomCenterPoint;
                List<float> ZoomList = pictureBoxZ1.ZoomList;
                int ZoomListIndex = pictureBoxZ1.ZoomListIndex;

                pictureBoxZ1.Image = NormalMapBMP;

                pictureBoxZ1.ZoomList = ZoomList;
                pictureBoxZ1.ZoomListIndex = ZoomListIndex;
                pictureBoxZ1.ZoomCenterPoint = ZoomCenter;
                pictureBoxZ1.ZoomUpdate(true);
            }
            else
                pictureBoxZ1.Image = NormalMapBMP;
        }

        Bitmap NormalMapBMP
        {
            get
            {
                Mat[] MatBGR = new Mat[NormalMap.Channels() + (NormalMap_Alpha == null ? 0 : 1)];
                for (int i = 0; i < NormalMap.Channels(); i++)
                    MatBGR[i] = NormalMap.ExtractChannel(i);

                Mat Result;
                if (NormalMap_Alpha != null)
                {
                    Result = new Mat(NormalMap.Size(), MatType.CV_8UC4);
                    MatBGR[3] = NormalMap_Alpha.Clone();
                }
                else
                    Result = new Mat(NormalMap.Size(), MatType.CV_8UC3);

                Cv2.Merge(MatBGR, Result);
                Garbage.Clear(MatBGR); //CleanUp

                Bitmap BMP = BitmapConverter.ToBitmap(Result);
                Garbage.Clear(Result); //CleanUp
                return BMP;
            }
            set
            {
                Garbage.Clear(NormalMap);
                NormalMap = BitmapConverter.ToMat(value);

                Mat[] MatBGR = new Mat[3];
                for (int i = 0; i < MatBGR.Length; i++)
                    MatBGR[i] = NormalMap.ExtractChannel(i % NormalMap.Channels()); // " % NormalMap.Channels()" used for loading Gray images as RGB

                Garbage.Clear(NormalMap_Alpha); //CleanUp
                NormalMap_Alpha = (NormalMap.Channels() > 3 ? NormalMap.ExtractChannel(3) : null);

                Garbage.Clear(NormalMap);
                NormalMap = new Mat(MatBGR[0].Size(), MatType.CV_8UC3);
                Cv2.Merge(MatBGR, NormalMap);
                Garbage.Clear(MatBGR);  //CleanUp

                pictureBoxZ1.Image = NormalMapBMP;
            }
        }

        #region pictureBoxes
        private void pictureBoxZ1_MouseClickMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && pictureBoxZ1.BackgroundImage != null)
            {
                Bitmap BMP = (Bitmap)pictureBoxZ1.BackgroundImage;
                if (e.X >= 0 && e.Y >= 0 && e.X < BMP.Width && e.Y < BMP.Height)
                {
                    NMapColor = BMP.GetPixel(e.X, e.Y);
                    label_RGB.Text = String.Format("RGB: [{0}; {1}; {2}]", NMapColor.R, NMapColor.G, NMapColor.B);
                    pictureBox2.Invalidate();
                }
            }
            else if (e.Button == MouseButtons.XButton1)
            {
                pictureBoxZ1.ZoomUpdate(false);
            }
            else if (e.Button == MouseButtons.XButton2)
            {
                List<float> ZoomList = pictureBoxZ1.ZoomList;

                for (int i = 0; i < ZoomList.Count; i++)
                    if (ZoomList[i] == 1.0f)
                    {
                        pictureBoxZ1.ZoomListIndex = i;
                        pictureBoxZ1.ZoomUpdate(true);
                    }
            }
        }

        private void pictureBoxZ1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && (e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
                e.Effect = DragDropEffects.Move;
        }

        private void pictureBoxZ1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && e.Effect == DragDropEffects.Move)
                OpenFirstImage((string[])e.Data.GetData(DataFormats.FileDrop));
        }

        private void pictureBoxZ1_Paint(object sender, PaintEventArgs e)
        {
            if (!radioButtonRGB.Checked)
            {
                Mat MatBGR = BitmapConverter.ToMat((Bitmap)pictureBoxZ1.BackgroundImage);

                if (!radioButtonBadPixels.Checked) //R or G or B checked
                {
                    Channels Channel = Channels.R;
                    if (radioButtonR.Checked)
                        Channel = Channels.R;
                    else if (radioButtonG.Checked)
                        Channel = Channels.G;
                    else if (radioButtonB.Checked)
                        Channel = Channels.B;

                    MatBGR = OpenCV_Operation.SelectChannel(MatBGR, Channel);
                }
                else // BadPixels checked
                    MatBGR = OpenCV_Operation.GetBadPixels(MatBGR);

                e.Graphics.DrawImage(BitmapConverter.ToBitmap(MatBGR), 0, 0);
                MatBGR.Dispose();
            }
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            Graphics Gr = e.Graphics;
            Gr.CompositingQuality = Settings.Graphics.NormalVector.CompositingQuality;
            Gr.PixelOffsetMode = Settings.Graphics.NormalVector.PixelOffsetMode;
            Gr.SmoothingMode = Settings.Graphics.NormalVector.SmoothingMode;

            RectangleF Re = pictureBox2.ClientRectangle;
            Gr.FillRectangle(new SolidBrush(NMapColor), Re);
            Gr.FillEllipse(new SolidBrush(Settings.Graphics.NormalVector.BackgroundColor.ToColor()), Re);

            Vector3f V;
            if (radioButtonSettingsNegG.Checked)
                V = new Vector3f(NMapColor.R, NMapColor.G, NMapColor.B);
            else
                V = new Vector3f(NMapColor.R, 255f - NMapColor.G, NMapColor.B);

            V = V / 255f * 2f - 1f; // [-1; 1]

            if (checkBox_IgnoreB.Checked)
            {
                double ZZ = (double)(1.0f - V.X * V.X - V.Y * V.Y);
                if (ZZ >= 0)
                {
                    V.Z = (float)Math.Sqrt(ZZ);
                    label_Z.Text = "Z: " + V.Z.ToString(Settings.ToStringParameter);
                }
                else
                    label_Z.Text = "Z: Z < 0 !!!";

                label_Length.Text = Localization.Info.Label_Length + " ≡ 1.0";
            }
            else
            {
                label_Length.Text = Localization.Info.Label_Length + ": " + V.Length.ToString(Settings.ToStringParameter);
                label_Z.Text = "Z: " + V.Z.ToString(Settings.ToStringParameter);
            }

            label_X.Text = "X: " + V.X.ToString(Settings.ToStringParameter);
            label_Y.Text = "Y: " + V.Y.ToString(Settings.ToStringParameter);

            V.Normalize(); //Normalize vector

            // Get Yaw, Pitch
            float Yaw = (float)Math.Atan2(V.X, V.Z);
            float Pitch = (float)Math.Asin(V.Y);

            // Normalize Yaw, Pitch [-1, 1]
            // Must be "(float)Math.PI / 2f", but PI/4 most usable.
            Yaw /= (float)Math.PI / 4f;
            Pitch /= (float)Math.PI / 4f;

            float HalfW = Re.Width / 2f;
            float HalfH = Re.Height / 2f;
            PointF P1 = new PointF(HalfW, HalfH);
            PointF P2 = new PointF(HalfW + HalfW * Yaw, HalfH + HalfH * Pitch);

            LinearGradientBrush b = new LinearGradientBrush(P1, P2,
                Settings.Graphics.NormalVector.LineColor1.ToColor(),
                Settings.Graphics.NormalVector.LineColor2.ToColor());
            b.GammaCorrection = true;
            Pen p = new Pen(b, Settings.Graphics.NormalVector.LineWidth);
            p.SetLineCap(LineCap.RoundAnchor, LineCap.ArrowAnchor, DashCap.Flat);
            Gr.DrawLine(p, P1, P2);
        }
        #endregion

        private void radioButtonRGB_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxSettingsInterpolationMode_SelectedIndexChanged(sender, e);
            UpdateNormalMap(true);
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            buttonGenerate.Enabled = buttonGenApply.Enabled = false;
            pictureBoxZ2.Image = null;
            pictureBoxZ2.LoadingCircleEnabled = true;

            Thread T = new Thread((s) =>
            {
                #region Get Kernels, Scales
                List<int> Kernels = new List<int>();
                List<double> Scales = new List<double>();

                for (int i = 0; i < controlsListKernels.Controls.Count; i++)
                {
                    SliderTrackbar ST1 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[0];
                    SliderTrackbar ST2 = (SliderTrackbar)controlsListKernels.Elements[i].Controls[1];

                    if (ST1.Enabled && ST2.Enabled)
                    {
                        Kernels.Add((int)ST1.Value);
                        Scales.Add((double)ST2.Value);
                    }
                }
                #endregion

                int n = Kernels.Count - 1;

                bool InvX = checkBoxGenInvX.Checked;
                bool InvY = radioButtonSettingsNegG.Checked ^ checkBoxGenInvY.Checked;

                Mat NMap_Old = OpenCV_Operation.GenerateNormalMap(NormalMap, Kernels[n], Scales[n], InvX, InvY);

                // Normalize
                if (checkBoxGenNormalize.Checked)
                {
                    Mat NMap_Normalized = OpenCV_Operation.Normalize(NMap_Old);
                    Garbage.Clear(NMap_Old); //CleanUp
                    NMap_Old = NMap_Normalized;
                }

                if (n > 0)
                {
                    for (int i = n - 1; i >= 0; i--)
                    {
                        Mat NMap = OpenCV_Operation.GenerateNormalMap(NormalMap, Kernels[i], Scales[i], InvX, InvY);

                        // Normalize
                        if (checkBoxGenNormalize.Checked)
                            NMap = OpenCV_Operation.Normalize(NMap);

                        Mat NMap_Summ = OpenCV_Operation.Blend(NMap, NMap_Old, BlendModes.LinearLight);
                        Garbage.Clear(NMap, NMap_Old); //CleanUp
                        NMap_Old = OpenCV_Operation.Normalize(NMap_Summ);
                        Garbage.Clear(NMap_Summ); //CleanUp
                    }
                }

                pictureBoxZ2.Image = BitmapConverter.ToBitmap(NMap_Old);
                Garbage.Clear(NMap_Old); //CleanUp

                pictureBoxZ2.BeginInvoke((MethodInvoker)(() => pictureBoxZ2.LoadingCircleEnabled = false));
                buttonGenerate.BeginInvoke((MethodInvoker)(() => buttonGenerate.Enabled = true));
                buttonGenApply.BeginInvoke((MethodInvoker)(() => buttonGenApply.Enabled = true));
            });
            T.IsBackground = true;
            T.Start();
        }

        private void buttonGenApply_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = BitmapConverter.ToMat((Bitmap)pictureBoxZ2.Image);
            UpdateNormalMap(true);
            pictureBoxZ2.Image = null;
            Garbage.Clear(); //CleanUp
            buttonGenApply.Enabled = false;
        }

        private void comboBoxSettingsInterpolationMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Graphics.NormalMap.InterpolationMode = (InterpolationMode)comboBoxSettingsInterpolationMode.SelectedValue;

            if (radioButtonBadPixels.Checked) // BadPixels checked
                pictureBoxZ1.InterpolationMode = InterpolationMode.NearestNeighbor;
            else
                pictureBoxZ1.InterpolationMode = Settings.Graphics.NormalMap.InterpolationMode;

            pictureBoxZ2.InterpolationMode = Settings.Graphics.NormalMap.InterpolationMode;
        }

        private void buttonSettingsSave_Click(object sender, EventArgs e)
        {
            Settings.PositiveGreenChannel = radioButtonSettingsPosG.Checked;
            SettingsXML.Save(Settings, Path.Combine(StartupPath, SettingsXML.XmlFile));
        }

        private void buttonSaveLocalizationFile_Click(object sender, EventArgs e)
        {
            string XmlFile = Path.Combine(StartupPath, LocalizationXML.XmlFile);
            if (LocalizationXML.Save(Localization, XmlFile))
            {
                MessageBox.Show(String.Format("Done, saved to: \"{0}\"", XmlFile), "Done",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                buttonSaveLocalizationFile.Visible = false;
            }
            else
                MessageBox.Show("Localization file saving Error!!!", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #region toolStripButton (Menu)
        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = Path.GetFileName(openFileDialog1.FileName);
            if (openFileDialog1.ShowDialog() == DialogResult.OK && File.Exists(openFileDialog1.FileName))
            {
                if (OpenImage(openFileDialog1.FileName))
                {
                    Wayback.ClearStates();
                    saveFileDialog1.FileName = openFileDialog1.FileName;
                    Garbage.Clear();
                }
                else
                    MessageBox.Show("Maybe \"" + Path.GetFileName(openFileDialog1.FileName) + "\" is not supported image!",
                        "File opening error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButtonSaveAs_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = Path.GetFileNameWithoutExtension(saveFileDialog1.FileName);
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FREE_IMAGE_FORMAT ImageFormat = FREE_IMAGE_FORMAT.FIF_UNKNOWN;
                FREE_IMAGE_SAVE_FLAGS ImageSaveFlags = FREE_IMAGE_SAVE_FLAGS.DEFAULT;

                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        ImageFormat = FREE_IMAGE_FORMAT.FIF_BMP;
                        break;
                    case 2:
                        ImageFormat = FREE_IMAGE_FORMAT.FIF_JPEG;
                        ImageSaveFlags |=
                            FREE_IMAGE_SAVE_FLAGS.JPEG_BASELINE |
                            //FREE_IMAGE_SAVE_FLAGS.JPEG_OPTIMIZE |
                            FREE_IMAGE_SAVE_FLAGS.JPEG_QUALITYSUPERB;
                        break;
                    case 3:
                        ImageFormat = FREE_IMAGE_FORMAT.FIF_PNG;
                        ImageSaveFlags |= FREE_IMAGE_SAVE_FLAGS.PNG_Z_NO_COMPRESSION;
                        break;
                    case 4:
                        ImageFormat = FREE_IMAGE_FORMAT.FIF_TARGA;
                        break;
                    case 5:
                        ImageFormat = FREE_IMAGE_FORMAT.FIF_TIFF;
                        ImageSaveFlags |= FREE_IMAGE_SAVE_FLAGS.TIFF_NONE; //ImageSaveFlags |= FREE_IMAGE_SAVE_FLAGS.TIFF_DEFLATE;
                        break;
                }

                if (!FreeImage.SaveBitmap(NormalMapBMP, saveFileDialog1.FileName, ImageFormat, ImageSaveFlags))
                {
                    MessageBox.Show("Don't can save file: \"" + Path.GetFileName(saveFileDialog1.FileName) + "\".",
                        "File saving error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void toolStripButtonUndo_Click(object sender, EventArgs e)
        {
            NormalMap = Wayback.Undo(NormalMap.Clone()).Clone();
            UpdateNormalMap();
        }

        private void toolStripButtonRedo_Click(object sender, EventArgs e)
        {
            NormalMap = Wayback.Redo().Clone();
            UpdateNormalMap();
        }

        private void toolStripButtonCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetImage(NormalMapBMP);
        }

        private void toolStripButtonPaste_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsImage())
            {
                Wayback.AddState(NormalMap.Clone());
                NormalMapBMP = (Bitmap)Clipboard.GetImage();
            }
        }

        private void toolStripButtonInvR_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.Invert(NormalMap, Channels.R);
            UpdateNormalMap(true);
        }

        private void toolStripButtonInvG_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.Invert(NormalMap, Channels.G);
            UpdateNormalMap(true);
        }

        private void toolStripButtonInvB_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.Invert(NormalMap, Channels.B);
            UpdateNormalMap(true);
        }

        private void toolStripButtonNormalize_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.Normalize(NormalMap);
            UpdateNormalMap(true);
        }

        private void toolStripButtonRestoreB_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.RestoreBlueChannel(NormalMap);
            UpdateNormalMap(true);
        }

        private void toolStripButtonClearB_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.FillChannel(NormalMap, Channels.B, 255);
            UpdateNormalMap(true);
        }

        private void toolStripButtonFlipRG_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.SwapChannels(NormalMap, Channels.R, Channels.G);
            UpdateNormalMap(true);
        }

        private void toolStripButtonFlipGB_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.SwapChannels(NormalMap, Channels.G, Channels.B);
            UpdateNormalMap(true);
        }

        private void toolStripButtonFlipBR_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.SwapChannels(NormalMap, Channels.B, Channels.R);
            UpdateNormalMap(true);
        }

        private void toolStripButtonRotate270_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.FlipRotate(NormalMap, RotateFlipType.Rotate270FlipNone, radioButtonSettingsNegG.Checked);
            if (NormalMap_Alpha != null)
                NormalMap_Alpha = NormalMap_Alpha.Transpose().Flip(FlipMode.X);
            UpdateNormalMap(true);
        }

        private void toolStripButtonRotate90_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.FlipRotate(NormalMap, RotateFlipType.Rotate90FlipNone, radioButtonSettingsNegG.Checked);
            if (NormalMap_Alpha != null)
                NormalMap_Alpha = NormalMap_Alpha.Transpose().Flip(FlipMode.Y);
            UpdateNormalMap(true);
        }

        private void toolStripButtonRotate180_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.FlipRotate(NormalMap, RotateFlipType.Rotate180FlipNone);
            UpdateNormalMap(true);
        }

        private void toolStripButtonFlipH_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.FlipRotate(NormalMap, RotateFlipType.RotateNoneFlipX);
            UpdateNormalMap(true);
        }

        private void toolStripButtonFlipV_Click(object sender, EventArgs e)
        {
            Wayback.AddState(NormalMap.Clone());
            NormalMap = OpenCV_Operation.FlipRotate(NormalMap, RotateFlipType.RotateNoneFlipY);
            UpdateNormalMap(true);
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Form1TextDefault + "\n\n" +
                "Link: https://gitlab.com/Alex_Green/NormalMap_Debugger\n\n" +
                "License: GNU GPL\n" +
                "FreeImage.NET Library licenses are GNU GPL or FIPL\n" +
                "(GNU GPL used for this program)\n\n" +
                "For any questions write me: \"ZAA93@yandex.ru\"",
                "About...", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        #endregion
    }
}
