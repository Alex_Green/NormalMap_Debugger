﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace System.Windows.Forms
{
    [DefaultEvent("ValueChanged")]
    [DefaultProperty("Value")]
    [DefaultBindingProperty("Value")]
    public partial class SliderTrackbar : Control
    {
        // Controls
        public TextBox TextInput;

        public SliderTrackbar()
        {
            InitializeComponent();
            ApplySomeStyleSettings();
        }

        public SliderTrackbar(string Settings)
        {
            InitializeComponent();
            ApplySomeStyleSettings();
            ApplySettings(Settings);
        }

        private void InitializeComponent()
        {
            this.TextInput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // TextInput
            // 
            this.TextInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TextInput.Location = new System.Drawing.Point(0, 0);
            this.TextInput.Name = "TextInput";
            this.TextInput.Size = new System.Drawing.Size(100, 20);
            this.TextInput.TabIndex = 0;
            this.TextInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextInput.Visible = false;
            this.TextInput.FontChanged += new System.EventHandler(this.TextInput_TextResize);
            this.TextInput.TextChanged += new System.EventHandler(this.TextInput_TextResize);
            this.TextInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextInput_KeyDown);
            this.TextInput.Leave += new System.EventHandler(this.TextInput_Leave);
            // 
            // SliderTrackbar
            // 
            this.Controls.Add(this.TextInput);
            this.MinimumSize = new System.Drawing.Size(30, 15);
            this.Size = new System.Drawing.Size(100, 20);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void ApplySomeStyleSettings()
        {
            SetStyle(ControlStyles.Selectable, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            BackColor = Color.Transparent;
            DoubleBuffered = true;
        }

        /// <summary>
        /// Apply settings from string. Example:
        /// <para>TextColor = "255; 255; 0; 0"</para>
        /// <para>ArrayOfValuesEnabled = True</para>
        /// </summary>
        /// <param name="Settings">Settings in multiline string, like: "A=1 \n B="Text" \n C=3",
        /// without quotas, where "\n" - new line char.</param>
        /// <param name="Culture">Culture for strings parsing. Default (if equal "null")
        /// <see cref="CultureInfo.InvariantCulture"/>.</param>
        /// <returns></returns>
        public bool ApplySettings(string Settings, CultureInfo Culture = null)
        {
            bool ParsingError = false;

            if (Culture == null)
                Culture = CultureInfo.InvariantCulture;

            if (Settings == null)
                return false;

            Settings = Settings.Replace("\r", "").Replace("\t", "");
            string[] Lines = Settings.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < Lines.Length; i++)
            {
                Lines[i] = Lines[i].Trim();
                string[] PropVal = Lines[i].Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);

                if (PropVal.Length < 2)
                    continue;

                string Property = PropVal[0].Trim();
                string Value = PropVal[1];
                Value = Value.Trim().TrimStart(new char[] { '"' });
                if (Value.LastIndexOf('"') == Value.Length - 1)
                    Value = Value.Substring(0, Value.Length - 1);

                try
                {
                    PropertyInfo PropInfo = this.GetType().GetProperty(Property);
                    object Obj = Value;

                    if (PropInfo.PropertyType == typeof(bool)) //True, true, False, false
                        Obj = bool.Parse(Value);

                    else if (PropInfo.PropertyType == typeof(float))
                        Obj = float.Parse(Value, NumberStyles.Any, Culture);

                    else if (PropInfo.PropertyType == typeof(int))
                        Obj = int.Parse(Value, NumberStyles.Any, Culture);

                    else if (PropInfo.PropertyType == typeof(decimal))
                        Obj = decimal.Parse(Value, NumberStyles.Any, Culture);

                    else if (PropInfo.PropertyType.IsEnum) //Left | Right | Top
                    {
                        string[] SV = Value.Split(new char[] { '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        int SomeEnum = 0;
                        for (int j = 0; j < SV.Length; j++)
                            SomeEnum |= (int)Enum.Parse(PropInfo.PropertyType, SV[j], true);
                        Obj = SomeEnum;
                    }

                    else if (PropInfo.PropertyType == typeof(string[])) //"Str1{\n}Str2{\n}Str3"
                    {
                        Obj = Value.Replace(@"\r", "\r").Replace(@"\n", "\n").Replace(@"\t", "\t").Replace(@"\\", @"\")
                            .Replace("\\\"", "\"").Split(new string[] { "{\n}" }, StringSplitOptions.None);
                    }

                    else if (PropInfo.PropertyType == typeof(Color)) //"Red", "#FFFF0000", "255; 255; 0; 0"
                        Obj = ColorTranslator.FromHtml(Value);

                    #region Point
                    else if (PropInfo.PropertyType == typeof(Point)) //"1 2", "1, 2", "1; 2"
                    {
                        string[] SV = Value.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (SV.Length >= 2)
                        {
                            int X = int.Parse(SV[0], NumberStyles.Any, Culture);
                            int Y = int.Parse(SV[1], NumberStyles.Any, Culture);
                            Obj = new Point(X, Y);
                        }
                    }
                    #endregion

                    #region PointF
                    else if (PropInfo.PropertyType == typeof(PointF)) //"1 2", "1; 2"
                    {
                        string[] SV = Value.Split(new char[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (SV.Length >= 2)
                        {
                            float X = float.Parse(SV[0], NumberStyles.Any, Culture);
                            float Y = float.Parse(SV[1], NumberStyles.Any, Culture);
                            Obj = new PointF(X, Y);
                        }
                    }
                    #endregion

                    #region Size
                    else if (PropInfo.PropertyType == typeof(Size)) //"1 2", "1, 2", "1; 2"
                    {
                        string[] SV = Value.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (SV.Length >= 2)
                        {
                            int X = int.Parse(SV[0], NumberStyles.Any, Culture);
                            int Y = int.Parse(SV[1], NumberStyles.Any, Culture);
                            Obj = new Size(X, Y);
                        }
                    }
                    #endregion

                    #region SizeF
                    else if (PropInfo.PropertyType == typeof(SizeF)) //"1 2", "1; 2"
                    {
                        string[] SV = Value.Split(new char[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (SV.Length >= 2)
                        {
                            float X = float.Parse(SV[0], NumberStyles.Any, Culture);
                            float Y = float.Parse(SV[1], NumberStyles.Any, Culture);
                            Obj = new SizeF(X, Y);
                        }
                    }
                    #endregion

                    #region Padding
                    else if (PropInfo.PropertyType == typeof(Padding)) //"1", "1, 2, 3, 4", "1; 2; 3; 4"
                    {
                        string[] SV = Value.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);

                        if (SV.Length == 1)
                            Obj = new Padding(int.Parse(SV[0], NumberStyles.Any, Culture));
                        else if (SV.Length >= 4)
                        {
                            int L = int.Parse(SV[0], NumberStyles.Any, Culture);
                            int T = int.Parse(SV[1], NumberStyles.Any, Culture);
                            int R = int.Parse(SV[2], NumberStyles.Any, Culture);
                            int B = int.Parse(SV[3], NumberStyles.Any, Culture);
                            Obj = new Padding(L, T, R, B);
                        }
                    }
                    #endregion

                    #region Rectangle
                    else if (PropInfo.PropertyType == typeof(Rectangle)) //"1", "1, 2, 3, 4", "1; 2; 3; 4"
                    {
                        string[] SV = Value.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (SV.Length >= 4)
                        {
                            int X = int.Parse(SV[0], NumberStyles.Any, Culture);
                            int Y = int.Parse(SV[1], NumberStyles.Any, Culture);
                            int W = int.Parse(SV[2], NumberStyles.Any, Culture);
                            int H = int.Parse(SV[3], NumberStyles.Any, Culture);
                            Obj = new Rectangle(X, Y, W, H);
                        }
                    }
                    #endregion

                    #region List<PointF>
                    else if (PropInfo.PropertyType == typeof(List<PointF>)) //"0.1, 0.2, 0.3, 0.4"
                    {
                        List<PointF> L = new List<PointF>();
                        string[] SV = Value.Split(new char[] { ' ', '.', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < SV.Length - SV.Length % 2; j += 2)
                        {
                            float X, Y;
                            bool Err = float.TryParse(SV[j], out X);
                            Err |= float.TryParse(SV[j + 1], out Y);

                            if (!Err)
                                L.Add(new PointF(X, Y));
                        }
                        Obj = L;
                    }
                    #endregion

                    #region Font
                    else if (PropInfo.PropertyType == typeof(Font)) //"Arial; 8", "Arial; 14; Regular; Point"
                    {
                        string[] SV = Value.ToLower().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                        if (SV.Length >= 2)
                        {
                            string FontName = SV[0].Trim();

                            float FontSize;
                            FontSize = float.Parse(SV[1].Trim(), NumberStyles.Any, Culture);

                            int FStyle = (int)Font.Style;
                            if (SV.Length >= 3)
                            {
                                FStyle = 0;
                                var SV2 = SV[2].Trim().Split(new char[] { '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                for (int j = 0; j < SV2.Length; j++)
                                    FStyle |= (int)Enum.Parse(typeof(FontStyle), SV2[j], true);
                            }

                            int FUnits = (int)Font.Unit;
                            if (SV.Length >= 4)
                            {
                                FUnits = 0;
                                var SV3 = SV[3].Trim().Split(new char[] { '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                for (int j = 0; j < SV3.Length; j++)
                                    FUnits |= (int)Enum.Parse(typeof(GraphicsUnit), SV3[j], true);
                            }

                            Obj = new Font(FontName, FontSize, (FontStyle)FStyle, (GraphicsUnit)FUnits);
                        }
                    }
                    #endregion

                    PropInfo.SetValue(this, Obj, null);
                }
                catch
                {
                    ParsingError = true;
                }
            }
            return ParsingError;
        }

        #region Properties and some events
        static PointF[] DefaultBlend = new PointF[] { new PointF(0f, 0f), new PointF(1f, 0.5f), new PointF(0f, 1f) };

        #region Disable properties, events browsable values
        [Browsable(false)]
        public override bool AllowDrop { get; set; }

        [Browsable(false)]
        public override Image BackgroundImage { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        new public event EventHandler BackgroundImageChanged
        {
            add { base.BackgroundImageChanged += value; }
            remove { base.BackgroundImageChanged -= value; }
        }

        [Browsable(false)]
        public override ImageLayout BackgroundImageLayout { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        new public event EventHandler BackgroundImageLayoutChanged
        {
            add { base.BackgroundImageLayoutChanged += value; }
            remove { base.BackgroundImageLayoutChanged -= value; }
        }

        [Browsable(false)]
        public override Color ForeColor { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        new public event EventHandler ForeColorChanged
        {
            add { base.ForeColorChanged += value; }
            remove { base.ForeColorChanged -= value; }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        new public event EventHandler PaddingChanged
        {
            add { base.PaddingChanged += value; }
            remove { base.PaddingChanged -= value; }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        new public event EventHandler TextChanged
        {
            add { base.TextChanged += value; }
            remove { base.TextChanged -= value; }
        }
        #endregion

        #region Targeted
        bool targeted = false;

        /// <summary>
        /// true if Mouse is on Control
        /// </summary>
        [Browsable(false)]
        public bool Targeted
        {
            get { return targeted; }
        }
        #endregion

        #region Background
        float bgRoundRadius = 1f;
        GraphicsExt.RoundAngles bgRoundAngles = GraphicsExt.RoundAngles.All;

        /// <summary>
        /// Gets or sets control background color.
        /// </summary>
        [Category("Background")]
        [DefaultValue(typeof(Color), "Transparent")]
        public override Color BackColor { get; set; }

        #region Background
        bool bgEnabled = true;
        Color bgColor1 = Color.Gainsboro;
        Color bgColor2 = Color.Gainsboro;
        Color bgColor1_Disabled = Color.Gainsboro;
        Color bgColor2_Disabled = Color.Gainsboro;
        Color bgColor1_Focused = Color.WhiteSmoke;
        Color bgColor2_Focused = Color.WhiteSmoke;
        Color bgColor1_Targeted = Color.WhiteSmoke;
        Color bgColor2_Targeted = Color.WhiteSmoke;
        float bgGradientAngle = 90f;
        List<PointF> bgBlend = new List<PointF>(DefaultBlend);

        /// <summary>
        /// Is background gradient enabled?
        /// </summary>
        [Category("Background")]
        [Description("Is background gradient enabled?")]
        [DefaultValue(true)]
        public bool BgEnabled
        {
            get { return bgEnabled; }
            set
            {
                bgEnabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 1.
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 1.")]
        [DefaultValue(typeof(Color), "Gainsboro")]
        public Color BgColor1
        {
            get { return bgColor1; }
            set
            {
                bgColor1 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 2.
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 2.")]
        [DefaultValue(typeof(Color), "Gainsboro")]
        public Color BgColor2
        {
            get { return bgColor2; }
            set
            {
                bgColor2 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 1 (for disabled state).
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 1 (for disabled state).")]
        [DefaultValue(typeof(Color), "Gainsboro")]
        public Color BgColor1_Disabled
        {
            get { return bgColor1_Disabled; }
            set
            {
                bgColor1_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 2 (for disabled state).
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 2 (for disabled state).")]
        [DefaultValue(typeof(Color), "Gainsboro")]
        public Color BgColor2_Disabled
        {
            get { return bgColor2_Disabled; }
            set
            {
                bgColor2_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 1 (on focus).
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 1 (on focus).")]
        [DefaultValue(typeof(Color), "WhiteSmoke")]
        public Color BgColor1_Focused
        {
            get { return bgColor1_Focused; }
            set
            {
                bgColor1_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 2 (on focus).
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 2 (on focus).")]
        [DefaultValue(typeof(Color), "WhiteSmoke")]
        public Color BgColor2_Focused
        {
            get { return bgColor2_Focused; }
            set
            {
                bgColor2_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 1 (on target).
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 1 (on target).")]
        [DefaultValue(typeof(Color), "WhiteSmoke")]
        public Color BgColor1_Targeted
        {
            get { return bgColor1_Targeted; }
            set
            {
                bgColor1_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient color 2 (on target).
        /// </summary>
        [Category("Background")]
        [Description("Background gradient color 2 (on target).")]
        [DefaultValue(typeof(Color), "WhiteSmoke")]
        public Color BgColor2_Targeted
        {
            get { return bgColor2_Targeted; }
            set
            {
                bgColor2_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient angle.
        /// </summary>
        [Category("Background")]
        [Description("Background gradient angle.")]
        [DefaultValue(90f)]
        public float BgGradientAngle
        {
            get { return bgGradientAngle; }
            set
            {
                bgGradientAngle = value % 360f;
                Invalidate();
            }
        }

        /// <summary>
        /// Background gradient blending params. X = Factor, Y = Position.
        /// </summary>
        [Category("Background")]
        [Description("Background gradient blending params. X = Factor, Y = Position.")]
        public List<PointF> BgBlend_FactorPos
        {
            get { return FixBlendProperties(bgBlend); }
            set
            {
                bgBlend = value;
                Invalidate();
            }
        }
        #endregion

        #region Background Border
        bool bgBorderEnabled = true;
        Color bgBorderColor1 = Color.DimGray;
        Color bgBorderColor2 = Color.DimGray;
        Color bgBorderColor1_Disabled = Color.DarkGray;
        Color bgBorderColor2_Disabled = Color.DarkGray;
        Color bgBorderColor1_Focused = Color.Gray;
        Color bgBorderColor2_Focused = Color.Gray;
        Color bgBorderColor1_Targeted = Color.Gray;
        Color bgBorderColor2_Targeted = Color.Gray;
        float bgBorderGradientAngle = 90f;
        List<PointF> bgBorderBlend = new List<PointF>(DefaultBlend);
        float bgBorderWidth = 1f;

        /// <summary>
        /// Is background border enabled?
        /// </summary>
        [Category("Background Border")]
        [Description("Is background border enabled?")]
        [DefaultValue(true)]
        public bool BgBorderEnabled
        {
            get { return bgBorderEnabled; }
            set
            {
                bgBorderEnabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 1.
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 1.")]
        [DefaultValue(typeof(Color), "DimGray")]
        public Color BgBorderColor1
        {
            get { return bgBorderColor1; }
            set
            {
                bgBorderColor1 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 2.
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 2.")]
        [DefaultValue(typeof(Color), "DimGray")]
        public Color BgBorderColor2
        {
            get { return bgBorderColor2; }
            set
            {
                bgBorderColor2 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 1 (for disabled state).
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 1 (for disabled state).")]
        [DefaultValue(typeof(Color), "DarkGray")]
        public Color BgBorderColor1_Disabled
        {
            get { return bgBorderColor1_Disabled; }
            set
            {
                bgBorderColor1_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 2 (for disabled state).
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 2 (for disabled state).")]
        [DefaultValue(typeof(Color), "DarkGray")]
        public Color BgBorderColor2_Disabled
        {
            get { return bgBorderColor2_Disabled; }
            set
            {
                bgBorderColor2_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 1 (on focus).
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 1 (on focus).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BgBorderColor1_Focused
        {
            get { return bgBorderColor1_Focused; }
            set
            {
                bgBorderColor1_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 2 (on focus).
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 2 (on focus).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BgBorderColor2_Focused
        {
            get { return bgBorderColor2_Focused; }
            set
            {
                bgBorderColor2_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 1 (on target).
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 1 (on target).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BgBorderColor1_Targeted
        {
            get { return bgBorderColor1_Targeted; }
            set
            {
                bgBorderColor1_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient color 2 (on target).
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient color 2 (on target).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BgBorderColor2_Targeted
        {
            get { return bgBorderColor2_Targeted; }
            set
            {
                bgBorderColor2_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient angle.
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient angle.")]
        [DefaultValue(90f)]
        public float BgBorderGradientAngle
        {
            get { return bgBorderGradientAngle; }
            set
            {
                bgBorderGradientAngle = value % 360f;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border gradient blending params. X = Factor, Y = Position.
        /// </summary>
        [Category("Background Border")]
        [Description("Background border gradient blending params. X = Factor, Y = Position.")]
        public List<PointF> BgBorderBlend_FactorPos
        {
            get { return FixBlendProperties(bgBorderBlend); }
            set
            {
                bgBorderBlend = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Background border width.
        /// </summary>
        [Category("Background Border")]
        [Description("Background border width.")]
        [DefaultValue(1f)]
        public float BgBorderWidth
        {
            get { return bgBorderWidth; }
            set
            {
                bgBorderWidth = Math.Max(value, 0f);
                Invalidate();
            }
        }
        #endregion

        /// <summary>
        /// Background rounded rectangle radius.
        /// </summary>
        [Category("Background")]
        [Description("Background rounded rectangle radius.")]
        [DefaultValue(1f)]
        public float BgRoundRadius
        {
            get { return bgRoundRadius; }
            set
            {
                bgRoundRadius = Math.Max(value, 0f);
                Invalidate();
            }
        }

        /// <summary>
        /// Background rectangle rounded angles.
        /// </summary>
        [Category("Background")]
        [Description("Background rectangle rounded angles.")]
        [DefaultValue(GraphicsExt.RoundAngles.All)]
        public GraphicsExt.RoundAngles BgRoundAngles
        {
            get { return bgRoundAngles; }
            set
            {
                bgRoundAngles = value;
                Invalidate();
            }
        }
        #endregion

        #region Bar
        float barRoundRadius = 1f;
        GraphicsExt.RoundAngles barRoundAngles = GraphicsExt.RoundAngles.All;
        Padding barMargin = Padding.Empty;

        #region Bar
        bool barEnabled = true;
        Color barColor1 = Color.White;
        Color barColor2 = Color.White;
        Color barColor1_Disabled = Color.White;
        Color barColor2_Disabled = Color.White;
        Color barColor1_Focused = Color.White;
        Color barColor2_Focused = Color.White;
        Color barColor1_Targeted = Color.White;
        Color barColor2_Targeted = Color.White;
        float barGradientAngle = 90f;
        List<PointF> barBlend = new List<PointF>(DefaultBlend);

        /// <summary>
        /// Is bar gradient enabled?
        /// </summary>
        [Category("Bar")]
        [Description("Is bar gradient enabled?")]
        [DefaultValue(true)]
        public bool BarEnabled
        {
            get { return barEnabled; }
            set
            {
                barEnabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 1.
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 1.")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor1
        {
            get { return barColor1; }
            set
            {
                barColor1 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 2.
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 2.")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor2
        {
            get { return barColor2; }
            set
            {
                barColor2 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 1 (for disabled state).
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 1 (for disabled state).")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor1_Disabled
        {
            get { return barColor1_Disabled; }
            set
            {
                barColor1_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 2 (for disabled state).
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 2 (for disabled state).")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor2_Disabled
        {
            get { return barColor2_Disabled; }
            set
            {
                barColor2_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 1 (on focus).
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 1 (on focus).")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor1_Focused
        {
            get { return barColor1_Focused; }
            set
            {
                barColor1_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 2 (on focus).
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 2 (on focus).")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor2_Focused
        {
            get { return barColor2_Focused; }
            set
            {
                barColor2_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 1 (on target).
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 1 (on target).")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor1_Targeted
        {
            get { return barColor1_Targeted; }
            set
            {
                barColor1_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient color 2 (on target).
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient color 2 (on target).")]
        [DefaultValue(typeof(Color), "White")]
        public Color BarColor2_Targeted
        {
            get { return barColor2_Targeted; }
            set
            {
                barColor2_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient angle.
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient angle.")]
        [DefaultValue(90f)]
        public float BarGradientAngle
        {
            get { return barGradientAngle; }
            set
            {
                barGradientAngle = value % 360f;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient blending params. X = Factor, Y = Position.
        /// </summary>
        [Category("Bar")]
        [Description("Bar gradient blending params. X = Factor, Y = Position.")]
        public List<PointF> BarBlend_FactorPos
        {
            get { return FixBlendProperties(barBlend); }
            set
            {
                barBlend = value;
                Invalidate();
            }
        }
        #endregion

        #region Bar Border
        bool barBorderEnabled = true;
        Color barBorderColor1 = Color.DimGray;
        Color barBorderColor2 = Color.DimGray;
        Color barBorderColor1_Disabled = Color.DarkGray;
        Color barBorderColor2_Disabled = Color.DarkGray;
        Color barBorderColor1_Focused = Color.Gray;
        Color barBorderColor2_Focused = Color.Gray;
        Color barBorderColor1_Targeted = Color.Gray;
        Color barBorderColor2_Targeted = Color.Gray;
        float barBorderGradientAngle = 90f;
        List<PointF> barBorderBlend = new List<PointF>(DefaultBlend);
        float barBorderWidth = 1f;

        /// <summary>
        /// Is bar border enabled?
        /// </summary>
        [Category("Bar Border")]
        [Description("Is bar border enabled?")]
        [DefaultValue(true)]
        public bool BarBorderEnabled
        {
            get { return barBorderEnabled; }
            set
            {
                barBorderEnabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 1.
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 1.")]
        [DefaultValue(typeof(Color), "DimGray")]
        public Color BarBorderColor1
        {
            get { return barBorderColor1; }
            set
            {
                barBorderColor1 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 2.
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 2.")]
        [DefaultValue(typeof(Color), "DimGray")]
        public Color BarBorderColor2
        {
            get { return barBorderColor2; }
            set
            {
                barBorderColor2 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 1 (for disabled state).
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 1 (for disabled state).")]
        [DefaultValue(typeof(Color), "DarkGray")]
        public Color BarBorderColor1_Disabled
        {
            get { return barBorderColor1_Disabled; }
            set
            {
                barBorderColor1_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 2 (for disabled state).
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 2 (for disabled state).")]
        [DefaultValue(typeof(Color), "DarkGray")]
        public Color BarBorderColor2_Disabled
        {
            get { return barBorderColor2_Disabled; }
            set
            {
                barBorderColor2_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 1 (on focus).
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 1 (on focus).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BarBorderColor1_Focused
        {
            get { return barBorderColor1_Focused; }
            set
            {
                barBorderColor1_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 2 (on focus).
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 2 (on focus).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BarBorderColor2_Focused
        {
            get { return barBorderColor2_Focused; }
            set
            {
                barBorderColor2_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 1 (on target).
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 1 (on target).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BarBorderColor1_Targeted
        {
            get { return barBorderColor1_Targeted; }
            set
            {
                barBorderColor1_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient color 2 (on target).
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient color 2 (on target).")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color BarBorderColor2_Targeted
        {
            get { return barBorderColor2_Targeted; }
            set
            {
                barBorderColor2_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border gradient angle.
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border gradient angle.")]
        [DefaultValue(90f)]
        public float BarBorderGradientAngle
        {
            get { return barBorderGradientAngle; }
            set
            {
                barBorderGradientAngle = value % 360f;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar gradient blending params. X = Factor, Y = Position.
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar gradient blending params. X = Factor, Y = Position.")]
        public List<PointF> BarBorderBlend_FactorPos
        {
            get { return FixBlendProperties(barBorderBlend); }
            set
            {
                barBorderBlend = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar border width.
        /// </summary>
        [Category("Bar Border")]
        [Description("Bar border width.")]
        [DefaultValue(1f)]
        public float BarBorderWidth
        {
            get { return barBorderWidth; }
            set
            {
                barBorderWidth = Math.Max(value, 0f);
                Invalidate();
            }
        }
        #endregion

        #region Bar Arrows Icon
        bool arrowsIconEnabled = true;
        Color arrowsIconColor1 = Color.Black;
        Color arrowsIconColor2 = Color.Black;
        Color arrowsIconColor1_Disabled = Color.DimGray;
        Color arrowsIconColor2_Disabled = Color.DimGray;
        Color arrowsIconColor1_Focused = Color.Black;
        Color arrowsIconColor2_Focused = Color.Black;
        Color arrowsIconColor1_Targeted = Color.Black;
        Color arrowsIconColor2_Targeted = Color.Black;
        float arrowsIconGradientAngle = 45f;
        List<PointF> arrowsIconBlend = new List<PointF>(DefaultBlend);
        float arrowsIconTrianglesSize = 0.67f;
        float arrowsIconScale = 0.55f;
        SizeF arrowsIconShift = new SizeF();

        /// <summary>
        /// Is arrows icon drawing enabled?
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Is arrows icon drawing enabled?")]
        [DefaultValue(true)]
        public bool ArrowsIconEnabled
        {
            get { return arrowsIconEnabled; }
            set
            {
                arrowsIconEnabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 1.
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 1.")]
        [DefaultValue(typeof(Color), "Black")]
        public Color ArrowsIconColor1
        {
            get { return arrowsIconColor1; }
            set
            {
                arrowsIconColor1 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 2.
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 2.")]
        [DefaultValue(typeof(Color), "Black")]
        public Color ArrowsIconColor2
        {
            get { return arrowsIconColor2; }
            set
            {
                arrowsIconColor2 = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 1 (for disabled state).
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 1 (for disabled state).")]
        [DefaultValue(typeof(Color), "DimGray")]
        public Color ArrowsIconColor1_Disabled
        {
            get { return arrowsIconColor1_Disabled; }
            set
            {
                arrowsIconColor1_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 2 (for disabled state).
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 2 (for disabled state).")]
        [DefaultValue(typeof(Color), "DimGray")]
        public Color ArrowsIconColor2_Disabled
        {
            get { return arrowsIconColor2_Disabled; }
            set
            {
                arrowsIconColor2_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 1 (on focus).
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 1 (on focus).")]
        [DefaultValue(typeof(Color), "Black")]
        public Color ArrowsIconColor1_Focused
        {
            get { return arrowsIconColor1_Focused; }
            set
            {
                arrowsIconColor1_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 2 (on focus).
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 2 (on focus).")]
        [DefaultValue(typeof(Color), "Black")]
        public Color ArrowsIconColor2_Focused
        {
            get { return arrowsIconColor2_Focused; }
            set
            {
                arrowsIconColor2_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 1 (on target).
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 1 (on target).")]
        [DefaultValue(typeof(Color), "Black")]
        public Color ArrowsIconColor1_Targeted
        {
            get { return arrowsIconColor1_Targeted; }
            set
            {
                arrowsIconColor1_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient color 2 (on target).
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient color 2 (on target).")]
        [DefaultValue(typeof(Color), "Black")]
        public Color ArrowsIconColor2_Targeted
        {
            get { return arrowsIconColor2_Targeted; }
            set
            {
                arrowsIconColor2_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient angle.
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient angle.")]
        [DefaultValue(45f)]
        public float ArrowsIconGradientAngle
        {
            get { return arrowsIconGradientAngle; }
            set
            {
                arrowsIconGradientAngle = value % 360f;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon gradient blending params. X = Factor, Y = Position.
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon gradient blending params. X = Factor, Y = Position.")]
        public List<PointF> ArrowsIconBlend_FactorPos
        {
            get { return FixBlendProperties(arrowsIconBlend); }
            set
            {
                arrowsIconBlend = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon triangles scale [0..1].
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon triangles scale [0..1].")]
        [DefaultValue(0.67f)]
        public float ArrowsIconTrianglesSize
        {
            get { return arrowsIconTrianglesSize; }
            set
            {
                arrowsIconTrianglesSize = Math.Max(Math.Min(value, 1f), 0f);
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon size scale [0..1].
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon size scale [0..1].")]
        [DefaultValue(0.55f)]
        public float ArrowsIconScale
        {
            get { return arrowsIconScale; }
            set
            {
                arrowsIconScale = Math.Max(Math.Min(value, 1f), 0f);
                Invalidate();
            }
        }

        /// <summary>
        /// Arrows icon location shift.
        /// </summary>
        [Category("Bar Arrows Icon")]
        [Description("Arrows icon location shift.")]
        [DefaultValue(typeof(SizeF), "0, 0")]
        public SizeF ArrowsIconShift
        {
            get { return arrowsIconShift; }
            set
            {
                arrowsIconShift = value;
                Invalidate();
            }
        }
        #endregion

        /// <summary>
        /// Bar rounded rectangle radius.
        /// </summary>
        [Category("Bar")]
        [Description("Bar rounded rectangle radius.")]
        [DefaultValue(1f)]
        public float BarRoundRadius
        {
            get { return barRoundRadius; }
            set
            {
                barRoundRadius = Math.Max(value, 0f);
                Invalidate();
            }
        }

        /// <summary>
        /// Bar rectangle rounded angles.
        /// </summary>
        [Category("Bar")]
        [Description("Bar rectangle rounded angles.")]
        [DefaultValue(GraphicsExt.RoundAngles.All)]
        public GraphicsExt.RoundAngles BarRoundAngles
        {
            get { return barRoundAngles; }
            set
            {
                barRoundAngles = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Bar Margin.
        /// </summary>
        [Category("Bar")]
        [Description("Bar Margin.")]
        [DefaultValue(typeof(Padding), "0, 0, 0, 0")]
        public Padding BarMargin
        {
            get { return barMargin; }
            set
            {
                barMargin = value;
                Invalidate();
            }
        }
        #endregion

        #region Text
        bool textEnabled = true;
        Color textColor = Color.Black;
        Color textColor_Disabled = Color.DimGray;
        Color textColor_Focused = Color.Black;
        Color textColor_Targeted = Color.Black;

        bool textShadowEnabled = true;
        Color textShadowColor = Color.LightGray;
        Color textShadowColor_Disabled = Color.LightGray;
        Color textShadowColor_Focused = Color.LightGray;
        Color textShadowColor_Targeted = Color.LightGray;
        SizeF textShadowOffset = new SizeF(1f, 1f);

        ContentAlignment textAlignment = ContentAlignment.MiddleLeft;
        Padding textMargin = Padding.Empty;

        #region Text
        /// <summary>
        /// Is text enabled?
        /// </summary>
        [Category("Text")]
        [Description("Is text enabled?")]
        [DefaultValue(true)]
        public bool TextEnabled
        {
            get { return textEnabled; }
            set
            {
                textEnabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text color.
        /// </summary>
        [Category("Text")]
        [Description("Text color.")]
        [DefaultValue(typeof(Color), "Black")]
        public Color TextColor
        {
            get { return textColor; }
            set
            {
                textColor = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text color (for disabled state).
        /// </summary>
        [Category("Text")]
        [Description("Text color (for disabled state).")]
        [DefaultValue(typeof(Color), "DimGray")]
        public Color TextColor_Disabled
        {
            get { return textColor_Disabled; }
            set
            {
                textColor_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text color (on focus).
        /// </summary>
        [Category("Text")]
        [Description("Text color (on focus).")]
        [DefaultValue(typeof(Color), "Black")]
        public Color TextColor_Focused
        {
            get { return textColor_Focused; }
            set
            {
                textColor_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text color (on target).
        /// </summary>
        [Category("Text")]
        [Description("Text color (on target).")]
        [DefaultValue(typeof(Color), "Black")]
        public Color TextColor_Targeted
        {
            get { return textColor_Targeted; }
            set
            {
                textColor_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Is text shadow enabled?
        /// </summary>
        [Category("Text")]
        [Description("Is text shadow enabled?")]
        [DefaultValue(true)]
        public bool TextShadowEnabled
        {
            get { return textShadowEnabled; }
            set
            {
                textShadowEnabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text shadow color.
        /// </summary>
        [Category("Text")]
        [Description("Text shadow color.")]
        [DefaultValue(typeof(Color), "LightGray")]
        public Color TextShadowColor
        {
            get { return textShadowColor; }
            set
            {
                textShadowColor = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text shadow color (for disabled state).
        /// </summary>
        [Category("Text")]
        [Description("Text shadow color (for disabled state).")]
        [DefaultValue(typeof(Color), "LightGray")]
        public Color TextShadowColor_Disabled
        {
            get { return textShadowColor_Disabled; }
            set
            {
                textShadowColor_Disabled = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text shadow color (on focus).
        /// </summary>
        [Category("Text")]
        [Description("Text shadow color (on focus).")]
        [DefaultValue(typeof(Color), "LightGray")]
        public Color TextShadowColor_Focused
        {
            get { return textShadowColor_Focused; }
            set
            {
                textShadowColor_Focused = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text shadow color (on target).
        /// </summary>
        [Category("Text")]
        [Description("Text shadow color (on target).")]
        [DefaultValue(typeof(Color), "LightGray")]
        public Color TextShadowColor_Targeted
        {
            get { return textShadowColor_Targeted; }
            set
            {
                textShadowColor_Targeted = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text shadow offset.
        /// </summary>
        [Category("Text")]
        [Description("Text shadow offset.")]
        [DefaultValue(typeof(SizeF), "1, 1")]
        public SizeF TextShadowOffset
        {
            get { return textShadowOffset; }
            set
            {
                textShadowOffset = value;
                Invalidate();
            }
        }
        #endregion

        /// <summary>
        /// Gets control text.
        /// </summary>
        [Category("Text")]
        [Description("Gets control text.")]
        [Bindable(false)]
        //[Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override string Text
        {
            get
            {
                if (arrayOfValuesEnabled)
                    return String.Format(TextStringFormat, SelectedItem, SelectedIndex, DefaultIndex);
                else
                    return String.Format(TextStringFormat, currentValue, minValue, maxValue,
                        sliderMinValue, sliderMaxValue, defaultValue, step);
            }
        }

        /// <summary>
        /// Specifies the alignment of text.
        /// </summary>
        [Category("Text")]
        [Description("Specifies the alignment of text.")]
        [DefaultValue(ContentAlignment.MiddleLeft)]
        public ContentAlignment TextAlignment
        {
            get { return textAlignment; }
            set
            {
                textAlignment = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Text Margin.
        /// </summary>
        [Category("Text")]
        [Description("Text Margin.")]
        [DefaultValue(typeof(Padding), "0, 0, 0, 0")]
        public Padding TextMargin
        {
            get { return textMargin; }
            set
            {
                textMargin = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Reset TextStringFormat to default.
        /// </summary>
        public override void ResetText()
        {
            base.ResetText();
            TextStringFormat = DefaultTextStringFormat;
        }
        #endregion

        #region Value
        decimal defaultValue = decimal.Zero;
        decimal currentValue = decimal.Zero;
        decimal minValue = decimal.Zero;
        decimal maxValue = 100;
        decimal sliderMinValue = decimal.Zero;
        decimal sliderMaxValue = 100;
        decimal step = 1;

        /// <summary>
        /// Default value.
        /// </summary>
        [Category("Value")]
        [Description("Default value.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(decimal), "0")]
        public decimal ValueDefault
        {
            get { return defaultValue; }
            set
            {
                var ValueOld = defaultValue;
                defaultValue = value;
                CheckValues(currentValue, ValueOld, minValue, maxValue, sliderMinValue, sliderMaxValue, step);
                Debug.WriteLineIf(defaultValue != value, "\"" + nameof(ValueDefault) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// Current value.
        /// </summary>
        [Category("Value")]
        [Description("Current value.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(decimal), "0")]
        public decimal Value
        {
            get { return currentValue; }
            set
            {
                var ValueOld = currentValue;
                currentValue = value;
                CheckValues(ValueOld, defaultValue, minValue, maxValue, sliderMinValue, sliderMaxValue, step);
                Debug.WriteLineIf(currentValue != value, "\"" + nameof(Value) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// Gets or sets the minimum value for the control.
        /// </summary>
        [Category("Value")]
        [Description("Gets or sets the minimum value for the control.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(decimal), "0")]
        public decimal Minimum
        {
            get { return minValue; }
            set
            {
                var ValueOld = minValue;

                minValue = value;
                if (minValue > maxValue)
                    maxValue = minValue;

                CheckValues(currentValue, defaultValue, ValueOld, maxValue, sliderMinValue, sliderMaxValue, step);
                Debug.WriteLineIf(minValue != value, "\"" + nameof(Minimum) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// Gets or sets the maximum value for the control.
        /// </summary>
        [Category("Value")]
        [Description("Gets or sets the maximum value for the control.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(decimal), "100")]
        public decimal Maximum
        {
            get { return maxValue; }
            set
            {
                var ValueOld = maxValue;

                maxValue = value;
                if (maxValue < minValue)
                    minValue = maxValue;

                CheckValues(currentValue, defaultValue, minValue, ValueOld, sliderMinValue, sliderMaxValue, step);
                Debug.WriteLineIf(maxValue != value, "\"" + nameof(Maximum) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// Slider minimum value. Must be bigger or equal then <see cref="Minimum"/>.
        /// </summary>
        [Category("Value")]
        [Description("Slider minimum value. Must be bigger or equal then 'Minimum'.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(decimal), "0")]
        public decimal SliderMinimum
        {
            get { return sliderMinValue; }
            set
            {
                var ValueOld = sliderMinValue;
                sliderMinValue = value;
                CheckValues(currentValue, defaultValue, minValue, maxValue, ValueOld, sliderMaxValue, step);
                Debug.WriteLineIf(sliderMinValue != value, "\"" + nameof(SliderMinimum) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// Slider maximum value. Must be smaller or equal then <see cref="Maximum"/>.
        /// </summary>
        [Category("Value")]
        [Description("Slider maximum value. Must be smaller or equal then 'Maximum'.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(decimal), "100")]
        public decimal SliderMaximum
        {
            get { return sliderMaxValue; }
            set
            {
                var ValueOld = sliderMaxValue;

                sliderMaxValue = value;
                if (sliderMaxValue < sliderMinValue)
                    sliderMinValue = sliderMaxValue;

                CheckValues(currentValue, defaultValue, minValue, maxValue, sliderMinValue, ValueOld, step);
                Debug.WriteLineIf(sliderMaxValue != value, "\"" + nameof(SliderMaximum) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// The amount to increment by (slider and arrow keys only).
        /// </summary>
        [Category("Value")]
        [Description("The amount to increment by (slider and arrow keys only).")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(decimal), "1")]
        public decimal Step
        {
            get { return step; }
            set
            {
                var ValueOld = step;
                step = value;
                CheckValues(currentValue, defaultValue, minValue, maxValue, sliderMinValue, sliderMaxValue, ValueOld);
                Debug.WriteLineIf(step != value, "\"" + nameof(Step) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// Check and fix values ranges.
        /// </summary>
        /// <param name="Cv"><see cref="Value"/> old value.</param>
        /// <param name="Dv"><see cref="ValueDefault"/> old value.</param>
        /// <param name="Min"><see cref="Minimum"/> old value.</param>
        /// <param name="Max"><see cref="Maximum"/> old value.</param>
        /// <param name="MinS"><see cref="SliderMinimum"/> old value.</param>
        /// <param name="MaxS"><see cref="SliderMaximum"/> old value.</param>
        /// <param name="St"><see cref="Step"/> old value.</param>
        void CheckValues(decimal Cv, decimal Dv, decimal Min, decimal Max, decimal MinS, decimal MaxS, decimal St)
        {
            // Slider Min
            if (sliderMinValue < minValue)
                sliderMinValue = minValue;
            if (sliderMinValue > maxValue)
                sliderMinValue = maxValue;
            if (sliderMinValue > sliderMaxValue)
                sliderMaxValue = sliderMinValue;

            // Slider Max
            if (sliderMaxValue > maxValue)
                sliderMaxValue = maxValue;
            if (sliderMaxValue < minValue)
                sliderMaxValue = minValue;
            if (sliderMaxValue < sliderMinValue)
                sliderMinValue = sliderMaxValue;

            // Value
            if (currentValue < minValue)
                currentValue = minValue;
            if (currentValue > maxValue)
                currentValue = maxValue;

            // Default Value
            if (defaultValue < minValue)
                defaultValue = minValue;
            if (defaultValue > maxValue)
                defaultValue = maxValue;

            // Step
            step = Math.Max(Math.Min(step, Math.Abs(maxValue - minValue)), 0);

            // Events
            if (currentValue != Cv)
                OnValueChanged(EventArgs.Empty);
            if (defaultValue != Dv)
                OnValueDefaultChanged(EventArgs.Empty);
            if (minValue != Min)
                OnMinimumChanged(EventArgs.Empty);
            if (maxValue != Max)
                OnMaximumChanged(EventArgs.Empty);
            if (sliderMinValue != MinS)
                OnSliderMinimumChanged(EventArgs.Empty);
            if (sliderMaxValue != MaxS)
                OnSliderMaximumChanged(EventArgs.Empty);
            if (step != St)
                OnStepChanged(EventArgs.Empty);

            Invalidate();
        }

        /// <summary>
        /// Sets <see cref="Value"/> = <see cref="ValueDefault"/>.
        /// </summary>
        public void ResetValue()
        {
            Value = defaultValue;
            OnValueReset(EventArgs.Empty);
        }
        #endregion

        #region Values Array
        bool arrayOfValuesEnabled = false;
        string[] arrayOfValues;
        int selectedIndex = 0;
        int defaultIndex = 0;

        /// <summary>
        /// Use Values Array?
        /// </summary>
        [Category("Values Array")]
        [Description("Use Values Array?")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(false)]
        public bool ArrayOfValuesEnabled
        {
            get { return arrayOfValuesEnabled; }
            set
            {
                var ValueOld = arrayOfValuesEnabled;

                arrayOfValuesEnabled = value;
                if (arrayOfValuesEnabled)
                {
                    CheckSelectedIndex(selectedIndex, defaultIndex);
                    TextInput.AutoCompleteSource = AutoCompleteSource.CustomSource;
                }
                else
                {
                    CheckValues(currentValue, defaultValue, minValue, maxValue, sliderMinValue, sliderMaxValue, step);
                    TextInput.AutoCompleteSource = AutoCompleteSource.None;
                }

                if (ValueOld != arrayOfValuesEnabled)
                    OnArrayOfValuesEnabledChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Array of values.
        /// </summary>
        [Category("Values Array")]
        [Description("Array of values.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(typeof(string[]), "[0]")]
        public string[] ArrayOfValues
        {
            get { return arrayOfValues; }
            set
            {
                bool IsArraysEqual = false;

                if (arrayOfValues == null || value == null || arrayOfValues.Length != value.Length)
                    SelectedIndex = 0;
                else //Compare arrays
                    IsArraysEqual = arrayOfValues.Equals(value);

                arrayOfValues = value;
                CheckSelectedIndex(selectedIndex, defaultIndex);

                if (arrayOfValues != null)
                {
                    TextInput.AutoCompleteCustomSource.Clear();
                    TextInput.AutoCompleteCustomSource.AddRange(arrayOfValues);
                }

                if (!IsArraysEqual)
                    OnArrayOfValuesChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// The [zero based] index of the default selected item in the <see cref="ArrayOfValues"/> array.
        /// </summary>
        [Category("Values Array")]
        [Description("The [zero based] index of the default selected item in the \"" + nameof(ArrayOfValues) + "\" array.")]
        [DefaultValue(0)]
        public int DefaultIndex
        {
            get { return defaultIndex; }
            set
            {
                var ValueOld = defaultIndex;
                defaultIndex = value;
                CheckSelectedIndex(selectedIndex, ValueOld);
                Debug.WriteLineIf(defaultIndex != value, "\"" + nameof(DefaultIndex) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// The [zero based] index of the currently selected item in the <see cref="ArrayOfValues"/> array.
        /// </summary>
        [Category("Values Array")]
        [Description("The [zero based] index of the currently selected item in the \"" + nameof(ArrayOfValues) + "\" array.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(0)]
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                var ValueOld = selectedIndex;
                selectedIndex = value;
                CheckSelectedIndex(ValueOld, defaultIndex);
                Debug.WriteLineIf(selectedIndex != value, "\"" + nameof(SelectedIndex) + "\" != what we just set it to!");
            }
        }

        /// <summary>
        /// Return currently selected item in the <see cref="ArrayOfValues"/> array. Note If the value 
        /// is "null", then the <see cref="SliderTrackbar"/> is "null".
        /// </summary>
        [Category("Values Array")]
        [Description("Gets or Sets currently selected item in the \"" + nameof(ArrayOfValues) +
            "\" array. Note If the value is \"null\", then the \"" + nameof(SliderTrackbar) + "\" is \"null\".")]
        [RefreshProperties(RefreshProperties.All)]
        public string SelectedItem
        {
            get
            {
                if (ArrayOfValues == null || ArrayOfValues.Length <= 0 || selectedIndex >= ArrayOfValues.Length)
                    return null;

                return ArrayOfValues[selectedIndex];
            }
            set
            {
                if (arrayOfValues == null || ArrayOfValues.Length <= 0)
                    return;

                int InHash = value.GetHashCode();
                string InLower = value.ToLowerInvariant();
                int InLHash = InLower.GetHashCode();
                string InTrimLower = value.Trim().ToLowerInvariant();
                int InTLHash = InTrimLower.GetHashCode();

                bool Found = false;

                for (int i = 0; i < arrayOfValues.Length; i++)
                {
                    if (arrayOfValues[i].GetHashCode() == InHash && arrayOfValues[i] == value)
                    {
                        SelectedIndex = i;
                        Found = true;
                        break;
                    }
                }

                if (!Found)
                    for (int i = 0; i < arrayOfValues.Length; i++)
                    {
                        string ArrL = arrayOfValues[i].ToLowerInvariant();
                        if (ArrL.GetHashCode() == InLHash && ArrL == InLower)
                        {
                            SelectedIndex = i;
                            Found = true;
                            break;
                        }
                    }

                if (!Found)
                    for (int i = 0; i < arrayOfValues.Length; i++)
                    {
                        string ArrTL = arrayOfValues[i].Trim().ToLowerInvariant();
                        if (ArrTL.GetHashCode() == InTLHash && ArrTL == InTrimLower)
                        {
                            SelectedIndex = i;
                            Found = true;
                            break;
                        }
                    }
            }
        }

        void CheckSelectedIndex(int SiOld, int DiOld)
        {
            if (arrayOfValues != null && arrayOfValues.Length > 0)
            {
                // SelectedIndex
                if (selectedIndex < 0)
                    selectedIndex = 0;
                if (selectedIndex >= arrayOfValues.Length)
                    selectedIndex = arrayOfValues.Length - 1;

                // DefaultIndex
                if (defaultIndex < 0)
                    defaultIndex = 0;
                if (defaultIndex >= arrayOfValues.Length)
                    defaultIndex = arrayOfValues.Length - 1;
            }
            else
            {
                selectedIndex = 0;
                defaultIndex = 0;
            }

            if (SiOld != selectedIndex)
                OnSelectedIndexChanged(EventArgs.Empty);

            if (DiOld != defaultIndex)
                OnDefaultIndexChanged(EventArgs.Empty);

            Invalidate();
        }

        /// <summary>
        /// Sets <see cref="SelectedIndex"/> = <see cref="DefaultIndex"/>.
        /// </summary>
        public void ResetSelectedIndex()
        {
            SelectedIndex = defaultIndex;
            OnSelectedIndexReset(EventArgs.Empty);
        }
        #endregion

        #region Appearance
        CompositingQuality compositingQuality = CompositingQuality.HighQuality;
        PixelOffsetMode pixelOffsetMode = PixelOffsetMode.HighQuality;
        SmoothingMode smoothingMode = SmoothingMode.HighQuality;
        TextRenderingHint textRenderingHint = TextRenderingHint.SystemDefault;
        bool gradientGammaCorrection = false;
        Orientation orientation = Orientation.Horizontal;
        bool autoOrientation = false;
        bool inverseLayout = false;
        bool applyLayoutToArrows = true;

        /// <summary>
        /// Compositing Quality.
        /// </summary>
        [Category("Appearance")]
        [Description("Compositing Quality")]
        [DefaultValue(CompositingQuality.HighQuality)]
        public CompositingQuality CompositingQuality
        {
            get { return compositingQuality; }
            set
            {
                if (value == CompositingQuality.Invalid)
                    value = CompositingQuality.Default;

                compositingQuality = value;

                Invalidate();
            }
        }

        /// <summary>
        /// PixelOffset Mode.
        /// </summary>
        [Category("Appearance")]
        [Description("PixelOffset Mode")]
        [DefaultValue(PixelOffsetMode.HighQuality)]
        public PixelOffsetMode PixelOffsetMode
        {
            get { return pixelOffsetMode; }
            set
            {
                if (value == PixelOffsetMode.Invalid)
                    value = PixelOffsetMode.Default;

                pixelOffsetMode = value;

                Invalidate();
            }
        }

        /// <summary>
        /// Smoothing Mode.
        /// </summary>
        [Category("Appearance")]
        [Description("Smoothing Mode")]
        [DefaultValue(SmoothingMode.HighQuality)]
        public SmoothingMode SmoothingMode
        {
            get { return smoothingMode; }
            set
            {
                if (value == SmoothingMode.Invalid)
                    value = SmoothingMode.Default;

                smoothingMode = value;

                Invalidate();
            }
        }

        /// <summary>
        /// Text rendering smoothing mode.
        /// </summary>
        [Category("Appearance")]
        [Description("Text rendering smoothing mode.")]
        [DefaultValue(TextRenderingHint.SystemDefault)]
        public TextRenderingHint TextRenderingHint
        {
            get { return textRenderingHint; }
            set
            {
                textRenderingHint = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Enable or disable double buffering.
        /// </summary>
        [Category("Appearance")]
        [Description("Enable or disable double buffering.")]
        [DefaultValue(true)]
        public new bool DoubleBuffered
        {
            get { return base.DoubleBuffered; }
            set { base.DoubleBuffered = value; }
        }

        /// <summary>
        /// Is gradient GammaCorrection enabled?
        /// </summary>
        [Category("Appearance")]
        [Description("Is gradient GammaCorrection enabled?")]
        [DefaultValue(false)]
        public bool GradientGammaCorrection
        {
            get { return gradientGammaCorrection; }
            set
            {
                gradientGammaCorrection = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Control orientation.
        /// </summary>
        [Category("Appearance")]
        [Description("Control orientation.")]
        [DefaultValue(Orientation.Horizontal)]
        public Orientation Orientation
        {
            get { return orientation; }
            set
            {
                var ValueOld = orientation;

                orientation = value;
                Invalidate();

                if (ValueOld != orientation)
                    OnOrientationChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Enable auto orientation.
        /// </summary>
        [Category("Appearance")]
        [Description("Enable auto orientation.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(false)]
        public bool AutoOrientation
        {
            get { return autoOrientation; }
            set
            {
                autoOrientation = value;
                FixOrientation();
            }
        }

        /// <summary>
        /// Enable RightToLeft or UpToDown draw inversion.
        /// </summary>
        [Category("Appearance")]
        [Description("Enable RightToLeft or UpToDown draw inversion.")]
        [DefaultValue(false)]
        public bool InverseLayout
        {
            get { return inverseLayout; }
            set
            {
                var ValueOld = inverseLayout;

                inverseLayout = value;
                Invalidate();

                if (ValueOld != inverseLayout)
                    OnInverseLayoutChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Enable Arrows icon inversion with layout.
        /// </summary>
        [Category("Appearance")]
        [Description("Enable Arrows icon inversion with layout.")]
        [DefaultValue(true)]
        public bool ApplyLayoutToArrows
        {
            get { return applyLayoutToArrows; }
            set
            {
                applyLayoutToArrows = value;
                Invalidate();
            }
        }
        #endregion

        #region Behavior
        bool keyboardInput = true;
        bool keyboardInputStep = true;
        bool arrowKeysInput = true;
        bool arrowKeysSliderRange = false;
        bool resetValueOnDoubleClick = true;
        MouseButtons resetValueMouseButton = MouseButtons.Middle;
        bool halfStepShift = true;

        /// <summary>
        /// Enable or disable keyboard input (press <see cref="Keys.Enter"/> to input).
        /// </summary>
        [Category("Behavior")]
        [Description("Enable or disable keyboard input (press " + nameof(Keys.Enter) + " to input).")]
        [DefaultValue(true)]
        public bool KeyboardInput
        {
            get { return keyboardInput; }
            set { keyboardInput = value; }
        }

        /// <summary>
        /// Enable or disable keyboard input step.
        /// </summary>
        [Category("Behavior")]
        [Description("Enable or disable keyboard input step.")]
        [DefaultValue(true)]
        public bool KeyboardInputStep
        {
            get { return keyboardInputStep; }
            set
            {
                keyboardInputStep = value;

                if (keyboardInputStep)
                {
                    decimal Val = Value - sliderMinValue;
                    decimal ModStep = (step > decimal.Zero ? Val % step : decimal.Zero);
                    Value = sliderMinValue + Val - ModStep + (ModStep >= step * 0.5M ? step : decimal.Zero);
                }
            }
        }

        /// <summary>
        /// Enable or disable using arrow keys: <see cref="Keys.Left"/>, <see cref="Keys.Right"/>,
        /// <see cref="Up"/>, <see cref="Down"/> for input.
        /// </summary>
        [Category("Behavior")]
        [Description("Enable or disable using arrow keys: \"" + nameof(Keys.Left) + "\", \"" +
            nameof(Keys.Right) + "\", \"" + nameof(Keys.Up) + "\", \"" + nameof(Keys.Down) + "\" for input.")]
        [DefaultValue(true)]
        public bool ArrowKeysInput
        {
            get { return arrowKeysInput; }
            set { arrowKeysInput = value; }
        }

        /// <summary>
        /// Arrow keys can change values only in range from <see cref="SliderMinimum"/> to <see cref="SliderMaximum"/>.
        /// </summary>
        [Category("Behavior")]
        [Description("Arrow keys can change values only in range from \"SliderMinimum\" to \"SliderMaximum\".")]
        [DefaultValue(false)]
        public bool ArrowKeysSliderRange
        {
            get { return arrowKeysSliderRange; }
            set { arrowKeysSliderRange = value; }
        }

        /// <summary>
        /// Use mouse DoubleClick or Click event for reset <see cref="Value"/> to <see cref="ValueDefault"/>
        /// (If <see cref="ResetValueMouseButton"/> not equal <see cref="MouseButtons.None"/>).
        /// </summary>
        [Category("Behavior")]
        [Description("Use mouse DoubleClick or Click event for reset Value to Default (If \"ResetValueMouseButton\" not equal \"None\").")]
        [DefaultValue(true)]
        public bool ResetValueOnDoubleClick
        {
            get { return resetValueOnDoubleClick; }
            set { resetValueOnDoubleClick = value; }
        }

        /// <summary>
        /// Mouse button for reseting <see cref="Value"/> to <see cref="ValueDefault"/>
        /// (If not equal <see cref="MouseButtons.None"/>).
        /// </summary>
        [Category("Behavior")]
        [Description("Mouse button for reseting Value to Default (If not equal \"None\").")]
        [DefaultValue(MouseButtons.Middle)]
        public MouseButtons ResetValueMouseButton
        {
            get { return resetValueMouseButton; }
            set { resetValueMouseButton = value; }
        }

        /// <summary>
        /// Half <see cref="Step"/> mouse input shift.
        /// </summary>
        [Category("Behavior")]
        [Description("Half \"" + nameof(Step) + "\" mouse input shift.")]
        [DefaultValue(true)]
        public bool HalfStepShift
        {
            get { return halfStepShift; }
            set { halfStepShift = value; }
        }
        #endregion

        #region  Format
        const string DefaultTextStringFormat = "{0:0.###}";
        const string DefaultTextInputStringFormat = "0.######";

        string textStringFormat = DefaultTextStringFormat;
        string textInputStringFormat = DefaultTextInputStringFormat;

        /// <summary>
        /// Text string format. Example: "Value: {0}", "Value: {0:0.###}".
        /// Where "{0}" used for <see cref="Value"/> or <see cref="SelectedItem"/>,
        /// "{1}" for <see cref="Minimum"/> or <see cref="SelectedIndex"/>,
        /// "{2}" for <see cref="Maximum"/> or <see cref="DefaultIndex"/>,
        /// "{3}" for <see cref="SliderMaximum"/>, "{4}" for <see cref="SliderMaximum"/>,
        /// "{5}" for <see cref="ValueDefault"/>, "{6}" for <see cref="Step"/>.
        /// </summary>
        [Category("Format")]
        [Description("Text string format, default = \"" + DefaultTextStringFormat +
            "\". Example: \"Value: {0}\", \"Value: {0:0.###}\"." +
            "Where \"{0}\" used for \"" + nameof(Value) + "\" or \"" + nameof(SelectedItem) +
            "\", \"{1}\" for \"" + nameof(Minimum) + "\" or \"" + nameof(SelectedIndex) +
            "\", \"{2}\" for \"" + nameof(Maximum) + "\" or \"" + nameof(DefaultIndex) +
            "\", \"{3}\" for \"" + nameof(SliderMaximum) + "\", \"{4}\" for \"" + nameof(SliderMaximum) +
            "\", \"{5}\" for \"" + nameof(ValueDefault) + "\", \"{6}\" for \"" + nameof(Step) + "\".")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(DefaultTextStringFormat)]
        public string TextStringFormat
        {
            get { return textStringFormat; }
            set
            {
                textStringFormat = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Input text string format. Example: "", "0.######".
        /// </summary>
        [Category("Format")]
        [Description("Input text string format, default = \"" + DefaultTextInputStringFormat + "\".")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(DefaultTextInputStringFormat)]
        public string TextInputStringFormat
        {
            get { return textInputStringFormat; }
            set { textInputStringFormat = value; }
        }
        #endregion

        List<PointF> FixBlendProperties(List<PointF> value)
        {
            bool LoadDefault = (value == null); //false;

            if (value.Count < 2)
                LoadDefault = true;
            else
            {
                value.Sort((a, b) => a.Y.CompareTo(b.Y));
                LoadDefault |= (value[0].Y != 0f || value[value.Count - 1].Y != 1f);

                for (int i = 0; i < value.Count - 1; i++)
                    if (value[i].Y == value[i + 1].Y)
                        LoadDefault = true;
            }

            if (LoadDefault)
                value = new List<PointF>(DefaultBlend);

            return value;
        }

        void FixOrientation()
        {
            if (AutoOrientation)
                Orientation = (Width < Height ? Orientation.Vertical : Orientation.Horizontal);
        }
        #endregion

        #region Control Events
        #region ValueChanged
        /// <summary>
        /// Occurs when the <see cref="Value"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(Value) + "\" property has been changed in some way.")]
        public event EventHandler ValueChanged;

        /// <summary>
        /// Raises the <see cref="OnValueChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnValueChanged(EventArgs e)
        {
            ValueChanged?.Invoke(this, e);
        }
        #endregion

        #region ValueDefaultChanged
        /// <summary>
        /// Occurs when the <see cref="ValueDefault"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(ValueDefault) + "\" property has been changed in some way.")]
        public event EventHandler ValueDefaultChanged;

        /// <summary>
        /// Raises the <see cref="OnValueDefaultChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnValueDefaultChanged(EventArgs e)
        {
            ValueDefaultChanged?.Invoke(this, e);
        }
        #endregion

        #region ValueReset
        /// <summary>
        /// Occurs when the <see cref="Value"/> property has been reseted to <see cref="ValueDefault"/>.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(Value) + "\" property has been reseted to\"" +
            nameof(ValueDefault) + "\".")]
        public event EventHandler ValueReset;

        /// <summary>
        /// Raises the <see cref="OnValueReset"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnValueReset(EventArgs e)
        {
            ValueReset?.Invoke(this, e);
        }
        #endregion

        #region MaximumChanged
        /// <summary>
        /// Occurs when the <see cref="Maximum"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(Maximum) + "\" property has been changed in some way.")]
        public event EventHandler MaximumChanged;

        /// <summary>
        /// Raises the <see cref="OnMaximumChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnMaximumChanged(EventArgs e)
        {
            MaximumChanged?.Invoke(this, e);
        }
        #endregion

        #region MinimumChanged
        /// <summary>
        /// Occurs when the <see cref="Minimum"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(Minimum) + "\" property has been changed in some way.")]
        public event EventHandler MinimumChanged;

        /// <summary>
        /// Raises the <see cref="OnMinimumChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnMinimumChanged(EventArgs e)
        {
            MinimumChanged?.Invoke(this, e);
        }
        #endregion

        #region SliderMaximumChanged
        /// <summary>
        /// Occurs when the <see cref="SliderMaximum"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(SliderMaximum) + "\" property has been changed in some way.")]
        public event EventHandler SliderMaximumChanged;

        /// <summary>
        /// Raises the <see cref="OnSliderMaximumChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnSliderMaximumChanged(EventArgs e)
        {
            SliderMaximumChanged?.Invoke(this, e);
        }
        #endregion

        #region SliderMinimumChanged
        /// <summary>
        /// Occurs when the <see cref="SliderMinimum"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(SliderMinimum) + "\" property has been changed in some way.")]
        public event EventHandler SliderMinimumChanged;

        /// <summary>
        /// Raises the <see cref="OnSliderMinimumChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnSliderMinimumChanged(EventArgs e)
        {
            SliderMinimumChanged?.Invoke(this, e);
        }
        #endregion

        #region StepChanged
        /// <summary>
        /// Occurs when the <see cref="Step"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(Step) + "\" property has been changed in some way.")]
        public event EventHandler StepChanged;

        /// <summary>
        /// Raises the <see cref="OnStepChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnStepChanged(EventArgs e)
        {
            StepChanged?.Invoke(this, e);
        }
        #endregion

        #region ValuesArrayEnabledChanged
        /// <summary>
        /// Occurs when the <see cref="ArrayOfValuesEnabled"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(ArrayOfValuesEnabled) + "\" property has been changed in some way.")]
        public event EventHandler ArrayOfValuesEnabledChanged;

        /// <summary>
        /// Raises the <see cref="OnArrayOfValuesEnabledChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnArrayOfValuesEnabledChanged(EventArgs e)
        {
            ArrayOfValuesEnabledChanged?.Invoke(this, e);
        }
        #endregion

        #region ArrayOfValuesChanged
        /// <summary>
        /// Occurs when the <see cref="ArrayOfValues"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(ArrayOfValues) + "\" property has been changed in some way.")]
        public event EventHandler ArrayOfValuesChanged;

        /// <summary>
        /// Raises the <see cref="OnArrayOfValuesChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnArrayOfValuesChanged(EventArgs e)
        {
            ArrayOfValuesChanged?.Invoke(this, e);
        }
        #endregion

        #region SelectedIndexChanged
        /// <summary>
        /// Occurs when the <see cref="SelectedIndex"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(SelectedIndex) + "\" property has been changed in some way.")]
        public event EventHandler SelectedIndexChanged;

        /// <summary>
        /// Raises the <see cref="OnSelectedIndexChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnSelectedIndexChanged(EventArgs e)
        {
            SelectedIndexChanged?.Invoke(this, e);
        }
        #endregion

        #region DefaultIndexChanged
        /// <summary>
        /// Occurs when the <see cref="DefaultIndex"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(DefaultIndex) + "\" property has been changed in some way.")]
        public event EventHandler DefaultIndexChanged;

        /// <summary>
        /// Raises the <see cref="OnDefaultIndexChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnDefaultIndexChanged(EventArgs e)
        {
            DefaultIndexChanged?.Invoke(this, e);
        }
        #endregion

        #region SelectedIndexReset
        /// <summary>
        /// Occurs when the <see cref="SelectedIndex"/> property has been reseted to <see cref="DefaultIndex"/>.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(SelectedIndex) + "\" property has been reseted to\"" +
            nameof(DefaultIndex) + "\".")]
        public event EventHandler SelectedIndexReset;

        /// <summary>
        /// Raises the <see cref="OnSelectedIndexReset"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnSelectedIndexReset(EventArgs e)
        {
            SelectedIndexReset?.Invoke(this, e);
        }
        #endregion

        #region OrientationChanged
        /// <summary>
        /// Occurs when the <see cref="Orientation"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(Orientation) + "\" property has been changed in some way.")]
        public event EventHandler OrientationChanged;

        /// <summary>
        /// Raises the <see cref="OnOrientationChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnOrientationChanged(EventArgs e)
        {
            OrientationChanged?.Invoke(this, e);
        }
        #endregion

        #region InverseLayoutChanged
        /// <summary>
        /// Occurs when the <see cref="InverseLayout"/> property has been changed in some way.
        /// </summary>
        [Category("Action")]
        [Description("Occurs when the \"" + nameof(InverseLayout) + "\" property has been changed in some way.")]
        public event EventHandler InverseLayoutChanged;

        /// <summary>
        /// Raises the <see cref="OnInverseLayoutChanged"/> event.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnInverseLayoutChanged(EventArgs e)
        {
            InverseLayoutChanged?.Invoke(this, e);
        }
        #endregion
        #endregion

        #region TextInput Events
        private void TextInput_TextResize(object sender, EventArgs e)
        {
            Size S = TextRenderer.MeasureText(TextInput.Text + "12345", TextInput.Font);
            Size SMin = TextRenderer.MeasureText("123", TextInput.Font);
            TextInput.Width = S.Width + (Width - ClientSize.Width);
            TextInput.Width = Math.Min(Width, Math.Max(TextInput.Width, SMin.Width)); //Clamp Width
            TextInput.Height = Height;

            TextInput.Left = (Width - TextInput.Width) / 2;
            TextInput.Top = (Height - TextInput.Height) / 2;
        }

        private void TextInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (keyboardInput && (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape))
            {
                e.Handled = true; //Disable "Ding" sound
                e.SuppressKeyPress = true;

                if (e.KeyCode == Keys.Enter)
                {
                    //Parse Input Text
                    if (arrayOfValuesEnabled)
                        SelectedItem = TextInput.Text;
                    else
                    {
                        decimal Val;
                        if (decimal.TryParse(TextInput.Text, NumberStyles.Any, CultureInfo.CurrentCulture, out Val))
                        {
                            if (keyboardInputStep)
                            {
                                Val -= sliderMinValue;
                                decimal ModStep = (step > decimal.Zero ? Val % step : decimal.Zero);
                                Value = sliderMinValue + Val - ModStep + (ModStep >= step * 0.5M ? step : decimal.Zero);
                            }
                            else
                                Value = Val;
                        }
                    }
                }

                TextInput.Visible = false;
                Focus();
            }
        }

        private void TextInput_Leave(object sender, EventArgs e)
        {
            TextInput.Visible = false;
            Invalidate();
        }
        #endregion

        #region override void On...
        protected override void OnAutoSizeChanged(EventArgs e)
        {
            base.OnAutoSizeChanged(e);
            TextInput_TextResize(this, e);
            FixOrientation();
            Invalidate();
        }

        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            Invalidate();
        }

        protected override void OnBackgroundImageChanged(EventArgs e)
        {
            base.OnBackgroundImageChanged(e);
            Invalidate();
        }

        protected override void OnBackgroundImageLayoutChanged(EventArgs e)
        {
            base.OnBackgroundImageLayoutChanged(e);
            Invalidate();
        }

        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);
            TextInput_TextResize(this, e);
            FixOrientation();
            Invalidate();
        }

        protected override void OnDockChanged(EventArgs e)
        {
            base.OnDockChanged(e);
            FixOrientation();
            Invalidate();
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            TextInput.Visible = false;
            targeted = false;
            Invalidate();
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
            TextInput_TextResize(this, e);
            Invalidate();
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            base.OnForeColorChanged(e);
            Invalidate();
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            targeted = true;
            Invalidate();
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            targeted = false;
            Invalidate();
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);
            Invalidate();
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            targeted = true;
            Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            targeted = false;
            Invalidate();
        }

        protected override void OnMove(EventArgs e)
        {
            base.OnMove(e);
            Invalidate();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            TextInput_TextResize(this, e);
            FixOrientation();
            Invalidate();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            TextInput_TextResize(this, e);
            FixOrientation();
            Invalidate();
        }

        protected override void OnStyleChanged(EventArgs e)
        {
            base.OnStyleChanged(e);
            Invalidate();
        }

        ///////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Returns a string representation for this control.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            const string coma = ", ";
            const string colon = ": ";

            if (arrayOfValuesEnabled)
                return base.ToString() + coma + nameof(SelectedItem) + colon + SelectedItem.ToString(CultureInfo.CurrentCulture);
            else
            {
                return base.ToString() +
                    coma + nameof(Minimum) + colon + Minimum.ToString(CultureInfo.CurrentCulture) +
                    coma + nameof(Maximum) + colon + Maximum.ToString(CultureInfo.CurrentCulture) +
                    coma + nameof(Value) + colon + Value.ToString(CultureInfo.CurrentCulture);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (Focused && ArrowKeysInput &&
                (keyData == Keys.Left || keyData == Keys.Right || keyData == Keys.Up || keyData == Keys.Down))
            {
                if (arrayOfValuesEnabled)
                {
                    var Val = selectedIndex;

                    if (keyData == Keys.Left || keyData == Keys.Down)
                        Val += (InverseLayout ? 1 : -1);
                    else if (keyData == Keys.Right || keyData == Keys.Up)
                        Val += (InverseLayout ? -1 : 1);

                    if (arrayOfValues != null && Val >= 0 && Val < arrayOfValues.Length)
                        SelectedIndex = Val;
                }
                else if (step != decimal.Zero)
                {
                    var Val = currentValue;

                    if (keyData == Keys.Left || keyData == Keys.Down)
                        Val -= (InverseLayout ? -step : step);
                    else if (keyData == Keys.Right || keyData == Keys.Up)
                        Val += (InverseLayout ? -step : step);
                    Val = Math.Min(Val, (arrowKeysSliderRange ? sliderMaxValue : maxValue));
                    Value = Math.Max(Val, (arrowKeysSliderRange ? sliderMinValue : minValue));
                }

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (keyboardInput && e.KeyCode == Keys.Enter)
            {
                TextInput.Text = (ArrayOfValuesEnabled ? SelectedItem : Value.ToString(textInputStringFormat));
                TextInput.Visible = true;
                TextInput.Focus();
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (!resetValueOnDoubleClick && resetValueMouseButton != MouseButtons.None && e.Button == resetValueMouseButton)
            {
                if (arrayOfValuesEnabled)
                    ResetSelectedIndex();
                else
                    ResetValue();
            }
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (resetValueOnDoubleClick && resetValueMouseButton != MouseButtons.None && e.Button == resetValueMouseButton)
            {
                if (arrayOfValuesEnabled)
                    ResetSelectedIndex();
                else
                    ResetValue();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            TextInput_Leave(this, e);
            Focus();
            Mouse_Up_Down_Move(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            TextInput_Leave(this, e);
            Mouse_Up_Down_Move(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            Mouse_Up_Down_Move(e);
        }

        private void Mouse_Up_Down_Move(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                float Coef = (orientation == Orientation.Horizontal ? e.X / (float)Width : 1f - e.Y / (float)Height);
                Coef = Math.Max(Math.Min((inverseLayout ? 1f - Coef : Coef), 1f), 0f);

                if (arrayOfValuesEnabled)
                {
                    float Val = 0f;
                    if (arrayOfValues != null && arrayOfValues.Length > 0)
                        Val = (arrayOfValues.Length - 1) * Coef;
                    SelectedIndex = (halfStepShift && Val - (int)Val > 0.5f ? (int)Val + 1 : (int)Val);
                }
                else
                {
                    decimal Val = (sliderMaxValue - sliderMinValue) * (decimal)Coef;
                    decimal ModStep = (step > decimal.Zero ? Val % step : decimal.Zero);
                    Value = sliderMinValue + Val - ModStep + (halfStepShift && ModStep >= step * 0.5M ? step : decimal.Zero);
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.CompositingQuality = compositingQuality;
            e.Graphics.PixelOffsetMode = pixelOffsetMode;
            e.Graphics.SmoothingMode = smoothingMode;
            e.Graphics.TextRenderingHint = textRenderingHint;

            #region Value for Bar [0; 1]
            float BarValue = 0f;
            if (arrayOfValuesEnabled && arrayOfValues != null && arrayOfValues.Length > 0)
            {
                if (arrayOfValues.Length > 1)
                    BarValue = selectedIndex / (float)(arrayOfValues.Length - 1);
                else // valuesArray.Length == 1
                    BarValue = 1f;
            }
            else
            {
                float SliderMinimumF = (float)sliderMinValue;
                float SliderMaximumF = (float)sliderMaxValue;
                if (SliderMinimumF != SliderMaximumF)
                {
                    BarValue = ((float)currentValue - SliderMinimumF) / (SliderMaximumF - SliderMinimumF);
                    BarValue = Math.Max(Math.Min(BarValue, 1f), 0f);
                }
            }
            #endregion

            #region Colors
            Color BG_Color1, BG_Color2, BG_BorderColor1, BG_BorderColor2,
                Bar_Color1, Bar_Color2, Bar_BorderColor1, Bar_BorderColor2,
                Text_Color, Text_ShadowColor,
                Arrows_Color1, Arrows_Color2;

            if (Enabled)
            {
                if (TextInput.Focused && TextInput.CanFocus || Focused && CanFocus)
                {
                    BG_Color1 = bgColor1_Focused;
                    BG_Color2 = bgColor2_Focused;
                    BG_BorderColor1 = bgBorderColor1_Focused;
                    BG_BorderColor2 = bgBorderColor2_Focused;
                    Bar_Color1 = barColor1_Focused;
                    Bar_Color2 = barColor2_Focused;
                    Bar_BorderColor1 = barBorderColor1_Focused;
                    Bar_BorderColor2 = barBorderColor2_Focused;
                    Text_Color = textColor_Focused;
                    Text_ShadowColor = textShadowColor_Focused;
                    Arrows_Color1 = arrowsIconColor1_Focused;
                    Arrows_Color2 = arrowsIconColor2_Focused;
                }
                else
                {
                    if (targeted)
                    {
                        BG_Color1 = bgColor1_Targeted;
                        BG_Color2 = bgColor2_Targeted;
                        BG_BorderColor1 = bgBorderColor1_Targeted;
                        BG_BorderColor2 = bgBorderColor2_Targeted;
                        Bar_Color1 = barColor1_Targeted;
                        Bar_Color2 = barColor2_Targeted;
                        Bar_BorderColor1 = barBorderColor1_Targeted;
                        Bar_BorderColor2 = barBorderColor2_Targeted;
                        Text_Color = textColor_Targeted;
                        Text_ShadowColor = textShadowColor_Targeted;
                        Arrows_Color1 = arrowsIconColor1_Targeted;
                        Arrows_Color2 = arrowsIconColor2_Targeted;
                    }
                    else
                    {
                        BG_Color1 = bgColor1;
                        BG_Color2 = bgColor2;
                        BG_BorderColor1 = bgBorderColor1;
                        BG_BorderColor2 = bgBorderColor2;
                        Bar_Color1 = barColor1;
                        Bar_Color2 = barColor2;
                        Bar_BorderColor1 = barBorderColor1;
                        Bar_BorderColor2 = barBorderColor2;
                        Text_Color = textColor;
                        Text_ShadowColor = textShadowColor;
                        Arrows_Color1 = arrowsIconColor1;
                        Arrows_Color2 = arrowsIconColor2;
                    }
                }
            }
            else
            {
                BG_Color1 = bgColor1_Disabled;
                BG_Color2 = bgColor2_Disabled;
                BG_BorderColor1 = bgBorderColor1_Disabled;
                BG_BorderColor2 = bgBorderColor2_Disabled;
                Bar_Color1 = barColor1_Disabled;
                Bar_Color2 = barColor2_Disabled;
                Bar_BorderColor1 = barBorderColor1_Disabled;
                Bar_BorderColor2 = barBorderColor2_Disabled;
                Text_Color = textColor_Disabled;
                Text_ShadowColor = textShadowColor_Disabled;
                Arrows_Color1 = arrowsIconColor1_Disabled;
                Arrows_Color2 = arrowsIconColor2_Disabled;
            }
            #endregion

            #region BG
            // Background
            RectangleF ReBg = ClientRectangle;

            // Background Border
            RectangleF ReBgBorder = ReBg;
            float NegHalfBorderWidth = -bgBorderWidth * 0.5f;
            ReBgBorder.Inflate(NegHalfBorderWidth, NegHalfBorderWidth);
            bool Border = (bgBorderEnabled && bgBorderWidth > 0f);

            try
            {
                if (bgEnabled)
                {
                    if (Border)
                        ReBg.Inflate(NegHalfBorderWidth, NegHalfBorderWidth);

                    LinearGradientBrush b = new LinearGradientBrush(ReBg, BG_Color1, BG_Color2, bgGradientAngle);
                    b.Blend = GraphicsExt.FactorPosListToBlend(BgBlend_FactorPos);
                    b.GammaCorrection = gradientGammaCorrection;
                    GraphicsExt.FillRoundedRectangle(e.Graphics, b, ReBg, bgRoundRadius, bgRoundAngles);
                }
            }
            catch { }

            try
            {
                if (Border)
                {
                    LinearGradientBrush b = new LinearGradientBrush(ReBgBorder, BG_BorderColor1, BG_BorderColor2,
                        bgBorderGradientAngle);

                    b.Blend = GraphicsExt.FactorPosListToBlend(BgBorderBlend_FactorPos);
                    b.GammaCorrection = gradientGammaCorrection;

                    GraphicsExt.DrawRoundedRectangle(e.Graphics, new Pen(b, bgBorderWidth), ReBgBorder,
                        bgRoundRadius - bgBorderWidth * 0.5f, bgRoundAngles);
                }
            }
            catch { }
            #endregion

            #region Bar
            // Bar
            RectangleF ReBar = new RectangleF(
                ClientRectangle.X + barMargin.Left,
                ClientRectangle.Y + barMargin.Top,
                ClientRectangle.Width - barMargin.Right * 2f,
                ClientRectangle.Height - barMargin.Bottom * 2f);

            // Scale Bar and apply Orientation
            if (orientation == Orientation.Horizontal)
            {
                if (inverseLayout)
                {
                    float NewWidth = ReBar.Width * BarValue;
                    float dWidth = ReBar.Width - NewWidth;
                    ReBar.Offset(dWidth, 0f);
                    ReBar.Width = NewWidth;
                }
                else
                    ReBar.Width *= BarValue;
            }
            else
            {
                if (inverseLayout)
                    ReBar.Height *= BarValue;
                else
                {
                    float NewHeight = ReBar.Height * BarValue;
                    float dHeight = ReBar.Height - NewHeight;
                    ReBar.Offset(0f, dHeight);
                    ReBar.Height = NewHeight;
                }
            }

            // Bar Border
            RectangleF ReBarBorder = ReBar;
            NegHalfBorderWidth = -barBorderWidth * 0.5f;
            ReBarBorder.Inflate(NegHalfBorderWidth, NegHalfBorderWidth);
            Border = (barBorderEnabled && barBorderWidth > 0f);

            try
            {
                if (BarEnabled)
                {
                    if (Border)
                        ReBar.Inflate(NegHalfBorderWidth, NegHalfBorderWidth);

                    LinearGradientBrush b = new LinearGradientBrush(ReBar, Bar_Color1, Bar_Color2, barGradientAngle);
                    b.Blend = GraphicsExt.FactorPosListToBlend(BarBlend_FactorPos);
                    b.GammaCorrection = gradientGammaCorrection;

                    if (ReBar.Width >= 0f && ReBar.Height >= 0f)
                        GraphicsExt.FillRoundedRectangle(e.Graphics, b, ReBar, barRoundRadius, barRoundAngles);
                }
            }
            catch { }

            try
            {
                if (Border)
                {
                    LinearGradientBrush b = new LinearGradientBrush(ReBarBorder, Bar_BorderColor1,
                        Bar_BorderColor2, barBorderGradientAngle);

                    b.Blend = GraphicsExt.FactorPosListToBlend(BarBorderBlend_FactorPos);
                    b.GammaCorrection = gradientGammaCorrection;

                    if (ReBarBorder.Width >= 0f && ReBarBorder.Height >= 0f)
                        GraphicsExt.DrawRoundedRectangle(e.Graphics, new Pen(b, barBorderWidth), ReBarBorder,
                            barRoundRadius - barBorderWidth * 0.5f, barRoundAngles);
                }
            }
            catch { }
            #endregion

            #region Arrows
            RectangleF ReArrows = ReBg;
            float MinSide, MaxSide;
            if (orientation == Orientation.Horizontal)
            {
                MinSide = ReArrows.Height;
                MaxSide = ReArrows.Width;
            }
            else
            {
                MinSide = ReArrows.Width;
                MaxSide = ReArrows.Height;
            }

            if (arrowsIconEnabled && MaxSide - MinSide >= MinSide)
            {
                try
                {
                    float HalfMinSide = MinSide * 0.5f;
                    float MinSideScaled = HalfMinSide * arrowsIconScale;

                    if (orientation == Orientation.Horizontal)
                    {
                        if (inverseLayout && applyLayoutToArrows)
                            ReArrows = new RectangleF(ReArrows.X + HalfMinSide, ReArrows.Y + HalfMinSide, 0f, 0f);
                        else
                            ReArrows = new RectangleF(ReArrows.Right - HalfMinSide, ReArrows.Y + HalfMinSide, 0f, 0f);
                    }
                    else
                    {
                        if (inverseLayout && applyLayoutToArrows)
                            ReArrows = new RectangleF(ReArrows.X + HalfMinSide, ReArrows.Bottom - HalfMinSide, 0f, 0f);
                        else
                            ReArrows = new RectangleF(ReArrows.X + HalfMinSide, ReArrows.Y + HalfMinSide, 0f, 0f);
                    }
                    ReArrows.Inflate(MinSideScaled, MinSideScaled);
                    ReArrows.Offset(arrowsIconShift.Width, arrowsIconShift.Height);

                    PointF[] pointsTriangle1 = new PointF[] {
                        new PointF(ReArrows.Right, ReArrows.Y),
                        new PointF(ReArrows.Right, ReArrows.Y + ReArrows.Height * arrowsIconTrianglesSize),
                        new PointF(ReArrows.Right - ReArrows.Width * arrowsIconTrianglesSize, ReArrows.Y)
                    };

                    PointF[] pointsTriangle2 = new PointF[] {
                        new PointF(ReArrows.X, ReArrows.Bottom - ReArrows.Height * arrowsIconTrianglesSize),
                        new PointF(ReArrows.X, ReArrows.Bottom),
                        new PointF(ReArrows.X + ReArrows.Width * arrowsIconTrianglesSize, ReArrows.Bottom)
                    };

                    LinearGradientBrush b = new LinearGradientBrush(ReArrows, Arrows_Color1, Arrows_Color2,
                        arrowsIconGradientAngle);

                    b.Blend = GraphicsExt.FactorPosListToBlend(ArrowsIconBlend_FactorPos);
                    b.GammaCorrection = gradientGammaCorrection;

                    e.Graphics.FillPolygon(b, pointsTriangle1);
                    e.Graphics.FillPolygon(b, pointsTriangle2);
                }
                catch { }
            }
            #endregion

            #region Text
            try
            {
                if (textEnabled)
                {
                    float BW = (bgBorderEnabled ? bgBorderWidth : 0f);
                    float RminusHW = bgRoundRadius - BW * 0.5f;
                    float TextMargin = Math.Max(BW, BW + RminusHW - RminusHW / (float)Math.Sqrt(2f));
                    RectangleF ReText = new RectangleF(
                        ReBg.X + BarMargin.Left + TextMargin + textMargin.Left,
                        ReBg.Y + BarMargin.Top + TextMargin + textMargin.Top,
                        ReBg.Width - (BarMargin.Right + TextMargin + textMargin.Right) * 2f,
                        ReBg.Height - (BarMargin.Bottom + TextMargin + textMargin.Bottom) * 2f);

                    StringFormat strFormat = new StringFormat();
                    strFormat.Alignment = StringAlignment.Near;
                    strFormat.LineAlignment = StringAlignment.Center;
                    switch (textAlignment)
                    {
                        case ContentAlignment.TopLeft:
                            strFormat.Alignment = StringAlignment.Near;
                            strFormat.LineAlignment = StringAlignment.Near;
                            break;
                        case ContentAlignment.TopCenter:
                            strFormat.Alignment = StringAlignment.Center;
                            strFormat.LineAlignment = StringAlignment.Near;
                            break;
                        case ContentAlignment.TopRight:
                            strFormat.Alignment = StringAlignment.Far;
                            strFormat.LineAlignment = StringAlignment.Near;
                            break;
                        case ContentAlignment.MiddleLeft:
                            strFormat.Alignment = StringAlignment.Near;
                            strFormat.LineAlignment = StringAlignment.Center;
                            break;
                        case ContentAlignment.MiddleCenter:
                            strFormat.Alignment = StringAlignment.Center;
                            strFormat.LineAlignment = StringAlignment.Center;
                            break;
                        case ContentAlignment.MiddleRight:
                            strFormat.Alignment = StringAlignment.Far;
                            strFormat.LineAlignment = StringAlignment.Center;
                            break;
                        case ContentAlignment.BottomLeft:
                            strFormat.Alignment = StringAlignment.Near;
                            strFormat.LineAlignment = StringAlignment.Far;
                            break;
                        case ContentAlignment.BottomCenter:
                            strFormat.Alignment = StringAlignment.Center;
                            strFormat.LineAlignment = StringAlignment.Far;
                            break;
                        case ContentAlignment.BottomRight:
                            strFormat.Alignment = StringAlignment.Far;
                            strFormat.LineAlignment = StringAlignment.Far;
                            break;
                    }

                    if (textShadowEnabled)
                    {
                        RectangleF ReSh = ReText;
                        ReSh.X += textShadowOffset.Width;
                        ReSh.Y += textShadowOffset.Height;
                        e.Graphics.DrawString(Text, Font, new SolidBrush(Text_ShadowColor), ReSh, strFormat);
                    }
                    e.Graphics.DrawString(Text, Font, new SolidBrush(Text_Color), ReText, strFormat);
                }
            }
            catch { }
            #endregion
        }
        #endregion
    }
}