﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace NormalMap_Debugger
{
    public static class DLLResolver
    {
        public static void ResolveDLLsPatch()
        {
            const string NativeDLLsFolder = "NativeDLLs";
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string DLLsFolderPath = Path.Combine(path, NativeDLLsFolder);

            while (Math.Abs(Path.GetDirectoryName(path).Length - path.Length) > 1 && !Directory.Exists(DLLsFolderPath))
            {
                path = Path.GetDirectoryName(path);
                DLLsFolderPath = Path.Combine(path, NativeDLLsFolder);
            }

            if (Directory.Exists(DLLsFolderPath))
            {
                AddPathToEnvironmentVars(DLLsFolderPath);
                AddPathToEnvironmentVars(Path.Combine(DLLsFolderPath, IntPtr.Size == 4 ? "x86" : "x64"));
            }
        }

        private static void AddPathToEnvironmentVars(string DirectoryPath)
        {
            const string PathString = "Path";
            const char PathSeparator = ';';

            if (String.IsNullOrEmpty(DirectoryPath))
                throw new ArgumentNullException();

            string EnvPath = Environment.GetEnvironmentVariable(PathString);
            string[] Paths = EnvPath.Split(new char[] { PathSeparator }, StringSplitOptions.RemoveEmptyEntries);

            bool AddPath = true;
            foreach (string path in Paths)
            {
                if (Path.GetFullPath(path).ToLower() == Path.GetFullPath(DirectoryPath).ToLower())
                {
                    AddPath = false;
                    break;
                }
            }

            if (AddPath)
                Environment.SetEnvironmentVariable(PathString, EnvPath.TrimEnd(new char[] { PathSeparator }) + PathSeparator.ToString() + DirectoryPath);
        }
    }
}
