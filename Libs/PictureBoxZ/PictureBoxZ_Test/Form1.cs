﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PictureBoxZ_Test
{
    public partial class Form1 : Form
    {
        Bitmap bmp;

        public Form1()
        {
            InitializeComponent();
            bmp = new Bitmap("Test.png");
            pictureBoxZ1.Image = bmp;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
            else if (e.KeyCode == Keys.F1)
                pictureBoxZ1.Image = (pictureBoxZ1.Image == null ? bmp : null);
            else if (e.KeyCode == Keys.F2)
                pictureBoxZ1.LoadingCircleEnabled = !pictureBoxZ1.LoadingCircleEnabled;
        }
    }
}
