﻿using System;
using System.Collections.Generic;

namespace NormalMap_Debugger
{
    public static class Garbage
    {
        /// <summary>
        /// Dispose all elements and call <see cref="Clear()"/>.
        /// </summary>
        /// <typeparam name="T">Any IDisposable type.</typeparam>
        /// <param name="Obj">Objects for disposing.</param>
        public static void Clear<T>(params T[] Obj) where T : IDisposable
        {
            for (int i = 0; i < Obj.Length; i++)
                Obj?[i]?.Dispose();
            Clear();
        }

        /// <summary>
        /// Dispose all elements from the list and call <see cref="Clear()"/>.
        /// </summary>
        /// <typeparam name="T">Any IDisposable type.</typeparam>
        /// <param name="ObjList">List with objects for disposing.</param>
        public static void Clear<T>(List<T> ObjList) where T : IDisposable
        {
            if (ObjList != null)
            {
                for (int i = 0; i < ObjList.Count; i++)
                    ObjList[i]?.Dispose();
            }
            Clear();
        }

        /// <summary>
        /// Dispose a range of elements from the array and call <see cref="Clear()"/>.
        /// </summary>
        /// <typeparam name="T">Any IDisposable type.</typeparam>
        /// <param name="Obj">Objects for disposing.</param>
        /// <param name="Index">The zero-based starting index of the range of elements to dispose.</param>
        /// <param name="Count">The number of elements to dispose.</param>
        public static void Clear<T>(T[] Obj, int Index, int Count) where T : IDisposable
        {
            if (Obj != null)
            {
                for (int i = Index; i < Index + Count; i++)
                    Obj[i]?.Dispose();
            }
            Clear();
        }

        /// <summary>
        /// Dispose a range of elements from the list and call <see cref="Clear()"/>.
        /// </summary>
        /// <typeparam name="T">Any IDisposable type.</typeparam>
        /// <param name="Obj">Objects for disposing.</param>
        /// <param name="Index">The zero-based starting index of the range of elements to dispose.</param>
        /// <param name="Count">The number of elements to dispose.</param>
        public static void Clear<T>(List<T> Obj, int Index, int Count) where T : IDisposable
        {
            if (Obj != null)
            {
                for (int i = Index; i < Index + Count; i++)
                    Obj[i]?.Dispose();
            }
            Clear();
        }

        /// <summary>
        /// Collect and clear unused Disposable resources.
        /// </summary>
        public static void Clear()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
