﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using OpenCvSharp;

namespace NormalMap_Debugger
{
    public enum Channels
    {
        R = 2,
        G = 1,
        B = 0,
        A = 3
    }

    public enum BlendModes
    {
        Normal,
        //Dissolve,

        DarkenOnly,
        Multiply,
        ColorBurn,
        LinearBurn,
        DarkerColor,

        LightenOnly,
        Screen,
        ColorDodge,
        LinearDodge_Add,
        LighterColor,

        Overlay,
        SoftLight,
        HardLight,
        VividLight,
        LinearLight,
        PinLight,
        HardMix,

        Difference,
        Exclusion,
        Subtract,
        Divide,

        Hue,
        Saturation,
        Color,
        Luminosity,

        /// <summary>
        /// This inverts the lower layer before subtracting it from the upper layer.
        /// </summary>
        InverseSubtract,
        /// <summary>
        /// Blends the upper layer as half-transparent with the lower.
        /// (It add the two layers together and then halves the value).
        /// </summary>
        Allanon,
        /// <summary>
        /// This blending mode multiplies the top layer with the bottom, and then outputs the square root of that.
        /// </summary>
        GeometricMean,
        /// <summary>
        /// Similar to subtract, the colors of the upper layer are subtracted from the
        /// colors of the lower layer, and then 50% grey is added.
        /// </summary>
        GrainExtract,
        /// <summary>
        /// Similar to addition, the colors of the upper layer are added to the colors,
        /// and then 50% grey is subtracted.
        /// </summary>
        GrainMerge,
        /// <summary>
        /// Subtract the square root of the lower layer from the upper layer.
        /// </summary>
        AdditiveSubtractive,
    }

    public static class OpenCV_Operation
    {
        /// <summary>
        /// Clamp Mat values to some range.
        /// </summary>
        /// <param name="Image">Some Mat.</param>
        /// <param name="Min">Minimum value.</param>
        /// <param name="Max">Maximum value.</param>
        /// <returns>Clamped Image (Mat).</returns>
        public static Mat Clamp(Mat Image, double Min = 0.0, double Max = 1.0)
        {
            if (Image == null)
                throw new ArgumentNullException(nameof(Image) + " is null!");

            Mat Result = new Mat(Image.Size(), Image.Type());
            Mat[] MatBGR = new Mat[Image.Channels()];

            for (int i = 0; i < MatBGR.Length; i++)
            {
                MatBGR[i] = Image.ExtractChannel(i);
                Mat Ch = new Mat(MatBGR[i].Size(), MatBGR[i].Type());
                Cv2.Min(MatBGR[i], Max, Ch);
                Cv2.Max(Ch, Min, MatBGR[i]);
                Garbage.Clear(Ch); // CleanUp
            }

            Cv2.Merge(MatBGR, Result);
            Garbage.Clear(MatBGR); // CleanUp

            return Result;
        }

        /// <summary>
        /// Invert image colors.
        /// </summary>
        /// <param name="Image">Input image.</param>
        /// <returns>Inverted image.</returns>
        public static Mat Invert(Mat Image)
        {
            if (Image == null)
                throw new ArgumentNullException(nameof(Image) + " is null!");

            Mat Result = new Mat(Image.Size(), Image.Type());

            if (Image.Type().Depth % 2 == 0)
                Cv2.BitwiseNot(Image, Result);
            else
            {
                double Min, Max;
                Image.MinMaxIdx(out Min, out Max);

                Mat[] MatBGR = new Mat[Image.Channels()];

                for (int i = 0; i < MatBGR.Length; i++)
                {
                    MatBGR[i] = Image.ExtractChannel(i);
                    MatBGR[i] = Max + Min - MatBGR[i];
                }

                Cv2.Merge(MatBGR, Result);
                Garbage.Clear(MatBGR); // CleanUp
            }

            return Result;
        }

        /// <summary>
        /// Invert channel in Image.
        /// </summary>
        /// <param name="Image">Input Image.</param>
        /// <param name="ChannelNumber">Channel number for inverting.</param>
        /// <returns>Image with inverted channel.</returns>
        public static Mat Invert(Mat Image, Channels ChannelNumber)
        {
            if (Image == null)
                throw new ArgumentNullException(nameof(Image) + " is null!");

            if (ChannelNumber < 0 || (int)ChannelNumber >= Image.Channels())
                throw new ArgumentOutOfRangeException(nameof(ChannelNumber) + " has wrong value!");

            Mat Result = new Mat(Image.Size(), Image.Type());

            Mat[] MatBGR = new Mat[Image.Channels()];
            for (int i = 0; i < MatBGR.Length; i++)
                MatBGR[i] = Image.ExtractChannel(i);

            if (MatBGR[(int)ChannelNumber].Type().Depth % 2 == 0)
                Cv2.BitwiseNot(MatBGR[(int)ChannelNumber], MatBGR[(int)ChannelNumber]);
            else
            {
                double Min, Max;
                Image.MinMaxIdx(out Min, out Max);

                MatBGR[(int)ChannelNumber] = Min + Max - MatBGR[(int)ChannelNumber];
            }

            Cv2.Merge(MatBGR, Result);
            Garbage.Clear(MatBGR); // CleanUp

            return Result;
        }

        /// <summary>
        /// Fill(clean) channel of image with some values, default = 0.
        /// </summary>
        /// <param name="Image">Input image.</param>
        /// <param name="ChannelNumber">Channel for filling.</param>
        /// <param name="Value">Filling value, default is 0.</param>
        /// <returns>Image with filled channel.</returns>
        public static Mat FillChannel(Mat Image, Channels ChannelNumber, int Value = 0)
        {
            Mat[] MatBGR = new Mat[Image.Channels()];
            for (int i = 0; i < MatBGR.Length; i++)
                MatBGR[i] = Image.ExtractChannel(i);

            MatBGR[(int)ChannelNumber].SetTo(Value);
            Cv2.Merge(MatBGR, Image);
            Garbage.Clear(MatBGR); //CleanUp

            return Image;
        }

        /// <summary>
        /// Get RGB image with some channel in R, G, B channels.
        /// </summary>
        /// <param name="Image">Input image.</param>
        /// <param name="ChannelNumber">Some channel.</param>
        /// <returns>RGB image with some channel in R, G, B channels.</returns>
        public static Mat SelectChannel(Mat Image, Channels ChannelNumber)
        {
            Mat[] MatBGR = new Mat[Image.Channels()];

            for (int i = 0; i < Math.Max(MatBGR.Length, 3); i++)
                MatBGR[i] = Image.ExtractChannel((int)ChannelNumber);

            if (MatBGR.Length >= 4)
                MatBGR[MatBGR.Length - 1] = Image.ExtractChannel(MatBGR.Length - 1);

            Cv2.Merge(MatBGR, Image);
            Garbage.Clear(MatBGR); //CleanUp

            return Image;
        }

        /// <summary>
        /// Swap channels in image.
        /// </summary>
        /// <param name="Image">Input image.</param>
        /// <param name="Channel1">First channel.</param>
        /// <param name="Channel2">Second channel.</param>
        /// <returns>Image with swapped channels.</returns>
        public static Mat SwapChannels(Mat Image, Channels Channel1, Channels Channel2)
        {
            if (Channel1 == Channel2)
                return Image;

            Mat[] MatBGR = new Mat[Image.Channels()];
            for (int i = 0; i < MatBGR.Length; i++)
            {
                int j = i;
                if (i == (int)Channel1)
                    j = (int)Channel2;
                else if (i == (int)Channel2)
                    j = (int)Channel1;

                MatBGR[i] = Image.ExtractChannel(j);
            }

            Cv2.Merge(MatBGR, Image);
            Garbage.Clear(MatBGR); //CleanUp

            return Image;
        }

        /// <summary>
        /// Flip and/or Rotate Narmal Map.
        /// </summary>
        /// <param name="Image">Normal Map image.</param>
        /// <param name="RotateFlip">Flip and/or Rotate direction.</param>
        /// <param name="InvY">Is Y (Green channel) is negative? Used only for ±90° rotation.</param>
        /// <returns></returns>
        public static Mat FlipRotate(Mat Image, RotateFlipType RotateFlip, bool InvY = false)
        {
            switch (RotateFlip)
            {
                case RotateFlipType.RotateNoneFlipNone:
                default:
                    return Image;

                case RotateFlipType.Rotate90FlipNone:
                    return SwapChannels(Invert(Image.Transpose().Flip(FlipMode.Y),
                        (InvY ? Channels.G : Channels.R)), Channels.R, Channels.G);

                case RotateFlipType.Rotate180FlipNone:
                    return Invert(Invert(Image.Flip(FlipMode.XY), Channels.R), Channels.G);

                case RotateFlipType.Rotate270FlipNone:
                    return SwapChannels(Invert(Image.Transpose().Flip(FlipMode.X),
                        (InvY ? Channels.R : Channels.G)), Channels.R, Channels.G);

                case RotateFlipType.RotateNoneFlipX:
                    return Invert(Image.Flip(FlipMode.Y), Channels.R);

                case RotateFlipType.Rotate90FlipX:
                    return SwapChannels(Image.Transpose(), Channels.R, Channels.G);

                case RotateFlipType.RotateNoneFlipY:
                    return Invert(Image.Flip(FlipMode.X), Channels.G);

                case RotateFlipType.Rotate90FlipY:
                    return SwapChannels(Image.Transpose(), Channels.R, Channels.G);
            }
        }

        /// <summary>
        /// Restore Blue channel in NormalMap from Red and Green.
        /// </summary>
        /// <param name="Image">NormalMap with wrong blue channel.</param>
        /// <returns>NormalMap with recovered blue channel.</returns>
        public static Mat RestoreBlueChannel(Mat Image)
        {
            var ResultType = Image.Type();

            // To float image [Image / 255 * 2 - 1] => [-1; 1]
            Mat ImageFloat = new Mat(Image.Size(), MatType.CV_32FC3);
            Image.ConvertTo(ImageFloat, ImageFloat.Type(), 1.0 / 127.5, -1.0);
            Garbage.Clear(Image); //CleanUp

            Mat[] MatBGR = new Mat[ImageFloat.Channels()];
            for (int i = 0; i < MatBGR.Length; i++)
                MatBGR[i] = ImageFloat.ExtractChannel(i);

            const int X = 2, Y = 1, Z = 0;
            MatBGR[Z] = 1.0 - MatBGR[X].Pow(2.0) - MatBGR[Y].Pow(2.0);
            Cv2.Max(MatBGR[Z], 0.0, MatBGR[Z]);
            MatBGR[Z] = MatBGR[Z].Pow(0.5);

            // Normalize Vector [Vector.Length ≡ 1]
            ImageFloat = MatBGR[X].Pow(2.0) + MatBGR[Y].Pow(2.0) + MatBGR[Z].Pow(2.0);
            ImageFloat = ImageFloat.Pow(0.5); // Vector length
            for (int i = 0; i < MatBGR.Length; i++)
                MatBGR[i] /= ImageFloat;

            // To 8-bit image [(Vector / 2 + 0.5) * 255] => [0; 255]
            Cv2.Merge(MatBGR, ImageFloat);
            Garbage.Clear(MatBGR); //CleanUp

            Mat Result = new Mat(ImageFloat.Size(), ResultType);
            ImageFloat.ConvertTo(Result, Result.Type(), 127.5, 127.5);
            Garbage.Clear(ImageFloat); //CleanUp

            return Result;
        }

        /// <summary>
        /// Normalize NormalMap.
        /// </summary>
        /// <param name="Image">Input image.</param>
        /// <returns>Normalized NormalMap.</returns>
        public static Mat Normalize(Mat Image)
        {
            var ResultType = Image.Type();

            // To float image [Image / 255 * 2 - 1] => [-1; 1]
            Mat ImageFloat = new Mat(Image.Size(), MatType.CV_32FC3);
            Image.ConvertTo(ImageFloat, ImageFloat.Type(), 1.0 / 127.5, -1.0);
            Garbage.Clear(Image); //CleanUp

            Mat[] MatBGR = new Mat[ImageFloat.Channels()];
            for (int i = 0; i < MatBGR.Length; i++)
                MatBGR[i] = ImageFloat.ExtractChannel(i);

            const int X = 2, Y = 1, Z = 0;

            // Normalize Vector [Vector.Length ≡ 1]
            ImageFloat = MatBGR[X].Pow(2.0) + MatBGR[Y].Pow(2.0) + MatBGR[Z].Pow(2.0);
            ImageFloat = ImageFloat.Pow(0.5); // Vector length
            for (int i = 0; i < MatBGR.Length; i++)
                MatBGR[i] /= ImageFloat;

            // To 8-bit image [(Vector / 2 + 0.5) * 255] => [0; 255]
            Cv2.Merge(MatBGR, ImageFloat);
            Garbage.Clear(MatBGR); //CleanUp

            Mat Result = new Mat(ImageFloat.Size(), ResultType);
            ImageFloat.ConvertTo(Result, Result.Type(), 127.5, 127.5);
            Garbage.Clear(ImageFloat); //CleanUp

            return Result;
        }

        /// <summary>
        /// Get image with NormalMap bad pixels (Red color).
        /// </summary>
        /// <param name="Image">Input image (NormalMap).</param>
        /// <param name="Threshold">NormalMap checking threshold.</param>
        /// <returns>Image with bad pixels(red color), or fully black image, if NormalMap is OK.</returns>
        public static Mat GetBadPixels(Mat Image, double Threshold = 0.02)
        {
            var ResultType = Image.Type();

            // To float image [Image / 255 * 2 - 1] => [-1; 1]
            Mat ImageFloat = new Mat(Image.Size(), MatType.CV_32FC3);
            Image.ConvertTo(ImageFloat, ImageFloat.Type(), 1.0 / 255.0 * 2.0, -1.0);
            Garbage.Clear(Image); //CleanUp

            Mat[] MatBGR = new Mat[ImageFloat.Channels()];
            for (int i = 0; i < MatBGR.Length; i++)
                MatBGR[i] = ImageFloat.ExtractChannel(i);

            const int X = 2, Y = 1, Z = 0;

            // Get BadPixels
            MatBGR[Y] = MatBGR[X].Pow(2.0) + MatBGR[Y].Pow(2.0) + MatBGR[Z].Pow(2.0);
            MatBGR[Z] = MatBGR[Y].Pow(0.5) - 1.0;
            MatBGR[Y] = MatBGR[Z].Abs();
            MatBGR[X] = MatBGR[Y].Threshold(Threshold, 1.0, ThresholdTypes.Binary);
            MatBGR[Y].SetTo(0.0); //Clear G
            MatBGR[Z].SetTo(0.0); //Clear B

            // To 8-bit image [(Vector / 2 + 0.5) * 255] => [0; 255]
            Cv2.Merge(MatBGR, ImageFloat);
            Garbage.Clear(MatBGR); //CleanUp

            Mat Result = new Mat(ImageFloat.Size(), ResultType);
            ImageFloat.ConvertTo(Result, Result.Type(), 255.0);
            Garbage.Clear(ImageFloat); //CleanUp

            return Result;
        }

        /// <summary>
        /// Generate NormalMap from image.
        /// </summary>
        /// <param name="Img">Input image.</param>
        /// <param name="Kernel">Kernel size (3 - 3x3, 5 - 5x5, 7 - 7x7 ...).</param>
        /// <param name="Scale">Scale multiplier, default must be 1.</param>
        /// <param name="InvX">Use horizontal inversion?</param>
        /// <param name="InvY">Use vertical inversion?</param>
        /// <returns></returns>
        public static Mat GenerateNormalMap(Mat Img, int Kernel = 0, double Scale = 1, bool InvX = false, bool InvY = false)
        {
            Img = Img.CvtColor(ColorConversionCodes.BGR2GRAY); //To Grayscale
            Mat ImageFloat = new Mat(Img.Size(), MatType.CV_32FC1);
            Img.ConvertTo(ImageFloat, ImageFloat.Type(), 1.0 / 255.0);
            Garbage.Clear(Img); //CleanUp

            //Sobel gradient kernels
            double[,] SobelGx, SobelGy;
            CreateSobelKernel(Kernel, out SobelGx, out SobelGy, true);
            MultMatrix(ref SobelGx, (InvX ? Scale : -Scale));
            MultMatrix(ref SobelGy, (InvY ? -Scale : Scale));

            Mat X = ImageFloat.Filter2D(ImageFloat.Type(), InputArray.Create(SobelGx));
            Mat Y = ImageFloat.Filter2D(ImageFloat.Type(), InputArray.Create(SobelGy));

            double K = 1.0 / Math.Sqrt(2.0);
            X = X * K;
            Y = Y * K;

            // Z
            ImageFloat = 1.0 - X.Pow(2.0) - Y.Pow(2.0);
            ImageFloat = ImageFloat.Pow(0.5);

            Mat Result = new Mat(ImageFloat.Size(), MatType.CV_32FC3);
            Cv2.Merge(new Mat[] { ImageFloat, Y, X }, Result);
            Garbage.Clear(X, Y, ImageFloat); //CleanUp

            Result.ConvertTo(Result, MatType.CV_8UC3, 127.5, 127.5);
            return Result;
        }

        #region Functions for NormalMap Genegation
        /// <summary>
        /// Create Sobel Kernel. Side must be 3, 5, 7... (3x3, 5x5, 7x7 ...).
        /// Maked from: http://stackoverflow.com/a/36177539/5849511
        /// </summary>
        /// <param name="Side">Kernel size (3 - 3x3, 5 - 5x5, 7 - 7x7 ...).</param>
        /// <param name="Gx">Horizontal gradient kernel.</param>
        /// <param name="Gy">Vertical gradient kernel.</param>
        /// <param name="Equalize">Equalize kernel, if true.</param>
        static void CreateSobelKernel(int Side, out double[,] Gx, out double[,] Gy, bool Equalize = false)
        {
            int HalfSide = Side / 2;

            Gx = new double[Side, Side];
            Gy = new double[Side, Side];

            for (int i = 0; i < Side; i++)
            {
                int k = (i <= HalfSide) ? (HalfSide + i) : (Side + HalfSide - i - 1);

                for (int j = 0; j < Side; j++)
                {
                    if (j < HalfSide)
                        Gx[i, j] = Gy[j, i] = j - k;
                    else if (j > HalfSide)
                        Gx[i, j] = Gy[j, i] = k - (Side - j - 1);
                    else
                        Gx[i, j] = Gy[j, i] = 0;
                }
            }

            if (Equalize)
            {
                EqualizeKernel(Gx);
                EqualizeKernel(Gy);
            }
        }

        /// <summary>
        /// Equalize Kernel (Divide Matrix by absolute summ of matrix values).
        /// </summary>
        /// <param name="Matrix">Equalized kernel.</param>
        static void EqualizeKernel(double[,] Matrix)
        {
            if (Matrix == null)
                throw new ArgumentNullException();

            double Koef = 0f;
            for (int i = 0; i < Matrix.GetLength(0); i++)
                for (int j = 0; j < Matrix.GetLength(1); j++)
                    Koef += Math.Abs(Matrix[i, j]); //Calc R
            Koef = 1.0 / Koef; // A = 1/R * [matrix]

            MultMatrix(ref Matrix, Koef);
        }

        /// <summary>
        /// Multiply matrix by value.
        /// </summary>
        /// <param name="Matrix">Input Matrix.</param>
        /// <param name="Value">Input Value.</param>
        static void MultMatrix(ref double[,] Matrix, double Value)
        {
            if (Matrix == null)
                throw new ArgumentNullException();

            for (int i = 0; i < Matrix.GetLength(0); i++)
                for (int j = 0; j < Matrix.GetLength(1); j++)
                    Matrix[i, j] *= Value;
        }
        #endregion

        /// <summary>
        /// Blend Images with some blend mode.
        /// </summary>
        /// <param name="Image1">First Image (like Top Layer in Photoshop).</param>
        /// <param name="Image2">Second Image (like Bottom Layer in Photoshop).</param>
        /// <param name="BlendMode">Blend Mode, not all of them are equivalent to Photoshop!</param>
        /// <returns>Blended image, in many times is will be normalized to range 0..1 or 0..255.</returns>
        public static Mat Blend(Mat Image1, Mat Image2, BlendModes BlendMode)
        {
            if (Image1 == null)
                throw new ArgumentNullException(nameof(Image1) + " is null!");
            if (Image2 == null)
                throw new ArgumentNullException(nameof(Image2) + " is null!");
            if (Image1.Size() != Image2.Size())
                throw new ArgumentException("\"" + nameof(Image1.Size) + "\" != \"" + nameof(Image2.Size) + "\"!");

            var ResultType = Image1.Type();

            double ConvertCoefA = 1.0;
            double ConvertCoefB = 0.0;
            switch (ResultType.Depth)
            {
                case MatType.CV_8U:
                    ConvertCoefA = byte.MaxValue;
                    ConvertCoefB = byte.MinValue;
                    break;
                case MatType.CV_8S:
                    break;
                case MatType.CV_16U:
                    ConvertCoefA = short.MaxValue;
                    ConvertCoefB = short.MinValue;
                    break;
                case MatType.CV_16S:
                    break;
                case MatType.CV_32S:
                    break;
                case MatType.CV_32F:
                case MatType.CV_64F:
                default:
                    break;
            }

            // To float image [Image / Depth] => [0; 1]
            Mat Img1 = new Mat(Image1.Size(), MatType.CV_32FC(Image1.Channels()));
            Mat Img2 = Img1.EmptyClone();
            Image1.ConvertTo(Img1, Img1.Type(), 1.0 / ConvertCoefA, ConvertCoefB);
            Image2.ConvertTo(Img2, Img2.Type(), 1.0 / ConvertCoefA, ConvertCoefB);

            switch (BlendMode)
            {
                case BlendModes.Normal:
                    break;

                //case BlendModes.Dissolve:
                //    break;

                #region DarkenOnly
                case BlendModes.DarkenOnly:
                    Cv2.Min(Img1, Img2, Img1);
                    break;
                #endregion

                #region Multiply
                case BlendModes.Multiply:
                    Cv2.Multiply(Img1, Img2, Img1);
                    break;
                #endregion

                #region ColorBurn
                case BlendModes.ColorBurn:
                    Mat ColorBurn_Inv = Invert(Img2);
                    Mat ColorBurn_Div = Blend(Img1, ColorBurn_Inv, BlendModes.Divide);
                    Garbage.Clear(ColorBurn_Inv); // CleanUp
                    Img1 = Invert(ColorBurn_Div);
                    Garbage.Clear(ColorBurn_Div); // CleanUp
                    break;
                #endregion

                #region LinearBurn
                case BlendModes.LinearBurn:
                    Mat LinearBurn_Res = Img1.EmptyClone();
                    Cv2.AddWeighted(Img1, 1.0, Img2, 1.0, -1.0, LinearBurn_Res);
                    Img1 = Clamp(LinearBurn_Res);
                    Garbage.Clear(LinearBurn_Res); // ClearUp
                    break;
                #endregion

                #region DarkerColor
                case BlendModes.DarkerColor:
                    Mat DarkerColor_V1 = Img1.CvtColor(ColorConversionCodes.BGR2GRAY);
                    Mat DarkerColor_V2 = Img2.CvtColor(ColorConversionCodes.BGR2GRAY);

                    Mat DarkerColor_Min = new Mat(Img1.Size(), MatType.CV_8UC1);
                    Cv2.Compare(DarkerColor_V1, DarkerColor_V2, DarkerColor_Min, CmpTypes.GE);
                    Garbage.Clear(DarkerColor_V1, DarkerColor_V2); // CleanUp

                    Cv2.Subtract(Img1, Img1, Img1, DarkerColor_Min);
                    Cv2.Add(Img1, Img2, Img1, DarkerColor_Min);
                    Garbage.Clear(DarkerColor_Min); // CleanUp
                    break;
                #endregion

                #region LightenOnly
                case BlendModes.LightenOnly:
                    Cv2.Max(Img1, Img2, Img1);
                    break;
                #endregion

                #region Screen
                case BlendModes.Screen:
                    Mat Screen_Mult = Img1.EmptyClone();
                    Mat Screen_Add = Img1.EmptyClone();
                    Cv2.Multiply(Img1, Img2, Screen_Mult);
                    Cv2.Add(Img1, Img2, Screen_Add);
                    Cv2.Subtract(Screen_Add, Screen_Mult, Img1);
                    Garbage.Clear(Screen_Mult, Screen_Add); //CleanUp
                    break;
                #endregion

                #region ColorDodge
                case BlendModes.ColorDodge:
                    Mat ColorDodge_Inv = Invert(Img1);
                    Img1 = Blend(ColorDodge_Inv, Img2, BlendModes.Divide);
                    Garbage.Clear(ColorDodge_Inv); // CleanUp
                    break;
                #endregion

                #region LinearDodge (Add)
                case BlendModes.LinearDodge_Add:
                    Mat LinearDodge_Res = Img1.EmptyClone();
                    Cv2.Add(Img1, Img2, LinearDodge_Res);
                    Img1 = Clamp(LinearDodge_Res);
                    Garbage.Clear(LinearDodge_Res); //CleanUp
                    break;
                #endregion

                #region LighterColor
                case BlendModes.LighterColor:
                    Mat LighterColor_V1 = Img1.CvtColor(ColorConversionCodes.BGR2GRAY);
                    Mat LighterColor_V2 = Img2.CvtColor(ColorConversionCodes.BGR2GRAY);

                    Mat LighterColor_Max = new Mat(Img1.Size(), MatType.CV_8UC1);
                    Cv2.Compare(LighterColor_V1, LighterColor_V2, LighterColor_Max, CmpTypes.LE);
                    Garbage.Clear(LighterColor_V1, LighterColor_V2); // CleanUp

                    Cv2.Subtract(Img1, Img1, Img1, LighterColor_Max);
                    Cv2.Add(Img1, Img2, Img1, LighterColor_Max);
                    Garbage.Clear(LighterColor_Max); // CleanUp
                    break;
                #endregion

                #region Overlay
                case BlendModes.Overlay:
                    Mat Overlay_MaskGr = Img2.EmptyClone();
                    Mat Overlay_MaskSm = Overlay_MaskGr.EmptyClone();
                    Mat Overlay_Greater = Overlay_MaskGr.EmptyClone();
                    Mat Overlay_Smaller = Overlay_MaskGr.EmptyClone();
                    Cv2.Threshold(Img2, Overlay_MaskGr, 0.5, 1.0, ThresholdTypes.Binary);
                    Cv2.Threshold(Img2, Overlay_MaskSm, 0.5, 1.0, ThresholdTypes.BinaryInv);

                    // Greater (Target > 0.5) * (2B + 2(T - BT) - 1)
                    Mat Overlay_Var = Img1.EmptyClone();
                    Cv2.Multiply(Img1, Img2, Overlay_Var);
                    Cv2.Subtract(Img2, Overlay_Var, Overlay_Var);
                    Cv2.AddWeighted(Overlay_Var, 2.0, Img1, 2.0, -1.0, Overlay_Var);
                    Cv2.Multiply(Overlay_Var, Overlay_MaskGr, Overlay_Greater);
                    Garbage.Clear(Overlay_MaskGr, Overlay_Var); //CleanUp

                    // Smaller (Target <= 0.5) * (2TB)
                    Cv2.Multiply(Img2, Img1, Overlay_Smaller, 2.0);
                    Cv2.Multiply(Overlay_Smaller, Overlay_MaskSm, Overlay_Smaller);
                    Garbage.Clear(Overlay_MaskSm); //CleanUp

                    // Add it together
                    Cv2.Add(Overlay_Smaller, Overlay_Greater, Img1);
                    Garbage.Clear(Overlay_Smaller, Overlay_Greater); //CleanUp
                    break;
                #endregion

                #region SoftLight
                case BlendModes.SoftLight:
                    Mat SoftLight_2AB = Img1.EmptyClone();
                    Cv2.Multiply(Img1, Img2, SoftLight_2AB, 2.0);
                    Cv2.AddWeighted(Img1, -1.0, Img1, -1.0, 1.0, Img1);
                    Cv2.Pow(Img2, 2.0, Img2);
                    Cv2.Multiply(Img1, Img2, Img2);
                    Cv2.Add(Img2, SoftLight_2AB, Img1);
                    Garbage.Clear(SoftLight_2AB); // CleanUp
                    break;
                #endregion

                #region HardLight
                case BlendModes.HardLight:
                    Img1 = Blend(Img2, Img1, BlendModes.Overlay);
                    break;
                #endregion

                // Not equal Photoshop
                #region VividLight
                case BlendModes.VividLight:
                    Mat VividLight_MaskGr = Img1.EmptyClone();
                    Mat VividLight_MaskSm = VividLight_MaskGr.EmptyClone();
                    Cv2.Threshold(Img1, VividLight_MaskGr, 0.5, 1.0, ThresholdTypes.Binary);
                    Cv2.Threshold(Img1, VividLight_MaskSm, 0.5, 1.0, ThresholdTypes.BinaryInv);

                    Mat VividLight_V1 = Img1.EmptyClone();
                    Cv2.Add(Img1, Img1, Img1);

                    // Smaller (Blend <= 0.5) * (T / (1 - 2B))
                    Mat VividLight_Smaller = Blend(Img1, Img2, BlendModes.ColorBurn);
                    Cv2.Multiply(VividLight_Smaller, VividLight_MaskSm, VividLight_Smaller);
                    Garbage.Clear(VividLight_MaskSm); //CleanUp

                    // Greater (Blend > 0.5) * (1 - (1 - T) / (2(B - 0.5)))
                    Cv2.AddWeighted(Img1, 0.5, Img1, 0.5, -1.0, Img1);
                    VividLight_V1 = Clamp(Img1);
                    Mat VividLight_Greater = Blend(VividLight_V1, Img2, BlendModes.ColorDodge);
                    Cv2.Multiply(VividLight_Greater, VividLight_MaskGr, VividLight_Greater);
                    Garbage.Clear(VividLight_MaskGr, VividLight_V1); //CleanUp

                    // Add it together
                    Cv2.Add(VividLight_Greater, VividLight_Smaller, Img1);
                    Garbage.Clear(VividLight_Greater, VividLight_Smaller); //CleanUp
                    break;
                #endregion

                #region LinearLight
                case BlendModes.LinearLight:
                    Mat LinearLight_Res = Img1.EmptyClone();
                    Cv2.AddWeighted(Img2, 1.0, Img1, 2.0, -1.0, LinearLight_Res);
                    Img1 = Clamp(LinearLight_Res);
                    Garbage.Clear(LinearLight_Res); //CleanUp
                    break;
                #endregion

                #region PinLight
                case BlendModes.PinLight:
                    Mat PinLight_MaskGr = Img2.EmptyClone();
                    Mat PinLight_MaskSm = PinLight_MaskGr.EmptyClone();
                    Mat PinLight_Greater = PinLight_MaskGr.EmptyClone();
                    Mat PinLight_Smaller = PinLight_MaskGr.EmptyClone();
                    Cv2.Threshold(Img1, PinLight_MaskGr, 0.5, 1.0, ThresholdTypes.Binary);
                    Cv2.Threshold(Img1, PinLight_MaskSm, 0.5, 1.0, ThresholdTypes.BinaryInv);

                    Cv2.Add(Img1, Img1, Img1); // 2B = 2 * B

                    // Smaller (Blend <= 0.5) * (Min(T, 2B))
                    Cv2.Min(Img1, Img2, PinLight_Smaller);
                    Cv2.Multiply(PinLight_Smaller, PinLight_MaskSm, PinLight_Smaller);
                    Garbage.Clear(PinLight_MaskSm); //CleanUp

                    // Greater (Blend > 0.5) * (Max(T, 2B - 1))
                    Cv2.AddWeighted(Img1, 0.5, Img1, 0.5, -1.0, PinLight_Greater);
                    Cv2.Max(PinLight_Greater, Img2, PinLight_Greater);
                    Cv2.Multiply(PinLight_Greater, PinLight_MaskGr, PinLight_Greater);
                    Garbage.Clear(PinLight_MaskGr); //CleanUp

                    // Add it together
                    Cv2.Add(PinLight_Greater, PinLight_Smaller, Img1);
                    Garbage.Clear(PinLight_Greater, PinLight_Smaller); //CleanUp
                    break;
                #endregion

                // Not equal Photoshop (see VividLight)
                #region HardMix
                case BlendModes.HardMix:
                    Mat HardMix_VividLight = Blend(Img1, Img2, BlendModes.VividLight);
                    Cv2.Threshold(HardMix_VividLight, Img1, 0.5, 1.0, ThresholdTypes.Binary);
                    Garbage.Clear(HardMix_VividLight); //CleanUp
                    break;
                #endregion

                #region Difference
                case BlendModes.Difference:
                    Cv2.Absdiff(Img1, Img2, Img1);
                    break;
                #endregion

                #region Exclusion
                case BlendModes.Exclusion:
                    Mat Exclusion_Mult = Img1.EmptyClone();
                    Mat Exclusion_Add = Img1.EmptyClone();
                    Cv2.Multiply(Img1, Img2, Exclusion_Mult, 2.0);
                    Cv2.Add(Img1, Img2, Exclusion_Add);
                    Cv2.Subtract(Exclusion_Add, Exclusion_Mult, Img1);
                    Garbage.Clear(Exclusion_Add, Exclusion_Mult); //CleanUp
                    break;
                #endregion

                #region Subtract
                case BlendModes.Subtract:
                    Mat Subtract_Res = Img1.EmptyClone();
                    Cv2.Subtract(Img2, Img1, Subtract_Res);
                    Img1 = Clamp(Subtract_Res);
                    Garbage.Clear(Subtract_Res); //CleanUp
                    break;
                #endregion

                #region Divide
                case BlendModes.Divide:
                    Mat Divide_Z = Img1.EmptyClone();
                    Mat Divide_NZ = Img2.EmptyClone();
                    Cv2.Compare(Img1, Divide_Z, Divide_Z, CmpTypes.EQ);
                    Cv2.Compare(Img2, Divide_NZ, Divide_NZ, CmpTypes.NE);
                    Divide_Z.ConvertTo(Divide_Z, Img2.Type(), 1.0 / 255.0, 0.0);
                    Divide_NZ.ConvertTo(Divide_NZ, Img2.Type(), 1.0 / 255.0, 0.0);
                    Cv2.Multiply(Divide_Z, Divide_NZ, Divide_NZ);
                    Garbage.Clear(Divide_Z); //CleanUp

                    Mat Divide_Res = Img1.EmptyClone();
                    Cv2.Divide(Img2, Img1, Divide_Res);
                    Cv2.Add(Divide_Res, Divide_NZ, Divide_Res);
                    Img1 = Clamp(Divide_Res);
                    Garbage.Clear(Divide_Res); //CleanUp
                    break;
                #endregion

                // Not equal Photoshop
                #region Hue
                case BlendModes.Hue:
                    Mat Hue1 = Img1.CvtColor(ColorConversionCodes.BGR2HLS);
                    Mat Hue2 = Img2.CvtColor(ColorConversionCodes.BGR2HLS);

                    Mat[] HueResult = new Mat[]
                    {
                        Hue1.ExtractChannel(0),
                        Hue2.ExtractChannel(1),
                        Hue2.ExtractChannel(2)
                    };

                    Garbage.Clear(Hue1, Hue2); //CleanUp
                    Cv2.Merge(HueResult, Img2);
                    Garbage.Clear(HueResult); //CleanUp
                    Img1 = Img2.CvtColor(ColorConversionCodes.HLS2BGR);
                    break;
                #endregion

                // Not equal Photoshop
                #region Saturation
                case BlendModes.Saturation:
                    Mat Saturation1 = Img1.CvtColor(ColorConversionCodes.BGR2HLS);
                    Mat Saturation2 = Img2.CvtColor(ColorConversionCodes.BGR2HLS);

                    Mat[] SaturationResult = new Mat[]
                    {
                        Saturation2.ExtractChannel(0),
                        Saturation2.ExtractChannel(1),
                        Saturation1.ExtractChannel(2)
                    };

                    Garbage.Clear(Saturation1, Saturation2); //CleanUp
                    Cv2.Merge(SaturationResult, Img2);
                    Garbage.Clear(SaturationResult); //CleanUp
                    Saturation1 = Img2.CvtColor(ColorConversionCodes.HLS2BGR);
                    Img1 = Clamp(Saturation1);
                    Garbage.Clear(Saturation1); //CleanUp
                    break;
                #endregion

                // Not equal Photoshop
                #region Color
                case BlendModes.Color:
                    Img1 = Blend(Img2, Img1, BlendModes.Luminosity);
                    break;
                #endregion

                // Not equal Photoshop
                #region Luminosity
                case BlendModes.Luminosity:
                    Mat Luminosity1 = Img1.CvtColor(ColorConversionCodes.BGR2YCrCb);
                    Mat Luminosity2 = Img2.CvtColor(ColorConversionCodes.BGR2YCrCb);

                    Mat[] LuminosityResult = new Mat[]
                    {
                        Luminosity1.ExtractChannel(0),
                        Luminosity2.ExtractChannel(1),
                        Luminosity2.ExtractChannel(2)
                    };

                    Garbage.Clear(Luminosity1, Luminosity2); //CleanUp
                    Cv2.Merge(LuminosityResult, Img2);
                    Garbage.Clear(LuminosityResult); //CleanUp
                    Luminosity1 = Img2.CvtColor(ColorConversionCodes.YCrCb2BGR);
                    Img1 = Clamp(Luminosity1);
                    Garbage.Clear(Luminosity1); //CleanUp
                    break;
                #endregion

                #region InverseSubtract
                case BlendModes.InverseSubtract:
                    Mat InverseSubtract_Res = Invert(Img2);
                    Cv2.Subtract(InverseSubtract_Res, Img1, InverseSubtract_Res);
                    Img1 = Clamp(InverseSubtract_Res);
                    Garbage.Clear(InverseSubtract_Res); //CleanUp
                    break;
                #endregion

                #region Allanon
                case BlendModes.Allanon:
                    Cv2.AddWeighted(Img1, 0.5, Img2, 0.5, 0.0, Img1);
                    break;
                #endregion

                #region GeometricMean
                case BlendModes.GeometricMean:
                    Mat GeometricMean_Mul = Img1.EmptyClone();
                    Cv2.Multiply(Img1, Img2, GeometricMean_Mul);
                    Cv2.Sqrt(GeometricMean_Mul, Img1);
                    Garbage.Clear(GeometricMean_Mul); // CleanUp
                    break;
                #endregion

                #region GrainExtract
                case BlendModes.GrainExtract:
                    Mat GrainExtract_Res = Img1.EmptyClone();
                    Cv2.AddWeighted(Img2, 1.0, Img1, -1.0, 0.5, GrainExtract_Res);
                    Img1 = Clamp(GrainExtract_Res);
                    Garbage.Clear(GrainExtract_Res); //CleanUp
                    break;
                #endregion

                #region GrainMerge
                case BlendModes.GrainMerge:
                    Mat GrainMerge_Res = Img1.EmptyClone();
                    Cv2.AddWeighted(Img1, 1.0, Img2, 1.0, -0.5, GrainMerge_Res);
                    Img1 = Clamp(GrainMerge_Res);
                    Garbage.Clear(GrainMerge_Res); //CleanUp
                    break;
                #endregion

                #region AdditiveSubtractive
                case BlendModes.AdditiveSubtractive:
                    Mat AdditiveSubtractive_Res = Img1.EmptyClone();
                    Cv2.Sqrt(Img2, Img2);
                    Cv2.Subtract(Img1, Img2, AdditiveSubtractive_Res);
                    Img1 = Clamp(AdditiveSubtractive_Res);
                    Garbage.Clear(AdditiveSubtractive_Res); // CleanUp
                    break;
                #endregion

                default:
                    throw new NotImplementedException();
            }

            Img1.ConvertTo(Img2, ResultType, ConvertCoefA, ConvertCoefB);
            Garbage.Clear(Img1); // CleanUp
            return Img2;
        }
    }
}