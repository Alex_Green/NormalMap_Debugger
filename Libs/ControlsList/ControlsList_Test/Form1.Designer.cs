﻿namespace ControlsList_Test
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.controlsListKernels = new ControlsList.ControlsList();
            this.SuspendLayout();
            // 
            // controlsListKernels
            // 
            this.controlsListKernels.AutoScroll = true;
            this.controlsListKernels.AutoSize = true;
            this.controlsListKernels.BackColor = System.Drawing.SystemColors.Control;
            this.controlsListKernels.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.controlsListKernels.ButtonsAddVisible = true;
            this.controlsListKernels.ButtonsDeleteVisible = true;
            this.controlsListKernels.ButtonsUpDownVisible = true;
            this.controlsListKernels.ColorIcons = true;
            this.controlsListKernels.DefaultRowsEnabled = true;
            this.controlsListKernels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlsListKernels.Elements = new System.Windows.Forms.Control[0];
            this.controlsListKernels.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.controlsListKernels.Location = new System.Drawing.Point(0, 0);
            this.controlsListKernels.MaxElementsCount = 20;
            this.controlsListKernels.MinElementsCount = 2;
            this.controlsListKernels.MinimumSize = new System.Drawing.Size(32, 32);
            this.controlsListKernels.Name = "controlsListKernels";
            this.controlsListKernels.Size = new System.Drawing.Size(284, 261);
            this.controlsListKernels.TabIndex = 0;
            this.controlsListKernels.WrapContents = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.controlsListKernels);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ControlsList Test";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ControlsList.ControlsList controlsListKernels;
    }
}

