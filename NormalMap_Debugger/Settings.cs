﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace NormalMap_Debugger
{
    [XmlRoot(ElementName = "Config")]
    public class SettingsXML
    {
        public const string XmlFile = "Settings.xml";

        public static bool Save(SettingsXML SettingsXml, string FullPath)
        {
            try
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(FullPath, new XmlWriterSettings() { Indent = true, IndentChars = "\t", OmitXmlDeclaration = true }))
                {
                    #region Remove 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ...'
                    XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);
                    #endregion

                    XmlSerializer serializer = new XmlSerializer(typeof(SettingsXML));
                    serializer.Serialize(xmlWriter, SettingsXml, namespaces);
                    xmlWriter.Close();
                    return File.Exists(XmlFile);
                }
            }
            catch
            {
                return false;
            }
        }

        public static SettingsXML Load(string XmlFile)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsXML));
            if (File.Exists(XmlFile)) //If file exist
            {
                try
                {
                    using (FileStream SettingsFile = new FileStream(XmlFile, FileMode.Open))
                    {
                        SettingsXML S = (SettingsXML)serializer.Deserialize(SettingsFile);
                        SettingsFile.Close();
                        return S;
                    }
                }
                catch
                {
                    return null;
                }
            }
            else //File not exist
            {
                if (Save(new SettingsXML(), XmlFile)) //Save new SettingsFile
                {
                    using (FileStream SettingsFile = new FileStream(XmlFile, FileMode.Open))
                    {
                        SettingsXML S = (SettingsXML)serializer.Deserialize(SettingsFile);
                        SettingsFile.Close();
                        return S;
                    }
                }
                else
                    return null;
            }
        }

        #region Settings
        public FormWindowState WindowState = FormWindowState.Normal;
        //Need a lot of RAM: UndoLevels * Width * Height * PixelDepth. [10 * 4K textures => 640Mb!]
        public uint UndoLevels = 10;
        public bool PositiveGreenChannel = false;
        public string ToStringParameter = "0.00000";

        public _Graphics Graphics = new _Graphics();
        public class _Graphics
        {
            public _NormalMap NormalMap = new _NormalMap();
            public class _NormalMap
            {
                public InterpolationMode InterpolationMode = InterpolationMode.NearestNeighbor;
                public PixelOffsetMode PixelOffsetMode = PixelOffsetMode.HighQuality;
            }

            public _NormalVector NormalVector = new _NormalVector();
            public class _NormalVector
            {
                public CompositingQuality CompositingQuality = CompositingQuality.HighQuality; 
                public PixelOffsetMode PixelOffsetMode = PixelOffsetMode.HighQuality;
                public SmoothingMode SmoothingMode = SmoothingMode.HighQuality;
                public float LineWidth = 2.0f;
                public RGBColor LineColor1 = new RGBColor(255, 0, 0);
                public RGBColor LineColor2 = new RGBColor(255, 255, 0);
                public RGBColor BackgroundColor = new RGBColor(0, 0, 0);
            }
        }
        #endregion
    }

    public class RGBColor
    {
        public byte R = 0, G = 0, B = 0;

        public RGBColor()
        {
        }

        public RGBColor(byte R, byte G, byte B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }

        public Color ToColor()
        {
            return Color.FromArgb(R, G, B);
        }
    }
}
