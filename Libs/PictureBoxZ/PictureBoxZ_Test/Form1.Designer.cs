﻿namespace PictureBoxZ_Test
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBoxZ1 = new PictureBoxZ.PictureBoxZ();
            this.SuspendLayout();
            // 
            // pictureBoxZ1
            // 
            this.pictureBoxZ1.BackColor = System.Drawing.Color.DarkGray;
            this.pictureBoxZ1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxZ1.BackgroundImage")));
            this.pictureBoxZ1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxZ1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxZ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxZ1.Image = null;
            this.pictureBoxZ1.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            this.pictureBoxZ1.LoadingCircleEnabled = true;
            this.pictureBoxZ1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxZ1.Name = "pictureBoxZ1";
            this.pictureBoxZ1.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            this.pictureBoxZ1.Size = new System.Drawing.Size(284, 261);
            this.pictureBoxZ1.TabIndex = 0;
            this.pictureBoxZ1.ZoomCenterPoint = ((System.Drawing.PointF)(resources.GetObject("pictureBoxZ1.ZoomCenterPoint")));
            this.pictureBoxZ1.ZoomList = ((System.Collections.Generic.List<float>)(resources.GetObject("pictureBoxZ1.ZoomList")));
            this.pictureBoxZ1.ZoomListIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.pictureBoxZ1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PictureBoxZ Test";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBoxZ.PictureBoxZ pictureBoxZ1;
    }
}

