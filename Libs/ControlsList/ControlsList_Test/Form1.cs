﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ControlsList_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            controlsListKernels.SetButtonAddFunc(() =>
            {
                //////////////////////////////////////////////////
                SliderTrackbar SliderTrackbar1 = new SliderTrackbar();
                SliderTrackbar SliderTrackbar2 = new SliderTrackbar();
                SliderTrackbar1.Name = SliderTrackbar1.Text = nameof(SliderTrackbar1);
                SliderTrackbar2.Name = SliderTrackbar2.Text = nameof(SliderTrackbar2);
                SliderTrackbar1.Dock = SliderTrackbar2.Dock = DockStyle.Fill;
                SliderTrackbar1.Margin = SliderTrackbar2.Margin = Padding.Empty;
                //////////////////////////////////////////////////

                TableLayoutPanel TLP = new TableLayoutPanel();
                TLP.Height = Math.Max(SliderTrackbar1.Height, SliderTrackbar2.Height);
                TLP.ColumnCount = 2;
                TLP.Margin = Padding.Empty;
                TLP.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
                TLP.Controls.Add(SliderTrackbar1, 0, 0);
                TLP.Controls.Add(SliderTrackbar2, 1, 0);
                return TLP;
            });

            controlsListKernels.AddControl();
            controlsListKernels.AddControl();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
    }
}
