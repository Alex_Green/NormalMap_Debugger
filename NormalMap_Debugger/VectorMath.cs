﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NormalMap_Debugger
{
    public struct Vector2
    {
        int x, y;

        public Vector2(int X)
        {
            x = X;
            y = 0;
        }

        public Vector2(int X, int Y)
        {
            x = X;
            y = Y;
        }

        public Vector2(Vector2 V)
        {
            x = V.X;
            y = V.Y;
        }

        public Vector2(Vector3 V)
        {
            x = V.X;
            y = V.Y;
        }

        public Vector2(Vector2f V)
        {
            x = (int)V.X;
            y = (int)V.Y;
        }

        public Vector2(Vector3f V)
        {
            x = (int)V.X;
            y = (int)V.Y;
        }

        public Vector2(Point P)
        {
            x = P.X;
            y = P.Y;
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public static Vector2 Zero
        {
            get { return new Vector2(0, 0); }
        }

        public static float Dot(Vector2 v1, Vector2 v2)
        {
            return v1.Dot(v2);
        }

        public float Dot(Vector2 v)
        {
            return x * v.x + y * v.y;
        }

        public float Length
        {
            get { return (float)Math.Sqrt(x * x + y * y); }
        }

        public Point ToPoint()
        {
            return new Point(x, y);
        }

        public override string ToString()
        {
            return String.Format("[{0}; {1}]", x, y);
        }

        #region Operators
        public static Vector2 operator +(Vector2 V, int Value)
        {
            return new Vector2(V.X + Value, V.Y + Value);
        }

        public static Vector2 operator -(Vector2 V, int Value)
        {
            return new Vector2(V.X - Value, V.Y - Value);
        }

        public static Vector2 operator +(Vector2 V1, Vector2 V2)
        {
            return new Vector2(V1.X + V2.X, V1.Y + V2.Y);
        }

        public static Vector2 operator -(Vector2 V1, Vector2 V2)
        {
            return new Vector2(V1.X - V2.X, V1.Y - V2.Y);
        }

        public static Vector2 operator *(Vector2 V, int Value)
        {
            return new Vector2(V.X * Value, V.Y * Value);
        }

        public static Vector2 operator /(Vector2 V, int Value)
        {
            return new Vector2(V.X / Value, V.Y / Value);
        }
        #endregion
    }

    public struct Vector3
    {
        int x, y, z;

        public Vector3(int X)
        {
            x = X;
            y = 0;
            z = 0;
        }

        public Vector3(int X, int Y)
        {
            x = X;
            y = Y;
            z = 0;
        }

        public Vector3(int X, int Y, int Z)
        {
            x = X;
            y = Y;
            z = Z;
        }

        public Vector3(Vector2 V)
        {
            x = V.X;
            y = V.Y;
            z = 0;
        }

        public Vector3(Vector3 V)
        {
            x = V.X;
            y = V.Y;
            z = V.Z;
        }

        public Vector3(Vector2f V)
        {
            x = (int)V.X;
            y = (int)V.Y;
            z = 0;
        }

        public Vector3(Vector3f V)
        {
            x = (int)V.X;
            y = (int)V.Y;
            z = (int)V.Z;
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public int Z
        {
            get { return z; }
            set { z = value; }
        }

        public static Vector3 Zero
        {
            get { return new Vector3(0, 0, 0); }
        }

        public static float Dot(Vector3 v1, Vector3 v2)
        {
            return v1.Dot(v2);
        }

        public float Dot(Vector3 v)
        {
            return x * v.x + y * v.y + z * v.z;
        }

        public float Length
        {
            get { return (float)Math.Sqrt(x * x + y * y + z * z); }
        }

        public override string ToString()
        {
            return String.Format("[{0}; {1}; {2}]", x, y, z);
        }

        #region Operators
        public static Vector3 operator +(Vector3 V, int Value)
        {
            return new Vector3(V.X + Value, V.Y + Value, V.Z + Value);
        }

        public static Vector3 operator -(Vector3 V, int Value)
        {
            return new Vector3(V.X - Value, V.Y - Value, V.Z - Value);
        }

        public static Vector3 operator +(Vector3 V1, Vector3 V2)
        {
            return new Vector3(V1.X + V2.X, V1.Y + V2.Y, V1.Z + V2.Z);
        }

        public static Vector3 operator -(Vector3 V1, Vector3 V2)
        {
            return new Vector3(V1.X - V2.X, V1.Y - V2.Y, V1.Z - V2.Z);
        }

        public static Vector3 operator *(Vector3 V, int Value)
        {
            return new Vector3(V.X * Value, V.Y * Value, V.Z * Value);
        }

        public static Vector3 operator /(Vector3 V, int Value)
        {
            return new Vector3(V.X / Value, V.Y / Value, V.Z / Value);
        }
        #endregion
    }

    public struct Vector2f
    {
        float x, y;

        public Vector2f(float X)
        {
            x = X;
            y = 0f;
        }

        public Vector2f(float X, float Y)
        {
            x = X;
            y = Y;
        }

        public Vector2f(Vector2 V)
        {
            x = V.X;
            y = V.Y;
        }

        public Vector2f(Vector2f V)
        {
            x = V.X;
            y = V.Y;
        }

        public Vector2f(Vector3 V)
        {
            x = V.X;
            y = V.Y;
        }

        public Vector2f(Vector3f V)
        {
            x = V.X;
            y = V.Y;
        }

        public Vector2f(PointF P)
        {
            x = P.X;
            y = P.Y;
        }

        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public static Vector2f Zero
        {
            get { return new Vector2f(0f, 0f); }
        }

        public static float Dot(Vector2f v1, Vector2f v2)
        {
            return v1.Dot(v2);
        }

        public float Dot(Vector2f v)
        {
            return x * v.x + y * v.y;
        }

        public float Length
        {
            get { return (float)Math.Sqrt(x * x + y * y); }
        }

        public void Normalize()
        {
            float L = this.Length;
            x /= L;
            y /= L;
        }

        public PointF ToPointF()
        {
            return new PointF(x, y);
        }

        public override string ToString()
        {
            return String.Format("[{0}; {1}]", x, y);
        }

        #region Operators
        public static Vector2f operator +(Vector2f V, float Value)
        {
            return new Vector2f(V.X + Value, V.Y + Value);
        }

        public static Vector2f operator -(Vector2f V, float Value)
        {
            return new Vector2f(V.X - Value, V.Y - Value);
        }

        public static Vector2f operator +(Vector2f V1, Vector2f V2)
        {
            return new Vector2f(V1.X + V2.X, V1.Y + V2.Y);
        }

        public static Vector2f operator -(Vector2f V1, Vector2f V2)
        {
            return new Vector2f(V1.X - V2.X, V1.Y - V2.Y);
        }

        public static Vector2f operator *(Vector2f V, float Value)
        {
            return new Vector2f(V.X * Value, V.Y * Value);
        }

        public static Vector2f operator /(Vector2f V, float Value)
        {
            return new Vector2f(V.X / Value, V.Y / Value);
        }
        #endregion
    }

    public struct Vector3f
    {
        float x, y, z;

        public Vector3f(float X)
        {
            x = X;
            y = 0f;
            z = 0f;
        }

        public Vector3f(float X, float Y)
        {
            x = X;
            y = Y;
            z = 0f;
        }

        public Vector3f(float X, float Y, float Z)
        {
            x = X;
            y = Y;
            z = Z;
        }

        public Vector3f(Vector2 V)
        {
            x = V.X;
            y = V.Y;
            z = 0f;
        }

        public Vector3f(Vector2f V)
        {
            x = V.X;
            y = V.Y;
            z = 0f;
        }

        public Vector3f(Vector3 V)
        {
            x = V.X;
            y = V.Y;
            z = V.Z;
        }

        public Vector3f(Vector3f V)
        {
            x = V.X;
            y = V.Y;
            z = V.Z;
        }

        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public float Z
        {
            get { return z; }
            set { z = value; }
        }

        public static Vector3f Zero
        {
            get { return new Vector3f(0f, 0f, 0f); }
        }

        public static float Dot(Vector3f v1, Vector3f v2)
        {
            return v1.Dot(v2);
        }

        public float Dot(Vector3f v)
        {
            return x * v.x + y * v.y + z * v.z;
        }

        public float Length
        {
            get { return (float)Math.Sqrt(x * x + y * y + z * z); }
        }

        public void Normalize()
        {
            float L = this.Length;
            x /= L;
            y /= L;
            z /= L;
        }

        public override string ToString()
        {
            return String.Format("[{0}; {1}; {2}]", x, y, z);
        }

        #region Operators
        public static Vector3f operator +(Vector3f V, float Value)
        {
            return new Vector3f(V.X + Value, V.Y + Value, V.Z + Value);
        }

        public static Vector3f operator -(Vector3f V, float Value)
        {
            return new Vector3f(V.X - Value, V.Y - Value, V.Z - Value);
        }

        public static Vector3f operator +(Vector3f V1, Vector3f V2)
        {
            return new Vector3f(V1.X + V2.X, V1.Y + V2.Y, V1.Z + V2.Z);
        }

        public static Vector3f operator -(Vector3f V1, Vector3f V2)
        {
            return new Vector3f(V1.X - V2.X, V1.Y - V2.Y, V1.Z - V2.Z);
        }

        public static Vector3f operator *(Vector3f V, float Value)
        {
            return new Vector3f(V.X * Value, V.Y * Value, V.Z * Value);
        }

        public static Vector3f operator /(Vector3f V, float Value)
        {
            return new Vector3f(V.X / Value, V.Y / Value, V.Z / Value);
        }
        #endregion
    }
}
