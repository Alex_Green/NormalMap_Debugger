// ==========================================================
// FreeImage 3 .NET wrapper
// Original FreeImage 3 functions and .NET compatible derived functions
//
// Design and implementation by
// - Jean-Philippe Goerke (jpgoerke@users.sourceforge.net)
// - Carsten Klein (cklein05@users.sourceforge.net)
//
// Contributors:
// - David Boland (davidboland@vodafone.ie)
//
// Main reference : MSDN Knowlede Base
//
// This file is part of FreeImage 3
//
// COVERED CODE IS PROVIDED UNDER THIS LICENSE ON AN "AS IS" BASIS, WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES
// THAT THE COVERED CODE IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE
// OR NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED
// CODE IS WITH YOU. SHOULD ANY COVERED CODE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY
// SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL
// PART OF THIS LICENSE. NO USE OF ANY COVERED CODE IS AUTHORIZED HEREUNDER EXCEPT UNDER
// THIS DISCLAIMER.
//
// Use at your own risk!
// ==========================================================

// ==========================================================
// CVS
// $Revision: 1.9 $
// $Date: 2009/09/15 11:41:37 $
// $Id: FreeImageStaticImports.cs,v 1.9 2009/09/15 11:41:37 cklein05 Exp $
// ==========================================================

using System;
using System.Runtime.InteropServices;
using FreeImageAPI.Plugins;
using FreeImageAPI.IO;

namespace FreeImageAPI
{
public static partial class FreeImage
	{
		private class FreeImage_x86
		{
            public const string FreeImageLibrary = "FreeImage_x86";

			#region General functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Initialise")]
			public static extern void Initialise(bool load_local_plugins_only);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_DeInitialise")]
			public static extern void DeInitialise();

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_GetVersion")]
			public static unsafe extern byte* GetVersion_();

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_GetCopyrightMessage")]
			public static unsafe extern byte* GetCopyrightMessage_();

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_OutputMessageProc")]
			public static extern void OutputMessageProc(FREE_IMAGE_FORMAT fif, string message);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetOutputMessage")]
			public static extern void SetOutputMessage(OutputMessageFunction omf);
			#endregion

			#region Bitmap management functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Allocate")]
			public static extern FIBITMAP Allocate(int width, int height, int bpp, uint red_mask, uint green_mask, uint blue_mask);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AllocateT")]
			public static extern FIBITMAP AllocateT(FREE_IMAGE_TYPE type, int width, int height, int bpp, uint red_mask, uint green_mask, uint blue_mask);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AllocateEx")]
			public static extern FIBITMAP AllocateEx(int width, int height, int bpp, IntPtr color, FREE_IMAGE_COLOR_OPTIONS options, RGBQUAD[] palette, uint red_mask, uint green_mask, uint blue_mask);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AllocateExT")]
			public static extern FIBITMAP AllocateExT(FREE_IMAGE_TYPE type, int width, int height, int bpp, IntPtr color, FREE_IMAGE_COLOR_OPTIONS options, RGBQUAD[] palette, uint red_mask, uint green_mask, uint blue_mask);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Clone")]
			public static extern FIBITMAP Clone(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Unload")]
			public static extern void Unload(FIBITMAP dib);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_LoadU")]
			public static extern FIBITMAP Load(FREE_IMAGE_FORMAT fif, string filename, FREE_IMAGE_LOAD_FLAGS flags);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_LoadU")]
			public static extern FIBITMAP LoadU(FREE_IMAGE_FORMAT fif, string filename, FREE_IMAGE_LOAD_FLAGS flags);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_LoadFromHandle")]
			public static extern FIBITMAP LoadFromHandle(FREE_IMAGE_FORMAT fif, ref FreeImageIO io, fi_handle handle, FREE_IMAGE_LOAD_FLAGS flags);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_SaveU")]
			public static extern bool Save(FREE_IMAGE_FORMAT fif, FIBITMAP dib, string filename, FREE_IMAGE_SAVE_FLAGS flags);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_SaveU")]
			public static extern bool SaveU(FREE_IMAGE_FORMAT fif, FIBITMAP dib, string filename, FREE_IMAGE_SAVE_FLAGS flags);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SaveToHandle")]
			public static extern bool SaveToHandle(FREE_IMAGE_FORMAT fif, FIBITMAP dib, ref FreeImageIO io, fi_handle handle, FREE_IMAGE_SAVE_FLAGS flags);
			#endregion

			#region Memory I/O streams
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_OpenMemory")]
			public static extern FIMEMORY OpenMemory(IntPtr data, uint size_in_bytes);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_OpenMemory")]
			public static extern FIMEMORY OpenMemoryEx(byte[] data, uint size_in_bytes);
			
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_CloseMemory")]
			public static extern void CloseMemory(FIMEMORY stream);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_LoadFromMemory")]
			public static extern FIBITMAP LoadFromMemory(FREE_IMAGE_FORMAT fif, FIMEMORY stream, FREE_IMAGE_LOAD_FLAGS flags);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SaveToMemory")]
			public static extern bool SaveToMemory(FREE_IMAGE_FORMAT fif, FIBITMAP dib, FIMEMORY stream, FREE_IMAGE_SAVE_FLAGS flags);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_TellMemory")]
			public static extern int TellMemory(FIMEMORY stream);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SeekMemory")]
			public static extern bool SeekMemory(FIMEMORY stream, int offset, System.IO.SeekOrigin origin);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AcquireMemory")]
			public static extern bool AcquireMemory(FIMEMORY stream, ref IntPtr data, ref uint size_in_bytes);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ReadMemory")]
			public static extern uint ReadMemory(byte[] buffer, uint size, uint count, FIMEMORY stream);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_WriteMemory")]
			public static extern uint WriteMemory(byte[] buffer, uint size, uint count, FIMEMORY stream);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_LoadMultiBitmapFromMemory")]
			public static extern FIMULTIBITMAP LoadMultiBitmapFromMemory(FREE_IMAGE_FORMAT fif, FIMEMORY stream, FREE_IMAGE_LOAD_FLAGS flags);
			#endregion

			#region Plugin functions
			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_RegisterLocalPlugin")]
			public static extern FREE_IMAGE_FORMAT RegisterLocalPlugin(InitProc proc_address, string format, string description, string extension, string regexpr);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_RegisterExternalPlugin")]
			public static extern FREE_IMAGE_FORMAT RegisterExternalPlugin(string path, string format, string description, string extension, string regexpr);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFIFCount")]
			public static extern int GetFIFCount();

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetPluginEnabled")]
			public static extern int SetPluginEnabled(FREE_IMAGE_FORMAT fif, bool enable);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_IsPluginEnabled")]
			public static extern int IsPluginEnabled(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_GetFIFFromFormat")]
			public static extern FREE_IMAGE_FORMAT GetFIFFromFormat(string format);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_GetFIFFromMime")]
			public static extern FREE_IMAGE_FORMAT GetFIFFromMime(string mime);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFormatFromFIF")]
			public static unsafe extern byte* GetFormatFromFIF_(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFIFExtensionList")]
			public static unsafe extern byte* GetFIFExtensionList_(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFIFDescription")]
			public static unsafe extern byte* GetFIFDescription_(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFIFRegExpr")]
			public static unsafe extern byte* GetFIFRegExpr_(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFIFMimeType")]
			public static unsafe extern byte* GetFIFMimeType_(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_GetFIFFromFilenameU")]
			public static extern FREE_IMAGE_FORMAT GetFIFFromFilename(string filename);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_GetFIFFromFilenameU")]
			public static extern FREE_IMAGE_FORMAT GetFIFFromFilenameU(string filename);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FIFSupportsReading")]
			public static extern bool FIFSupportsReading(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FIFSupportsWriting")]
			public static extern bool FIFSupportsWriting(FREE_IMAGE_FORMAT fif);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FIFSupportsExportBPP")]
			public static extern bool FIFSupportsExportBPP(FREE_IMAGE_FORMAT fif, int bpp);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FIFSupportsExportType")]
			public static extern bool FIFSupportsExportType(FREE_IMAGE_FORMAT fif, FREE_IMAGE_TYPE type);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FIFSupportsICCProfiles")]
			public static extern bool FIFSupportsICCProfiles(FREE_IMAGE_FORMAT fif);
			#endregion

			#region Multipage functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_OpenMultiBitmap")]
			public static extern FIMULTIBITMAP OpenMultiBitmap(FREE_IMAGE_FORMAT fif, string filename, bool create_new, bool read_only, bool keep_cache_in_memory, FREE_IMAGE_LOAD_FLAGS flags);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_OpenMultiBitmapFromHandle")]
			public static extern FIMULTIBITMAP OpenMultiBitmapFromHandle(FREE_IMAGE_FORMAT fif, ref FreeImageIO io, fi_handle handle, FREE_IMAGE_LOAD_FLAGS flags);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_CloseMultiBitmap")]
			public static extern bool CloseMultiBitmap_(FIMULTIBITMAP bitmap, FREE_IMAGE_SAVE_FLAGS flags);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetPageCount")]
			public static extern int GetPageCount(FIMULTIBITMAP bitmap);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AppendPage")]
			public static extern void AppendPage(FIMULTIBITMAP bitmap, FIBITMAP data);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_InsertPage")]
			public static extern void InsertPage(FIMULTIBITMAP bitmap, int page, FIBITMAP data);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_DeletePage")]
			public static extern void DeletePage(FIMULTIBITMAP bitmap, int page);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_LockPage")]
			public static extern FIBITMAP LockPage(FIMULTIBITMAP bitmap, int page);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_UnlockPage")]
			public static extern void UnlockPage(FIMULTIBITMAP bitmap, FIBITMAP data, bool changed);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_MovePage")]
			public static extern bool MovePage(FIMULTIBITMAP bitmap, int target, int source);
			
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetLockedPageNumbers")]
			public static extern bool GetLockedPageNumbers(FIMULTIBITMAP bitmap, int[] pages, ref int count);
			#endregion

			#region Filetype functions
			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_GetFileTypeU")]
			public static extern FREE_IMAGE_FORMAT GetFileType(string filename, int size);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_GetFileTypeU")]
			public static extern FREE_IMAGE_FORMAT GetFileTypeU(string filename, int size);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFileTypeFromHandle")]
			public static extern FREE_IMAGE_FORMAT GetFileTypeFromHandle(ref FreeImageIO io, fi_handle handle, int size);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetFileTypeFromMemory")]
			public static extern FREE_IMAGE_FORMAT GetFileTypeFromMemory(FIMEMORY stream, int size);
			#endregion

			#region Helper functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_IsLittleEndian")]
			public static extern bool IsLittleEndian();

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_LookupX11Color")]
			public static extern bool LookupX11Color(string szColor, out byte nRed, out byte nGreen, out byte nBlue);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_LookupSVGColor")]
			public static extern bool LookupSVGColor(string szColor, out byte nRed, out byte nGreen, out byte nBlue);
			#endregion

			#region Pixel access functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetBits")]
			public static extern IntPtr GetBits(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetScanLine")]
			public static extern IntPtr GetScanLine(FIBITMAP dib, int scanline);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetPixelIndex")]
			public static extern bool GetPixelIndex(FIBITMAP dib, uint x, uint y, out byte value);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetPixelColor")]
			public static extern bool GetPixelColor(FIBITMAP dib, uint x, uint y, out RGBQUAD value);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetPixelIndex")]
			public static extern bool SetPixelIndex(FIBITMAP dib, uint x, uint y, ref byte value);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetPixelColor")]
			public static extern bool SetPixelColor(FIBITMAP dib, uint x, uint y, ref RGBQUAD value);
			#endregion

			#region Bitmap information functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetImageType")]
			public static extern FREE_IMAGE_TYPE GetImageType(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetColorsUsed")]
			public static extern uint GetColorsUsed(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetBPP")]
			public static extern uint GetBPP(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetWidth")]
			public static extern uint GetWidth(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetHeight")]
			public static extern uint GetHeight(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetLine")]
			public static extern uint GetLine(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetPitch")]
			public static extern uint GetPitch(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetDIBSize")]
			public static extern uint GetDIBSize(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetPalette")]
			public static extern IntPtr GetPalette(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetDotsPerMeterX")]
			public static extern uint GetDotsPerMeterX(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetDotsPerMeterY")]
			public static extern uint GetDotsPerMeterY(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetDotsPerMeterX")]
			public static extern void SetDotsPerMeterX(FIBITMAP dib, uint res);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetDotsPerMeterY")]
			public static extern void SetDotsPerMeterY(FIBITMAP dib, uint res);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetInfoHeader")]
			public static extern IntPtr GetInfoHeader(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetInfo")]
			public static extern IntPtr GetInfo(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetColorType")]
			public static extern FREE_IMAGE_COLOR_TYPE GetColorType(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetRedMask")]
			public static extern uint GetRedMask(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetGreenMask")]
			public static extern uint GetGreenMask(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetBlueMask")]
			public static extern uint GetBlueMask(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTransparencyCount")]
			public static extern uint GetTransparencyCount(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTransparencyTable")]
			public static extern IntPtr GetTransparencyTable(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTransparent")]
			public static extern void SetTransparent(FIBITMAP dib, bool enabled);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTransparencyTable")]
			public static extern void SetTransparencyTable(FIBITMAP dib, byte[] table, int count);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_IsTransparent")]
			public static extern bool IsTransparent(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_HasBackgroundColor")]
			public static extern bool HasBackgroundColor(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetBackgroundColor")]
			public static extern bool GetBackgroundColor(FIBITMAP dib, out RGBQUAD bkcolor);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetBackgroundColor")]
			public static unsafe extern bool SetBackgroundColor(FIBITMAP dib, ref RGBQUAD bkcolor);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetBackgroundColor")]
			public static unsafe extern bool SetBackgroundColor(FIBITMAP dib, RGBQUAD[] bkcolor);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTransparentIndex")]
			public static extern void SetTransparentIndex(FIBITMAP dib, int index);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTransparentIndex")]
			public static extern int GetTransparentIndex(FIBITMAP dib);
			#endregion

			#region ICC profile functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetICCProfile")]
			public static extern IntPtr GetICCProfile(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_CreateICCProfile")]
			public static extern IntPtr CreateICCProfile(FIBITMAP dib, byte[] data, int size);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_DestroyICCProfile")]
			public static extern void DestroyICCProfile(FIBITMAP dib);
			#endregion

			#region Conversion functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertTo4Bits")]
			public static extern FIBITMAP ConvertTo4Bits(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertTo8Bits")]
			public static extern FIBITMAP ConvertTo8Bits(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertToGreyscale")]
			public static extern FIBITMAP ConvertToGreyscale(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertTo16Bits555")]
			public static extern FIBITMAP ConvertTo16Bits555(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertTo16Bits565")]
			public static extern FIBITMAP ConvertTo16Bits565(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertTo24Bits")]
			public static extern FIBITMAP ConvertTo24Bits(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertTo32Bits")]
			public static extern FIBITMAP ConvertTo32Bits(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ColorQuantize")]
			public static extern FIBITMAP ColorQuantize(FIBITMAP dib, FREE_IMAGE_QUANTIZE quantize);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ColorQuantizeEx")]
			public static extern FIBITMAP ColorQuantizeEx(FIBITMAP dib, FREE_IMAGE_QUANTIZE quantize, int PaletteSize, int ReserveSize, RGBQUAD[] ReservePalette);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Threshold")]
			public static extern FIBITMAP Threshold(FIBITMAP dib, byte t);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Dither")]
			public static extern FIBITMAP Dither(FIBITMAP dib, FREE_IMAGE_DITHER algorithm);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertFromRawBits")]
			public static extern FIBITMAP ConvertFromRawBits(IntPtr bits, int width, int height, int pitch, uint bpp, uint red_mask, uint green_mask, uint blue_mask, bool topdown);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertFromRawBits")]
			public static extern FIBITMAP ConvertFromRawBits(byte[] bits, int width, int height, int pitch, uint bpp, uint red_mask, uint green_mask, uint blue_mask, bool topdown);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertToRawBits")]
			public static extern void ConvertToRawBits(IntPtr bits, FIBITMAP dib, int pitch, uint bpp, uint red_mask, uint green_mask, uint blue_mask, bool topdown);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertToRawBits")]
			public static extern void ConvertToRawBits(byte[] bits, FIBITMAP dib, int pitch, uint bpp, uint red_mask, uint green_mask, uint blue_mask, bool topdown);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertToRGBF")]
			public static extern FIBITMAP ConvertToRGBF(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertToStandardType")]
			public static extern FIBITMAP ConvertToStandardType(FIBITMAP src, bool scale_linear);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ConvertToType")]
			public static extern FIBITMAP ConvertToType(FIBITMAP src, FREE_IMAGE_TYPE dst_type, bool scale_linear);
			#endregion

			#region Tone mapping operators
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ToneMapping")]
			public static extern FIBITMAP ToneMapping(FIBITMAP dib, FREE_IMAGE_TMO tmo, double first_param, double second_param);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_TmoDrago03")]
			public static extern FIBITMAP TmoDrago03(FIBITMAP src, double gamma, double exposure);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_TmoReinhard05")]
			public static extern FIBITMAP TmoReinhard05(FIBITMAP src, double intensity, double contrast);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_TmoFattal02")]
			public static extern FIBITMAP TmoFattal02(FIBITMAP src, double color_saturation, double attenuation);
			#endregion

			#region Compression functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ZLibCompress")]
			public static extern uint ZLibCompress(byte[] target, uint target_size, byte[] source, uint source_size);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ZLibUncompress")]
			public static extern uint ZLibUncompress(byte[] target, uint target_size, byte[] source, uint source_size);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ZLibGZip")]
			public static extern uint ZLibGZip(byte[] target, uint target_size, byte[] source, uint source_size);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ZLibGUnzip")]
			public static extern uint ZLibGUnzip(byte[] target, uint target_size, byte[] source, uint source_size);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ZLibCRC32")]
			public static extern uint ZLibCRC32(uint crc, byte[] source, uint source_size);
			#endregion

			#region Tag creation and destruction
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_CreateTag")]
			public static extern FITAG CreateTag();

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_DeleteTag")]
			public static extern void DeleteTag(FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_CloneTag")]
			public static extern FITAG CloneTag(FITAG tag);
			#endregion

			#region Tag accessors
			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_GetTagKey")]
			public static unsafe extern byte* GetTagKey_(FITAG tag);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_GetTagDescription")]
			public static unsafe extern byte* GetTagDescription_(FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTagID")]
			public static extern ushort GetTagID(FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTagType")]
			public static extern FREE_IMAGE_MDTYPE GetTagType(FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTagCount")]
			public static extern uint GetTagCount(FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTagLength")]
			public static extern uint GetTagLength(FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetTagValue")]
			public static extern IntPtr GetTagValue(FITAG tag);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_SetTagKey")]
			public static extern bool SetTagKey(FITAG tag, string key);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_SetTagDescription")]
			public static extern bool SetTagDescription(FITAG tag, string description);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTagID")]
			public static extern bool SetTagID(FITAG tag, ushort id);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTagType")]
			public static extern bool SetTagType(FITAG tag, FREE_IMAGE_MDTYPE type);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTagCount")]
			public static extern bool SetTagCount(FITAG tag, uint count);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTagLength")]
			public static extern bool SetTagLength(FITAG tag, uint length);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetTagValue")]
			public static extern bool SetTagValue(FITAG tag, byte[] value);
			#endregion

			#region Metadata iterator
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FindFirstMetadata")]
			public static extern FIMETADATA FindFirstMetadata(FREE_IMAGE_MDMODEL model, FIBITMAP dib, out FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FindNextMetadata")]
			public static extern bool FindNextMetadata(FIMETADATA mdhandle, out FITAG tag);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FindCloseMetadata")]
			public static extern void FindCloseMetadata_(FIMETADATA mdhandle);
			#endregion

			#region Metadata setter and getter
			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_GetMetadata")]
			public static extern bool GetMetadata(FREE_IMAGE_MDMODEL model, FIBITMAP dib, string key, out FITAG tag);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_SetMetadata")]
			public static extern bool SetMetadata(FREE_IMAGE_MDMODEL model, FIBITMAP dib, string key, FITAG tag);
			#endregion

			#region Metadata helper functions
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetMetadataCount")]
			public static extern uint GetMetadataCount(FREE_IMAGE_MDMODEL model, FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_CloneMetadata")]
			public static extern bool CloneMetadata(FIBITMAP dst, FIBITMAP src);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Ansi, EntryPoint = "FreeImage_TagToString")]
			public static unsafe extern byte* TagToString_(FREE_IMAGE_MDMODEL model, FITAG tag, uint Make);
			#endregion

			#region Rotation and flipping
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_RotateClassic")]
			public static extern FIBITMAP RotateClassic(FIBITMAP dib, double angle);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Rotate")]
			public static extern FIBITMAP Rotate(FIBITMAP dib, double angle, IntPtr backgroundColor);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_RotateEx")]
			public static extern FIBITMAP RotateEx(FIBITMAP dib, double angle, double x_shift, double y_shift, double x_origin, double y_origin, bool use_mask);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FlipHorizontal")]
			public static extern bool FlipHorizontal(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FlipVertical")]
			public static extern bool FlipVertical(FIBITMAP dib);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_JPEGTransformU")]
			public static extern bool JPEGTransform(string src_file, string dst_file, FREE_IMAGE_JPEG_OPERATION operation, bool perfect);
			#endregion

			#region Upsampling / downsampling
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Rescale")]
			public static extern FIBITMAP Rescale(FIBITMAP dib, int dst_width, int dst_height, FREE_IMAGE_FILTER filter);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_MakeThumbnail")]
			public static extern FIBITMAP MakeThumbnail(FIBITMAP dib, int max_pixel_size, bool convert);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_EnlargeCanvas")]
			public static extern FIBITMAP EnlargeCanvas(FIBITMAP dib,	int left, int top, int right, int bottom, IntPtr color, FREE_IMAGE_COLOR_OPTIONS options);
			#endregion

			#region Color manipulation
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AdjustCurve")]
			public static extern bool AdjustCurve(FIBITMAP dib, byte[] lookUpTable, FREE_IMAGE_COLOR_CHANNEL channel);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AdjustGamma")]
			public static extern bool AdjustGamma(FIBITMAP dib, double gamma);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AdjustBrightness")]
			public static extern bool AdjustBrightness(FIBITMAP dib, double percentage);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AdjustContrast")]
			public static extern bool AdjustContrast(FIBITMAP dib, double percentage);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Invert")]
			public static extern bool Invert(FIBITMAP dib);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetHistogram")]
			public static extern bool GetHistogram(FIBITMAP dib, int[] histo, FREE_IMAGE_COLOR_CHANNEL channel);
			#endregion

			#region Channel processing
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetChannel")]
			public static extern FIBITMAP GetChannel(FIBITMAP dib, FREE_IMAGE_COLOR_CHANNEL channel);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetChannel")]
			public static extern bool SetChannel(FIBITMAP dib, FIBITMAP dib8, FREE_IMAGE_COLOR_CHANNEL channel);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetComplexChannel")]
			public static extern FIBITMAP GetComplexChannel(FIBITMAP src, FREE_IMAGE_COLOR_CHANNEL channel);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SetComplexChannel")]
			public static extern bool SetComplexChannel(FIBITMAP dst, FIBITMAP src, FREE_IMAGE_COLOR_CHANNEL channel);
			#endregion

			#region Copy / Paste / Composite routines
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Copy")]
			public static extern FIBITMAP Copy(FIBITMAP dib, int left, int top, int right, int bottom);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Paste")]
			public static extern bool Paste(FIBITMAP dst, FIBITMAP src, int left, int top, int alpha);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Composite")]
			public static extern FIBITMAP Composite(FIBITMAP fg, bool useFileBkg, ref RGBQUAD appBkColor, FIBITMAP bg);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_Composite")]
			public static extern FIBITMAP Composite(FIBITMAP fg, bool useFileBkg, RGBQUAD[] appBkColor, FIBITMAP bg);

			[DllImport(FreeImageLibrary, CharSet = CharSet.Unicode, EntryPoint = "FreeImage_JPEGCropU")]
			public static extern bool JPEGCrop(string src_file, string dst_file, int left, int top, int right, int bottom);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_PreMultiplyWithAlpha")]
			public static extern bool PreMultiplyWithAlpha(FIBITMAP dib);
			#endregion

			#region Miscellaneous algorithms
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_MultigridPoissonSolver")]
			public static extern FIBITMAP MultigridPoissonSolver(FIBITMAP Laplacian, int ncycle);
			#endregion

			#region Colors
			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_GetAdjustColorsLookupTable")]
			public static extern int GetAdjustColorsLookupTable(byte[] lookUpTable, double brightness, double contrast, double gamma, bool invert);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_AdjustColors")]
			public static extern bool AdjustColors(FIBITMAP dib, double brightness, double contrast, double gamma, bool invert);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ApplyColorMapping")]
			public static extern uint ApplyColorMapping(FIBITMAP dib, RGBQUAD[] srccolors, RGBQUAD[] dstcolors, uint count, bool ignore_alpha, bool swap);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SwapColors")]
			public static extern uint SwapColors(FIBITMAP dib, ref RGBQUAD color_a, ref RGBQUAD color_b, bool ignore_alpha);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_ApplyPaletteIndexMapping")]
			public static extern uint ApplyPaletteIndexMapping(FIBITMAP dib, byte[] srcindices, byte[] dstindices, uint count, bool swap);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_SwapPaletteIndices")]
			public static extern uint SwapPaletteIndices(FIBITMAP dib, ref byte index_a, ref byte index_b);

			[DllImport(FreeImageLibrary, EntryPoint = "FreeImage_FillBackground")]
			public static extern bool FillBackground(FIBITMAP dib, IntPtr color, FREE_IMAGE_COLOR_OPTIONS options);
			#endregion
		}
	}
}