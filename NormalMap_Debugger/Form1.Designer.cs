﻿namespace NormalMap_Debugger
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonBadPixels = new System.Windows.Forms.RadioButton();
            this.radioButtonRGB = new System.Windows.Forms.RadioButton();
            this.radioButtonB = new System.Windows.Forms.RadioButton();
            this.radioButtonG = new System.Windows.Forms.RadioButton();
            this.radioButtonR = new System.Windows.Forms.RadioButton();
            this.toolStripIconMenu = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveAs = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonInvR = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInvG = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInvB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNormalize = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRestoreB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonClearB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonFlipRG = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipGB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipBR = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRotate270 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRotate90 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRotate180 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipH = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlipV = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxZ1 = new PictureBoxZ.PictureBoxZ();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageGenNMap = new System.Windows.Forms.TabPage();
            this.controlsListKernels = new ControlsList.ControlsList();
            this.pictureBoxZ2 = new PictureBoxZ.PictureBoxZ();
            this.buttonGenApply = new System.Windows.Forms.Button();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.checkBoxGenNormalize = new System.Windows.Forms.CheckBox();
            this.checkBoxGenInvY = new System.Windows.Forms.CheckBox();
            this.checkBoxGenInvX = new System.Windows.Forms.CheckBox();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.buttonSaveLocalizationFile = new System.Windows.Forms.Button();
            this.label_SettingsInterpolationMode = new System.Windows.Forms.Label();
            this.comboBoxSettingsInterpolationMode = new System.Windows.Forms.ComboBox();
            this.buttonSettingsSave = new System.Windows.Forms.Button();
            this.radioButtonSettingsPosG = new System.Windows.Forms.RadioButton();
            this.radioButtonSettingsNegG = new System.Windows.Forms.RadioButton();
            this.groupBoxInfo = new System.Windows.Forms.GroupBox();
            this.checkBox_IgnoreB = new System.Windows.Forms.CheckBox();
            this.label_RGB = new System.Windows.Forms.Label();
            this.label_X = new System.Windows.Forms.Label();
            this.label_Length = new System.Windows.Forms.Label();
            this.label_Y = new System.Windows.Forms.Label();
            this.label_Z = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStripIconMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageGenNMap.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.groupBoxInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.toolStripIconMenu, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxZ1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxInfo, 2, 1);
            this.tableLayoutPanel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.radioButtonBadPixels);
            this.panel1.Controls.Add(this.radioButtonRGB);
            this.panel1.Controls.Add(this.radioButtonB);
            this.panel1.Controls.Add(this.radioButtonG);
            this.panel1.Controls.Add(this.radioButtonR);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(519, 156);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 24);
            this.panel1.TabIndex = 3;
            // 
            // radioButtonBadPixels
            // 
            this.radioButtonBadPixels.AutoSize = true;
            this.radioButtonBadPixels.Location = new System.Drawing.Point(173, 3);
            this.radioButtonBadPixels.Name = "radioButtonBadPixels";
            this.radioButtonBadPixels.Size = new System.Drawing.Size(74, 17);
            this.radioButtonBadPixels.TabIndex = 4;
            this.radioButtonBadPixels.Text = "Bad Pixels";
            this.radioButtonBadPixels.UseVisualStyleBackColor = true;
            this.radioButtonBadPixels.CheckedChanged += new System.EventHandler(this.radioButtonRGB_CheckedChanged);
            // 
            // radioButtonRGB
            // 
            this.radioButtonRGB.AutoSize = true;
            this.radioButtonRGB.Checked = true;
            this.radioButtonRGB.Location = new System.Drawing.Point(3, 3);
            this.radioButtonRGB.Name = "radioButtonRGB";
            this.radioButtonRGB.Size = new System.Drawing.Size(48, 17);
            this.radioButtonRGB.TabIndex = 0;
            this.radioButtonRGB.TabStop = true;
            this.radioButtonRGB.Text = "RGB";
            this.radioButtonRGB.UseVisualStyleBackColor = true;
            this.radioButtonRGB.CheckedChanged += new System.EventHandler(this.radioButtonRGB_CheckedChanged);
            // 
            // radioButtonB
            // 
            this.radioButtonB.AutoSize = true;
            this.radioButtonB.Location = new System.Drawing.Point(135, 3);
            this.radioButtonB.Name = "radioButtonB";
            this.radioButtonB.Size = new System.Drawing.Size(32, 17);
            this.radioButtonB.TabIndex = 3;
            this.radioButtonB.Text = "B";
            this.radioButtonB.UseVisualStyleBackColor = true;
            this.radioButtonB.CheckedChanged += new System.EventHandler(this.radioButtonRGB_CheckedChanged);
            // 
            // radioButtonG
            // 
            this.radioButtonG.AutoSize = true;
            this.radioButtonG.Location = new System.Drawing.Point(96, 3);
            this.radioButtonG.Name = "radioButtonG";
            this.radioButtonG.Size = new System.Drawing.Size(33, 17);
            this.radioButtonG.TabIndex = 2;
            this.radioButtonG.Text = "G";
            this.radioButtonG.UseVisualStyleBackColor = true;
            this.radioButtonG.CheckedChanged += new System.EventHandler(this.radioButtonRGB_CheckedChanged);
            // 
            // radioButtonR
            // 
            this.radioButtonR.AutoSize = true;
            this.radioButtonR.Location = new System.Drawing.Point(57, 3);
            this.radioButtonR.Name = "radioButtonR";
            this.radioButtonR.Size = new System.Drawing.Size(33, 17);
            this.radioButtonR.TabIndex = 1;
            this.radioButtonR.Text = "R";
            this.radioButtonR.UseVisualStyleBackColor = true;
            this.radioButtonR.CheckedChanged += new System.EventHandler(this.radioButtonRGB_CheckedChanged);
            // 
            // toolStripIconMenu
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.toolStripIconMenu, 3);
            this.toolStripIconMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripIconMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOpen,
            this.toolStripButtonSaveAs,
            this.toolStripSeparator1,
            this.toolStripButtonUndo,
            this.toolStripButtonRedo,
            this.toolStripSeparator2,
            this.toolStripButtonCopy,
            this.toolStripButtonPaste,
            this.toolStripSeparator3,
            this.toolStripButtonInvR,
            this.toolStripButtonInvG,
            this.toolStripButtonInvB,
            this.toolStripSeparator4,
            this.toolStripButtonNormalize,
            this.toolStripSeparator5,
            this.toolStripButtonRestoreB,
            this.toolStripButtonClearB,
            this.toolStripSeparator6,
            this.toolStripButtonFlipRG,
            this.toolStripButtonFlipGB,
            this.toolStripButtonFlipBR,
            this.toolStripSeparator7,
            this.toolStripButtonRotate270,
            this.toolStripButtonRotate90,
            this.toolStripButtonRotate180,
            this.toolStripButtonFlipH,
            this.toolStripButtonFlipV,
            this.toolStripSeparator8,
            this.toolStripButtonHelp});
            this.toolStripIconMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripIconMenu.Name = "toolStripIconMenu";
            this.toolStripIconMenu.Size = new System.Drawing.Size(784, 25);
            this.toolStripIconMenu.TabIndex = 13;
            this.toolStripIconMenu.Text = "toolStrip1";
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpen.Image")));
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonOpen.Text = " (Ctrl + O)";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripButtonSaveAs
            // 
            this.toolStripButtonSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSaveAs.Image")));
            this.toolStripButtonSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveAs.Name = "toolStripButtonSaveAs";
            this.toolStripButtonSaveAs.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSaveAs.Text = " (Ctrl + S)";
            this.toolStripButtonSaveAs.Click += new System.EventHandler(this.toolStripButtonSaveAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonUndo
            // 
            this.toolStripButtonUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUndo.Enabled = false;
            this.toolStripButtonUndo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUndo.Image")));
            this.toolStripButtonUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUndo.Name = "toolStripButtonUndo";
            this.toolStripButtonUndo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUndo.Text = " (Ctrl + Z)";
            this.toolStripButtonUndo.Click += new System.EventHandler(this.toolStripButtonUndo_Click);
            // 
            // toolStripButtonRedo
            // 
            this.toolStripButtonRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRedo.Enabled = false;
            this.toolStripButtonRedo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRedo.Image")));
            this.toolStripButtonRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRedo.Name = "toolStripButtonRedo";
            this.toolStripButtonRedo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRedo.Text = " (Ctrl + Y)";
            this.toolStripButtonRedo.Click += new System.EventHandler(this.toolStripButtonRedo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonCopy
            // 
            this.toolStripButtonCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCopy.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCopy.Image")));
            this.toolStripButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCopy.Name = "toolStripButtonCopy";
            this.toolStripButtonCopy.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCopy.Text = " (Ctrl + C)";
            this.toolStripButtonCopy.Click += new System.EventHandler(this.toolStripButtonCopy_Click);
            // 
            // toolStripButtonPaste
            // 
            this.toolStripButtonPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPaste.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPaste.Image")));
            this.toolStripButtonPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaste.Name = "toolStripButtonPaste";
            this.toolStripButtonPaste.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonPaste.Text = " (Ctrl + V)";
            this.toolStripButtonPaste.Click += new System.EventHandler(this.toolStripButtonPaste_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonInvR
            // 
            this.toolStripButtonInvR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInvR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvR.Image")));
            this.toolStripButtonInvR.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvR.Name = "toolStripButtonInvR";
            this.toolStripButtonInvR.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInvR.Text = " (Ctrl + R)";
            this.toolStripButtonInvR.Click += new System.EventHandler(this.toolStripButtonInvR_Click);
            // 
            // toolStripButtonInvG
            // 
            this.toolStripButtonInvG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInvG.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvG.Image")));
            this.toolStripButtonInvG.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvG.Name = "toolStripButtonInvG";
            this.toolStripButtonInvG.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInvG.Text = " (Ctrl + G)";
            this.toolStripButtonInvG.Click += new System.EventHandler(this.toolStripButtonInvG_Click);
            // 
            // toolStripButtonInvB
            // 
            this.toolStripButtonInvB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInvB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvB.Image")));
            this.toolStripButtonInvB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvB.Name = "toolStripButtonInvB";
            this.toolStripButtonInvB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInvB.Text = " (Ctrl + B)";
            this.toolStripButtonInvB.Click += new System.EventHandler(this.toolStripButtonInvB_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonNormalize
            // 
            this.toolStripButtonNormalize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNormalize.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNormalize.Image")));
            this.toolStripButtonNormalize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNormalize.Name = "toolStripButtonNormalize";
            this.toolStripButtonNormalize.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonNormalize.Text = " (Ctrl + N)";
            this.toolStripButtonNormalize.Click += new System.EventHandler(this.toolStripButtonNormalize_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonRestoreB
            // 
            this.toolStripButtonRestoreB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRestoreB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRestoreB.Image")));
            this.toolStripButtonRestoreB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRestoreB.Name = "toolStripButtonRestoreB";
            this.toolStripButtonRestoreB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRestoreB.Text = " (Shift + B)";
            this.toolStripButtonRestoreB.Click += new System.EventHandler(this.toolStripButtonRestoreB_Click);
            // 
            // toolStripButtonClearB
            // 
            this.toolStripButtonClearB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClearB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClearB.Image")));
            this.toolStripButtonClearB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClearB.Name = "toolStripButtonClearB";
            this.toolStripButtonClearB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonClearB.Text = " (Alt + B)";
            this.toolStripButtonClearB.Click += new System.EventHandler(this.toolStripButtonClearB_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonFlipRG
            // 
            this.toolStripButtonFlipRG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipRG.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipRG.Image")));
            this.toolStripButtonFlipRG.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipRG.Name = "toolStripButtonFlipRG";
            this.toolStripButtonFlipRG.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipRG.Click += new System.EventHandler(this.toolStripButtonFlipRG_Click);
            // 
            // toolStripButtonFlipGB
            // 
            this.toolStripButtonFlipGB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipGB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipGB.Image")));
            this.toolStripButtonFlipGB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipGB.Name = "toolStripButtonFlipGB";
            this.toolStripButtonFlipGB.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipGB.Click += new System.EventHandler(this.toolStripButtonFlipGB_Click);
            // 
            // toolStripButtonFlipBR
            // 
            this.toolStripButtonFlipBR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipBR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipBR.Image")));
            this.toolStripButtonFlipBR.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipBR.Name = "toolStripButtonFlipBR";
            this.toolStripButtonFlipBR.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipBR.Click += new System.EventHandler(this.toolStripButtonFlipBR_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonRotate270
            // 
            this.toolStripButtonRotate270.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotate270.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotate270.Image")));
            this.toolStripButtonRotate270.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRotate270.Name = "toolStripButtonRotate270";
            this.toolStripButtonRotate270.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRotate270.Click += new System.EventHandler(this.toolStripButtonRotate270_Click);
            // 
            // toolStripButtonRotate90
            // 
            this.toolStripButtonRotate90.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotate90.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotate90.Image")));
            this.toolStripButtonRotate90.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRotate90.Name = "toolStripButtonRotate90";
            this.toolStripButtonRotate90.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRotate90.Click += new System.EventHandler(this.toolStripButtonRotate90_Click);
            // 
            // toolStripButtonRotate180
            // 
            this.toolStripButtonRotate180.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotate180.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotate180.Image")));
            this.toolStripButtonRotate180.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRotate180.Name = "toolStripButtonRotate180";
            this.toolStripButtonRotate180.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRotate180.Click += new System.EventHandler(this.toolStripButtonRotate180_Click);
            // 
            // toolStripButtonFlipH
            // 
            this.toolStripButtonFlipH.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipH.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipH.Image")));
            this.toolStripButtonFlipH.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipH.Name = "toolStripButtonFlipH";
            this.toolStripButtonFlipH.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipH.Click += new System.EventHandler(this.toolStripButtonFlipH_Click);
            // 
            // toolStripButtonFlipV
            // 
            this.toolStripButtonFlipV.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFlipV.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFlipV.Image")));
            this.toolStripButtonFlipV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFlipV.Name = "toolStripButtonFlipV";
            this.toolStripButtonFlipV.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFlipV.Click += new System.EventHandler(this.toolStripButtonFlipV_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonHelp.Text = " (F1)";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(519, 28);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(122, 122);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
            // 
            // pictureBoxZ1
            // 
            this.pictureBoxZ1.AllowDrop = true;
            this.pictureBoxZ1.BackColor = System.Drawing.Color.Gray;
            this.pictureBoxZ1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxZ1.BackgroundImage")));
            this.pictureBoxZ1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxZ1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxZ1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBoxZ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxZ1.Image = null;
            this.pictureBoxZ1.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            this.pictureBoxZ1.Location = new System.Drawing.Point(3, 28);
            this.pictureBoxZ1.Name = "pictureBoxZ1";
            this.pictureBoxZ1.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            this.tableLayoutPanel1.SetRowSpan(this.pictureBoxZ1, 3);
            this.pictureBoxZ1.Size = new System.Drawing.Size(510, 530);
            this.pictureBoxZ1.TabIndex = 11;
            this.pictureBoxZ1.ZoomCenterPoint = ((System.Drawing.PointF)(resources.GetObject("pictureBoxZ1.ZoomCenterPoint")));
            this.pictureBoxZ1.ZoomList = ((System.Collections.Generic.List<float>)(resources.GetObject("pictureBoxZ1.ZoomList")));
            this.pictureBoxZ1.ZoomListIndex = 0;
            this.pictureBoxZ1.DragDrop += new System.Windows.Forms.DragEventHandler(this.pictureBoxZ1_DragDrop);
            this.pictureBoxZ1.DragEnter += new System.Windows.Forms.DragEventHandler(this.pictureBoxZ1_DragEnter);
            this.pictureBoxZ1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxZ1_Paint);
            this.pictureBoxZ1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClickMove);
            this.pictureBoxZ1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClickMove);
            this.pictureBoxZ1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClickMove);
            this.pictureBoxZ1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxZ1_MouseClickMove);
            // 
            // tabControl1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tabControl1, 2);
            this.tabControl1.Controls.Add(this.tabPageGenNMap);
            this.tabControl1.Controls.Add(this.tabPageSettings);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(519, 186);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(262, 372);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPageGenNMap
            // 
            this.tabPageGenNMap.BackColor = System.Drawing.Color.White;
            this.tabPageGenNMap.Controls.Add(this.controlsListKernels);
            this.tabPageGenNMap.Controls.Add(this.pictureBoxZ2);
            this.tabPageGenNMap.Controls.Add(this.buttonGenApply);
            this.tabPageGenNMap.Controls.Add(this.buttonGenerate);
            this.tabPageGenNMap.Controls.Add(this.checkBoxGenNormalize);
            this.tabPageGenNMap.Controls.Add(this.checkBoxGenInvY);
            this.tabPageGenNMap.Controls.Add(this.checkBoxGenInvX);
            this.tabPageGenNMap.Location = new System.Drawing.Point(4, 22);
            this.tabPageGenNMap.Name = "tabPageGenNMap";
            this.tabPageGenNMap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGenNMap.Size = new System.Drawing.Size(254, 346);
            this.tabPageGenNMap.TabIndex = 2;
            this.tabPageGenNMap.Text = "Generate NormalMap";
            // 
            // controlsListKernels
            // 
            this.controlsListKernels.AutoScroll = true;
            this.controlsListKernels.AutoSize = true;
            this.controlsListKernels.BackColor = System.Drawing.SystemColors.Control;
            this.controlsListKernels.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.controlsListKernels.ButtonsAddVisible = true;
            this.controlsListKernels.ButtonsDeleteVisible = true;
            this.controlsListKernels.ButtonsUpDownVisible = true;
            this.controlsListKernels.ColorIcons = true;
            this.controlsListKernels.DefaultRowsEnabled = true;
            this.controlsListKernels.Elements = new System.Windows.Forms.Control[0];
            this.controlsListKernels.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.controlsListKernels.Location = new System.Drawing.Point(1, 4);
            this.controlsListKernels.Margin = new System.Windows.Forms.Padding(1);
            this.controlsListKernels.MaxElementsCount = 20;
            this.controlsListKernels.MinElementsCount = 1;
            this.controlsListKernels.MinimumSize = new System.Drawing.Size(32, 32);
            this.controlsListKernels.Name = "controlsListKernels";
            this.controlsListKernels.Size = new System.Drawing.Size(250, 123);
            this.controlsListKernels.TabIndex = 15;
            this.controlsListKernels.WrapContents = false;
            // 
            // pictureBoxZ2
            // 
            this.pictureBoxZ2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxZ2.BackgroundImage")));
            this.pictureBoxZ2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxZ2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxZ2.Image = null;
            this.pictureBoxZ2.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            this.pictureBoxZ2.Location = new System.Drawing.Point(6, 179);
            this.pictureBoxZ2.Name = "pictureBoxZ2";
            this.pictureBoxZ2.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            this.pictureBoxZ2.Size = new System.Drawing.Size(237, 161);
            this.pictureBoxZ2.TabIndex = 14;
            this.pictureBoxZ2.ZoomCenterPoint = ((System.Drawing.PointF)(resources.GetObject("pictureBoxZ2.ZoomCenterPoint")));
            this.pictureBoxZ2.ZoomList = ((System.Collections.Generic.List<float>)(resources.GetObject("pictureBoxZ2.ZoomList")));
            this.pictureBoxZ2.ZoomListIndex = -1;
            // 
            // buttonGenApply
            // 
            this.buttonGenApply.Enabled = false;
            this.buttonGenApply.Location = new System.Drawing.Point(199, 133);
            this.buttonGenApply.Name = "buttonGenApply";
            this.buttonGenApply.Size = new System.Drawing.Size(50, 40);
            this.buttonGenApply.TabIndex = 13;
            this.buttonGenApply.Text = "Apply";
            this.buttonGenApply.UseVisualStyleBackColor = true;
            this.buttonGenApply.Click += new System.EventHandler(this.buttonGenApply_Click);
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(133, 133);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(60, 40);
            this.buttonGenerate.TabIndex = 12;
            this.buttonGenerate.Text = "Generate";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // checkBoxGenNormalize
            // 
            this.checkBoxGenNormalize.AutoSize = true;
            this.checkBoxGenNormalize.Checked = true;
            this.checkBoxGenNormalize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGenNormalize.Location = new System.Drawing.Point(75, 133);
            this.checkBoxGenNormalize.Name = "checkBoxGenNormalize";
            this.checkBoxGenNormalize.Size = new System.Drawing.Size(72, 17);
            this.checkBoxGenNormalize.TabIndex = 7;
            this.checkBoxGenNormalize.Text = "Normalize";
            this.checkBoxGenNormalize.UseVisualStyleBackColor = true;
            // 
            // checkBoxGenInvY
            // 
            this.checkBoxGenInvY.AutoSize = true;
            this.checkBoxGenInvY.Location = new System.Drawing.Point(6, 156);
            this.checkBoxGenInvY.Name = "checkBoxGenInvY";
            this.checkBoxGenInvY.Size = new System.Drawing.Size(63, 17);
            this.checkBoxGenInvY.TabIndex = 4;
            this.checkBoxGenInvY.Text = "Invert Y";
            this.checkBoxGenInvY.UseVisualStyleBackColor = true;
            // 
            // checkBoxGenInvX
            // 
            this.checkBoxGenInvX.AutoSize = true;
            this.checkBoxGenInvX.Location = new System.Drawing.Point(6, 133);
            this.checkBoxGenInvX.Name = "checkBoxGenInvX";
            this.checkBoxGenInvX.Size = new System.Drawing.Size(63, 17);
            this.checkBoxGenInvX.TabIndex = 2;
            this.checkBoxGenInvX.Text = "Invert X";
            this.checkBoxGenInvX.UseVisualStyleBackColor = true;
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.buttonSaveLocalizationFile);
            this.tabPageSettings.Controls.Add(this.label_SettingsInterpolationMode);
            this.tabPageSettings.Controls.Add(this.comboBoxSettingsInterpolationMode);
            this.tabPageSettings.Controls.Add(this.buttonSettingsSave);
            this.tabPageSettings.Controls.Add(this.radioButtonSettingsPosG);
            this.tabPageSettings.Controls.Add(this.radioButtonSettingsNegG);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(254, 346);
            this.tabPageSettings.TabIndex = 1;
            this.tabPageSettings.Text = "Settings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // buttonSaveLocalizationFile
            // 
            this.buttonSaveLocalizationFile.Location = new System.Drawing.Point(109, 317);
            this.buttonSaveLocalizationFile.Name = "buttonSaveLocalizationFile";
            this.buttonSaveLocalizationFile.Size = new System.Drawing.Size(139, 23);
            this.buttonSaveLocalizationFile.TabIndex = 5;
            this.buttonSaveLocalizationFile.Text = "Save Localization File";
            this.buttonSaveLocalizationFile.UseVisualStyleBackColor = true;
            this.buttonSaveLocalizationFile.Visible = false;
            this.buttonSaveLocalizationFile.Click += new System.EventHandler(this.buttonSaveLocalizationFile_Click);
            // 
            // label_SettingsInterpolationMode
            // 
            this.label_SettingsInterpolationMode.Location = new System.Drawing.Point(6, 99);
            this.label_SettingsInterpolationMode.Name = "label_SettingsInterpolationMode";
            this.label_SettingsInterpolationMode.Size = new System.Drawing.Size(141, 21);
            this.label_SettingsInterpolationMode.TabIndex = 4;
            this.label_SettingsInterpolationMode.Text = "Viewer Interpolation Mode:";
            this.label_SettingsInterpolationMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxSettingsInterpolationMode
            // 
            this.comboBoxSettingsInterpolationMode.FormattingEnabled = true;
            this.comboBoxSettingsInterpolationMode.Location = new System.Drawing.Point(153, 99);
            this.comboBoxSettingsInterpolationMode.Name = "comboBoxSettingsInterpolationMode";
            this.comboBoxSettingsInterpolationMode.Size = new System.Drawing.Size(95, 21);
            this.comboBoxSettingsInterpolationMode.TabIndex = 3;
            this.comboBoxSettingsInterpolationMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxSettingsInterpolationMode_SelectedIndexChanged);
            // 
            // buttonSettingsSave
            // 
            this.buttonSettingsSave.Location = new System.Drawing.Point(67, 146);
            this.buttonSettingsSave.Name = "buttonSettingsSave";
            this.buttonSettingsSave.Size = new System.Drawing.Size(119, 32);
            this.buttonSettingsSave.TabIndex = 2;
            this.buttonSettingsSave.Text = "Save";
            this.buttonSettingsSave.UseVisualStyleBackColor = true;
            this.buttonSettingsSave.Click += new System.EventHandler(this.buttonSettingsSave_Click);
            // 
            // radioButtonSettingsPosG
            // 
            this.radioButtonSettingsPosG.Location = new System.Drawing.Point(6, 51);
            this.radioButtonSettingsPosG.Name = "radioButtonSettingsPosG";
            this.radioButtonSettingsPosG.Size = new System.Drawing.Size(230, 40);
            this.radioButtonSettingsPosG.TabIndex = 1;
            this.radioButtonSettingsPosG.Text = "Unity, Blender, Maya, Modo, Toolbag...\r\n[R+, G+, B+]";
            this.radioButtonSettingsPosG.UseVisualStyleBackColor = true;
            // 
            // radioButtonSettingsNegG
            // 
            this.radioButtonSettingsNegG.Checked = true;
            this.radioButtonSettingsNegG.Location = new System.Drawing.Point(6, 6);
            this.radioButtonSettingsNegG.Name = "radioButtonSettingsNegG";
            this.radioButtonSettingsNegG.Size = new System.Drawing.Size(230, 39);
            this.radioButtonSettingsNegG.TabIndex = 0;
            this.radioButtonSettingsNegG.TabStop = true;
            this.radioButtonSettingsNegG.Text = "Unreal Engine, CryENGINE,\r\nSource Engine, 3ds Max... [R+, G-, B+]";
            this.radioButtonSettingsNegG.UseVisualStyleBackColor = true;
            // 
            // groupBoxInfo
            // 
            this.groupBoxInfo.Controls.Add(this.checkBox_IgnoreB);
            this.groupBoxInfo.Controls.Add(this.label_RGB);
            this.groupBoxInfo.Controls.Add(this.label_X);
            this.groupBoxInfo.Controls.Add(this.label_Length);
            this.groupBoxInfo.Controls.Add(this.label_Y);
            this.groupBoxInfo.Controls.Add(this.label_Z);
            this.groupBoxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxInfo.Location = new System.Drawing.Point(647, 28);
            this.groupBoxInfo.Name = "groupBoxInfo";
            this.groupBoxInfo.Size = new System.Drawing.Size(134, 122);
            this.groupBoxInfo.TabIndex = 16;
            this.groupBoxInfo.TabStop = false;
            this.groupBoxInfo.Text = "Info";
            // 
            // checkBox_IgnoreB
            // 
            this.checkBox_IgnoreB.AutoSize = true;
            this.checkBox_IgnoreB.Location = new System.Drawing.Point(6, 96);
            this.checkBox_IgnoreB.Name = "checkBox_IgnoreB";
            this.checkBox_IgnoreB.Size = new System.Drawing.Size(108, 17);
            this.checkBox_IgnoreB.TabIndex = 7;
            this.checkBox_IgnoreB.Text = "Ignore B-Channel";
            this.checkBox_IgnoreB.UseVisualStyleBackColor = true;
            // 
            // label_RGB
            // 
            this.label_RGB.AutoSize = true;
            this.label_RGB.Location = new System.Drawing.Point(6, 19);
            this.label_RGB.Name = "label_RGB";
            this.label_RGB.Size = new System.Drawing.Size(33, 13);
            this.label_RGB.TabIndex = 5;
            this.label_RGB.Text = "RGB:";
            // 
            // label_X
            // 
            this.label_X.AutoSize = true;
            this.label_X.Location = new System.Drawing.Point(6, 32);
            this.label_X.Name = "label_X";
            this.label_X.Size = new System.Drawing.Size(17, 13);
            this.label_X.TabIndex = 6;
            this.label_X.Text = "X:";
            // 
            // label_Length
            // 
            this.label_Length.AutoSize = true;
            this.label_Length.Location = new System.Drawing.Point(6, 71);
            this.label_Length.Name = "label_Length";
            this.label_Length.Size = new System.Drawing.Size(43, 13);
            this.label_Length.TabIndex = 8;
            this.label_Length.Text = "Length:";
            // 
            // label_Y
            // 
            this.label_Y.AutoSize = true;
            this.label_Y.Location = new System.Drawing.Point(6, 45);
            this.label_Y.Name = "label_Y";
            this.label_Y.Size = new System.Drawing.Size(17, 13);
            this.label_Y.TabIndex = 9;
            this.label_Y.Text = "Y:";
            // 
            // label_Z
            // 
            this.label_Z.AutoSize = true;
            this.label_Z.Location = new System.Drawing.Point(6, 58);
            this.label_Z.Name = "label_Z";
            this.label_Z.Size = new System.Drawing.Size(17, 13);
            this.label_Z.TabIndex = 10;
            this.label_Z.Text = "Z:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = resources.GetString("openFileDialog1.Filter");
            this.openFileDialog1.FilterIndex = 11;
            this.openFileDialog1.SupportMultiDottedExtensions = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = resources.GetString("saveFileDialog1.Filter");
            this.saveFileDialog1.FilterIndex = 4;
            this.saveFileDialog1.SupportMultiDottedExtensions = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(430, 430);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NormalMap Debugger";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStripIconMenu.ResumeLayout(false);
            this.toolStripIconMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageGenNMap.ResumeLayout(false);
            this.tabPageGenNMap.PerformLayout();
            this.tabPageSettings.ResumeLayout(false);
            this.groupBoxInfo.ResumeLayout(false);
            this.groupBoxInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private PictureBoxZ.PictureBoxZ pictureBoxZ1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonBadPixels;
        private System.Windows.Forms.RadioButton radioButtonRGB;
        private System.Windows.Forms.RadioButton radioButtonB;
        private System.Windows.Forms.RadioButton radioButtonG;
        private System.Windows.Forms.RadioButton radioButtonR;
        private System.Windows.Forms.ToolStrip toolStripIconMenu;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveAs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButtonUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvR;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvG;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonNormalize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonRestoreB;
        private System.Windows.Forms.ToolStripButton toolStripButtonClearB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripButton toolStripButtonRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButtonCopy;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaste;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageGenNMap;
        private System.Windows.Forms.CheckBox checkBoxGenNormalize;
        private System.Windows.Forms.CheckBox checkBoxGenInvY;
        private System.Windows.Forms.CheckBox checkBoxGenInvX;
        private System.Windows.Forms.TabPage tabPageSettings;
        public System.Windows.Forms.RadioButton radioButtonSettingsPosG;
        public System.Windows.Forms.RadioButton radioButtonSettingsNegG;
        private System.Windows.Forms.GroupBox groupBoxInfo;
        private System.Windows.Forms.CheckBox checkBox_IgnoreB;
        private System.Windows.Forms.Label label_RGB;
        private System.Windows.Forms.Label label_X;
        private System.Windows.Forms.Label label_Length;
        private System.Windows.Forms.Label label_Y;
        private System.Windows.Forms.Label label_Z;
        private System.Windows.Forms.Button buttonSettingsSave;
        private System.Windows.Forms.Label label_SettingsInterpolationMode;
        private System.Windows.Forms.ComboBox comboBoxSettingsInterpolationMode;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Button buttonGenApply;
        private PictureBoxZ.PictureBoxZ pictureBoxZ2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipRG;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipGB;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipBR;
        private System.Windows.Forms.ToolStripButton toolStripButtonRotate270;
        private System.Windows.Forms.ToolStripButton toolStripButtonRotate90;
        private System.Windows.Forms.ToolStripButton toolStripButtonRotate180;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipH;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlipV;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.Button buttonSaveLocalizationFile;
        private ControlsList.ControlsList controlsListKernels;
    }
}

