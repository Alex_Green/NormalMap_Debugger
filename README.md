# NormalMap Debugger

## Description
1. NormalMap Debugger is a FREE, OpenSource program for debug NomalMap direction in any pixel of a NomalMap texture.
2. With NormalMap Debugger you can view and change NomalMap directions, generate and normalize NormalMap, restore and clear B-Channel.
3. Program use FreeImage.NET (http://freeimage.sourceforge.net/) - FreeImage is an Open Source library project for developers who would like to support a lot of popular graphics image formats.

## Link
* https://gitlab.com/Alex_Green/NormalMap_Debugger
* https://gitlab.com/Alex_Green/NormalMap_Debugger_OpenGL

## License:
**GNU GPL, FreeImage.NET licenses are GNU GPL or FIPL (GNU GPL used for this program).**

## Supported formats
**More formats are supported by FreeImage.NET only, with some formats program can work incorrectly! Better use one layer 8-bit RGB images without Alpha!**
1. Bitmap File (*.bmp)
2. Independent JPEG Group (\*.jpg\*.jpeg;\*.jif;\*.jpe)
3. Portable Network Graphics (*.png)
4. Truevision Targa files (\*.tga;\*.targa)
5. Tagged Image File Format (\*.tif;\*.tiff)
6. Adobe Photoshop (*.psd)
7. DirectDraw Surface (*.dds)
8. Graphics Interchange Format (*.gif)
9. High Dynamic Range (*.hdr)
10. OpenEXR format (*.exr)
and others...

## Author
Zelenskyi Alexandr (Зеленський Олександр) - ZAA93@yandex.ru