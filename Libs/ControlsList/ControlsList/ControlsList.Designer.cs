﻿namespace ControlsList
{
    partial class ControlsList
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlsList));
            this.BtnImgList = new System.Windows.Forms.ImageList(this.components);
            this.BtnImgList_Color = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // BtnImgList
            // 
            this.BtnImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("BtnImgList.ImageStream")));
            this.BtnImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.BtnImgList.Images.SetKeyName(0, "Add.png");
            this.BtnImgList.Images.SetKeyName(1, "Up.png");
            this.BtnImgList.Images.SetKeyName(2, "Down.png");
            this.BtnImgList.Images.SetKeyName(3, "Delete.png");
            // 
            // BtnImgList_Color
            // 
            this.BtnImgList_Color.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("BtnImgList_Color.ImageStream")));
            this.BtnImgList_Color.TransparentColor = System.Drawing.Color.Transparent;
            this.BtnImgList_Color.Images.SetKeyName(0, "Add_Color.png");
            this.BtnImgList_Color.Images.SetKeyName(1, "Up_Color.png");
            this.BtnImgList_Color.Images.SetKeyName(2, "Down_Color.png");
            this.BtnImgList_Color.Images.SetKeyName(3, "Delete_Color.png");
            // 
            // ControlsList
            // 
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.MinimumSize = new System.Drawing.Size(32, 32);
            this.Size = new System.Drawing.Size(128, 128);
            this.WrapContents = false;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList BtnImgList;
        private System.Windows.Forms.ImageList BtnImgList_Color;
    }
}
