﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace ControlsList
{
    public partial class ControlsList : FlowLayoutPanel
    {
        bool rowsEnabled = true;
        bool btnAddVisible = true;
        bool btnUpDownVisible = true;
        bool btnDeleteVisible = true;
        bool colorIcons = true;
        int minElementsCount = 0;
        int maxElementsCount = 0;

        public ControlsList()
        {
            InitializeComponent();
        }

        public bool DefaultRowsEnabled
        {
            get { return rowsEnabled; }
            set { rowsEnabled = value; }
        }

        public bool ButtonsAddVisible
        {
            get { return btnAddVisible; }
            set { btnAddVisible = value; }
        }

        public bool ButtonsUpDownVisible
        {
            get { return btnUpDownVisible; }
            set { btnUpDownVisible = value; }
        }

        public bool ButtonsDeleteVisible
        {
            get { return btnDeleteVisible; }
            set { btnDeleteVisible = value; }
        }

        public bool ColorIcons
        {
            get { return colorIcons; }
            set
            {
                colorIcons = value;
                ButtonsEnableAndIconsChange();
            }
        }

        [RefreshProperties(RefreshProperties.All)]
        public int MinElementsCount
        {
            get { return minElementsCount; }
            set
            {
                minElementsCount = Math.Max(0, value);

                if (maxElementsCount != 0)
                    if (maxElementsCount < minElementsCount)
                        maxElementsCount = minElementsCount;

                ButtonsEnableAndIconsChange();
            }
        }

        [RefreshProperties(RefreshProperties.All)]
        public int MaxElementsCount
        {
            get { return maxElementsCount; }
            set
            {
                maxElementsCount = Math.Max(0, value);

                if (maxElementsCount != 0 && maxElementsCount < minElementsCount)
                    minElementsCount = maxElementsCount;

                ButtonsEnableAndIconsChange();
            }
        }

        public Control[] Elements
        {
            get
            {
                Control[] CArr = new Control[Controls.Count];
                for (int i = 0; i < Controls.Count; i++)
                    CArr[i] = Controls[i].Controls[1];
                return CArr;
            }
            set
            {
                if (value != null)
                {
                    Controls.Clear();
                    for (int i = 0; i < value.Length; i++)
                        AddControl(value[i]);
                    OnResize(EventArgs.Empty);
                }
            }
        }

        #region Add Controls
        public delegate Control AddFunc();
        AddFunc addFunc = new AddFunc(() =>
        {
            return new Control();
        });

        /// <summary>
        /// Control call this function on every AddButton("+") click.
        /// </summary>
        /// <param name="ControlCreationFunc">Static function, must return <see cref="Control"/>.</param>
        public void SetButtonAddFunc(AddFunc ControlCreationFunc)
        {
            addFunc = ControlCreationFunc;
        }

        /// <summary>
        /// Add <see cref="Control"/> to list.
        /// </summary>
        /// <param name="control">Control, must have GUI (like <see cref="Button"/>, <see cref="Label"/>...).</param>
        /// <param name="ControlPosition">Zero index element insert pisition.</param>
        /// <param name="ButtonAdd_Visible">Button Add "+" visibility.</param>
        /// <param name="ButtonsUpDown_Visible">Buttons Up "↑", Down "↓" visibility.</param>
        /// <param name="ButtonDelete_Visible">Button Delete "x" visibility.</param>
        public void AddControl(Control control = null,
            int ControlPosition = -1,
            bool ButtonAdd_Visible = true,
            bool ButtonsUpDown_Visible = true,
            bool ButtonDelete_Visible = true)
        {
            if (control == null)
                control = addFunc.Invoke();

            TableLayoutPanel tlPanel = new TableLayoutPanel();
            CheckBox chBoxEnabled = new CheckBox();

            #region Buttons, Buttons Panel
            Button[] Buttons = new Button[4]; //Add, Up, Down, Delete
            TableLayoutPanel bPanel = new TableLayoutPanel();
            bPanel.AutoSize = true;
            bPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            bPanel.Anchor = AnchorStyles.None;
            bPanel.Padding = bPanel.Margin = new Padding(0);
            bPanel.RowCount = 1;
            bPanel.ColumnCount = Buttons.Length;

            bPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            bPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;

            for (int i = 0; i < Buttons.Length; i++)
            {
                Buttons[i] = new Button();
                Buttons[i].Name = String.Format("Button_", i);
                Buttons[i].AutoSizeMode = AutoSizeMode.GrowAndShrink;
                Buttons[i].AutoSize = true;
                Buttons[i].Text = String.Empty;
                Buttons[i].Margin = Buttons[i].Padding = new Padding();
                Buttons[i].ImageList = (colorIcons ? BtnImgList_Color : BtnImgList);
                Buttons[i].ImageIndex = i;

                bPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                bPanel.Controls.Add(Buttons[i], i, 0);
            }
            Buttons[0].Visible = ButtonAdd_Visible;
            Buttons[1].Visible = Buttons[2].Visible = ButtonsUpDown_Visible;
            Buttons[3].Visible = ButtonDelete_Visible;

            Buttons[0].Click += delegate //Add
            {
                AddControl(null, Controls.IndexOf(tlPanel) + 1, btnAddVisible, btnUpDownVisible, btnDeleteVisible);
                ButtonsEnableAndIconsChange();
            };

            Buttons[1].Click += delegate //Up
            {
                int i = Controls.IndexOf(tlPanel);
                if (i > 0)
                    Controls.SetChildIndex(tlPanel, i - 1);
                ButtonsEnableAndIconsChange();
            };

            Buttons[2].Click += delegate //Down
            {
                int i = Controls.IndexOf(tlPanel);
                if (i < Controls.Count - 1)
                    Controls.SetChildIndex(tlPanel, i + 1);
                ButtonsEnableAndIconsChange();
            };

            Buttons[3].Click += delegate //Delete
            {
                if (Controls.Count <= minElementsCount)
                    return;

                Controls.Remove(tlPanel);
                ButtonsEnableAndIconsChange();
                OnResize(EventArgs.Empty);
            };
            #endregion

            tlPanel.Enabled = rowsEnabled;
            tlPanel.RowCount = 1;
            tlPanel.ColumnCount = 3;

            tlPanel.Controls.Add(chBoxEnabled, 0, 0);
            tlPanel.Controls.Add(control, 1, 0);
            tlPanel.Controls.Add(bPanel, 2, 0);

            chBoxEnabled.Name = chBoxEnabled.Text = String.Empty;
            chBoxEnabled.Checked = true;
            chBoxEnabled.AutoSize = true;
            chBoxEnabled.Anchor = AnchorStyles.None;
            chBoxEnabled.CheckedChanged += delegate
            {
                control.Enabled = chBoxEnabled.Checked;
            };

            tlPanel.Padding = tlPanel.Margin = new Padding();
            tlPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tlPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            tlPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            tlPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            tlPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

            int MaxHeight = 0;
            foreach (Control C in tlPanel.Controls)
                MaxHeight = Math.Max(MaxHeight, C.Height);
            tlPanel.Size = new Size(ClientSize.Width, MaxHeight);

            Controls.Add(tlPanel);
            int CPos = (ControlPosition >= 0 && ControlPosition < Controls.Count ? ControlPosition : Controls.Count);
            Controls.SetChildIndex(tlPanel, CPos);

            ButtonsEnableAndIconsChange();
        }
        #endregion

        void ButtonsEnableAndIconsChange()
        {
            for (int i = 0; i < Controls.Count; i++)
            {
                ControlCollection RowsCs = Controls[i].Controls;
                ControlCollection ButtonsCs = RowsCs[RowsCs.Count - 1].Controls;

                for (int j = 0; j < ButtonsCs.Count - 1; j++)
                {
                    ((Button)(ButtonsCs[j])).ImageList = (colorIcons ? BtnImgList_Color : BtnImgList);
                    ButtonsCs[j].Enabled = true;
                }

                ButtonsCs[0].Enabled = (maxElementsCount == 0 || Controls.Count < maxElementsCount);
                ButtonsCs[1].Enabled = (i > 0);
                ButtonsCs[2].Enabled = (i < Controls.Count - 1);
                ButtonsCs[3].Enabled = (Controls.Count > minElementsCount);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            foreach (Control RowsC in Controls)
            {
                int MaxHeight = 0;
                foreach (Control C in RowsC.Controls)
                    MaxHeight = Math.Max(MaxHeight, C.Height);
                RowsC.Size = new Size(ClientSize.Width, MaxHeight);
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            OnResize(e);
        }
    }
}