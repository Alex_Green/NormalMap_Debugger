﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace NormalMap_Debugger
{
    static class Program
    {
        public static string[] Args;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Args = args;
            Application.EnableVisualStyles();
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture; //For fix parsing values like "0.5" and "0,5"
            Application.SetCompatibleTextRenderingDefault(false);
            DLLResolver.ResolveDLLsPatch();
            Application.Run(new Form1());
        }
    }
}
