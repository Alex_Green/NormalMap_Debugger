﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PictureBoxZ
{
    public partial class PictureBoxZ : UserControl
    {
        Image Image_Original = null;
        InterpolationMode Image_InterpolationMode = InterpolationMode.Default;
        PixelOffsetMode Image_PixelOffsetMode = PixelOffsetMode.Default;

        float Stretch_Zoom = 0f;
        int Zoom_Index = 0;
        List<float> Zoom_List = new List<float>();
        Point Mouse_LocationOld = new Point();
        PointF Zoom_CenterPoint = new PointF();

        #region Loading Circle Animation
        Timer LoadingCircleTimer = new Timer();
        RectangleF LoadingCircleRe = new RectangleF();
        int LoadingCirclePartN = 0;
        bool animLoading = false;
        bool animInvRotation = false;
        bool animInvVertical = false;
        int animInterval = 100;
        int loadingCircleDiametr = 48;
        int loadingCircleParts = 9;
        float loadingCircleLineWidth = 5f;
        float loadingCircleLineStart = 0.5f;
        float loadingCircleLineEnd = 1f;
        Color loadingCircleColor1 = Color.White;
        Color loadingCircleColor2 = Color.FromArgb(128, 0, 0, 0);
        LineCap loadingCircleLineCapStart = LineCap.Round;
        LineCap loadingCircleLineCapEnd = LineCap.Round;
        #endregion

        public PictureBoxZ()
        {
            InitializeComponent();
            MouseWheel += new MouseEventHandler(PictureBoxZ_MouseWheel);

            LoadingCircleTimer.Interval = animInterval;
            pictureBoxAnim_Resize(null, EventArgs.Empty);
            LoadingCircleTimer.Enabled = animLoading;
        }

        #region Loading Circle Animation
        /// <summary>
        /// Is background "Loading Circle Animation" enabled?
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Is background \"Loading Circle Animation\" enabled?")]
        [DefaultValue(false)]
        public bool LoadingCircleEnabled
        {
            get { return animLoading; }
            set
            {
                animLoading = value;

                if (animLoading)
                {
                    LoadingCircleTimer.Tick += LoadingCircleTimer_Tick;
                    LoadingCirclePartN = 0;
                }
                else
                    LoadingCircleTimer.Tick -= LoadingCircleTimer_Tick;

                LoadingCircleTimer.Enabled = pictureBoxAnim.Visible = animLoading;
            }
        }

        /// <summary>
        /// Inverse rotation (if false - Left to Right)?
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Inverse rotation (if false - Left to Right)?")]
        [DefaultValue(false)]
        public bool LoadingCircleInvRotation
        {
            get { return animInvRotation; }
            set
            {
                animInvRotation = value;
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// Inverse vertical layout?
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Inverse vertical layout?")]
        [DefaultValue(false)]
        public bool LoadingCircleInvVertical
        {
            get { return animInvVertical; }
            set
            {
                animInvVertical = value;
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// Animation timer interval, ms. Default = 100.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Animation timer interval, ms. Default = 100.")]
        [DefaultValue(100)]
        public int LoadingCircleAnimInterval
        {
            get { return animInterval; }
            set { LoadingCircleTimer.Interval = animInterval = Math.Max(0, value); }
        }

        /// <summary>
        /// Maximum Diameter of a Loading Circle.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Maximum Diameter of a Loading Circle.")]
        [DefaultValue(48)]
        public int LoadingCircleDiametr
        {
            get { return loadingCircleDiametr; }
            set
            {
                loadingCircleDiametr = Math.Max(0, value);
                pictureBoxAnim_Resize(null, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Number of parts (lines) in Loading Circle. Minimum = 2.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Number of parts (lines) in circle. Minimum = 2.")]
        [DefaultValue(9)]
        public int LoadingCircleParts
        {
            get { return loadingCircleParts; }
            set
            {
                loadingCircleParts = Math.Max(2, value);
                LoadingCirclePartN = 0;
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// Loading Circle Lines width.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Loading Circle Lines width.")]
        [DefaultValue(5f)]
        public float LoadingCircleLineWidth
        {
            get { return loadingCircleLineWidth; }
            set
            {
                loadingCircleLineWidth = Math.Max(0f, value);
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// Coeficient in range [0..1] for setting or getting Line Start from circle center.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Coeficient in range [0..1] for setting or getting Line Start from circle center.")]
        [DefaultValue(0.5f)]
        public float LoadingCircleLineStart
        {
            get { return loadingCircleLineStart; }
            set
            {
                loadingCircleLineStart = Math.Max(0f, Math.Min(1f, value));
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// Coeficient in range [0..1] for setting or getting Line End from circle center.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Coeficient in range [0..1] for setting or getting Line End from circle center.")]
        [DefaultValue(1f)]
        public float LoadingCircleLineEnd
        {
            get { return loadingCircleLineEnd; }
            set
            {
                loadingCircleLineEnd = Math.Max(0f, Math.Min(1f, value));
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// First color. Must have Alpha = 255.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("First color. Must (or can) have Alpha = 255.")]
        [DefaultValue(typeof(Color), "White")]
        public Color LoadingCircleColor1
        {
            get { return loadingCircleColor1; }
            set
            {
                loadingCircleColor1 = value;
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// Second color. Must (or can) have Alpha smaller then 255.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("Second color. Must (or can) have Alpha smaller then 255.")]
        [DefaultValue(typeof(Color), "128, 0, 0, 0")]
        public Color LoadingCircleColor2
        {
            get { return loadingCircleColor2; }
            set
            {
                loadingCircleColor2 = value;
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// LineCap of line starts. Default = <see cref="LineCap.Round"/>.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("LineCap of line starts. Default = \"" + nameof(LineCap.Round) + "\".")]
        [DefaultValue(LineCap.Round)]
        public LineCap LoadingCircleLineCapStart
        {
            get { return loadingCircleLineCapStart; }
            set
            {
                loadingCircleLineCapStart = value;
                pictureBoxAnim.Invalidate();
            }
        }

        /// <summary>
        /// LineCap of line ends. Default = <see cref="LineCap.Round"/>.
        /// </summary>
        [Category("Loading Circle Animation")]
        [Description("LineCap of line ends. Default = \"" + nameof(LineCap.Round) + "\".")]
        [DefaultValue(LineCap.Round)]
        public LineCap LoadingCircleLineCapEnd
        {
            get { return loadingCircleLineCapEnd; }
            set
            {
                loadingCircleLineCapEnd = value;
                pictureBoxAnim.Invalidate();
            }
        }

        private void LoadingCircleTimer_Tick(object sender, EventArgs e)
        {
            LoadingCirclePartN = (LoadingCirclePartN + 1) % loadingCircleParts;
            pictureBoxAnim.Invalidate();
        }

        private void pictureBoxAnim_Resize(object sender, EventArgs e)
        {
            float MinSide = Math.Min(ClientSize.Width, ClientSize.Height);
            float CircleSideSize = Math.Min(MinSide, loadingCircleDiametr);
            LoadingCircleRe.X = (ClientSize.Width - CircleSideSize) * 0.5f;
            LoadingCircleRe.Y = (ClientSize.Height - CircleSideSize) * 0.5f;
            LoadingCircleRe.Width = LoadingCircleRe.Height = CircleSideSize;
            pictureBoxAnim.Invalidate();
        }

        private void pictureBoxAnim_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            using (Pen LinePen = new Pen(loadingCircleColor1, loadingCircleLineWidth))
            {
                PointF PStart = new PointF();
                PointF PEnd = new PointF();
                PointF CenterPoint = new PointF(
                    LoadingCircleRe.X + LoadingCircleRe.Width * 0.5f,
                    LoadingCircleRe.Y + LoadingCircleRe.Height * 0.5f);

                for (int i = 0; i < loadingCircleParts; i++)
                {
                    float ColorCoef = (i + 1 + LoadingCirclePartN) % loadingCircleParts / (float)(loadingCircleParts - 1);

                    LinePen.Color = Lerp(loadingCircleColor1, loadingCircleColor2, ColorCoef);
                    LinePen.StartCap = loadingCircleLineCapStart;
                    LinePen.EndCap = loadingCircleLineCapEnd;

                    double SinCosIn = i * 2 * Math.PI / loadingCircleParts;
                    float SinCoef = (float)Math.Sin(SinCosIn);
                    float CosCoef = (float)Math.Cos(SinCosIn);

                    PStart = PEnd = CenterPoint;
                    float HalfReSize = LoadingCircleRe.Width * 0.5f;
                    float P1_Scaled = loadingCircleLineStart * HalfReSize;
                    float P2_Scaled = loadingCircleLineEnd * HalfReSize;
                    PStart.X += (animInvRotation ^ animInvVertical ? SinCoef * P1_Scaled : -SinCoef * P1_Scaled);
                    PStart.Y += (animInvVertical ? CosCoef * P1_Scaled : -CosCoef * P1_Scaled);
                    PEnd.X += (animInvRotation ^ animInvVertical ? SinCoef * P2_Scaled : -SinCoef * P2_Scaled);
                    PEnd.Y += (animInvVertical ? CosCoef * P2_Scaled : -CosCoef * P2_Scaled);

                    if (loadingCircleLineStart == loadingCircleLineEnd)
                    {
                        using (SolidBrush SBrush = new SolidBrush(LinePen.Color))
                        {
                            float HalfLW = loadingCircleLineWidth * 0.5f;
                            e.Graphics.FillEllipse(SBrush, PStart.X - HalfLW, PStart.Y - HalfLW,
                                loadingCircleLineWidth, loadingCircleLineWidth);
                        }
                    }
                    else
                        e.Graphics.DrawLine(LinePen, PStart, PEnd);
                }
            }
        }

        Color Lerp(Color C1, Color C2, float Value)
        {
            return Color.FromArgb(
                (int)Math.Round(C1.A + Value * (C2.A - C1.A)),
                (int)Math.Round(C1.R + Value * (C2.R - C1.R)),
                (int)Math.Round(C1.G + Value * (C2.G - C1.G)),
                (int)Math.Round(C1.B + Value * (C2.B - C1.B)));
        }
        #endregion

        private void PictureBoxZ_Resize(object sender, EventArgs e)
        {
            ZoomUpdate(true);
            pictureBoxAnim_Resize(null, EventArgs.Empty);
        }

        private void PictureBoxZ_MouseEnter(object sender, EventArgs e)
        {
            if (!Focused)
                Focus(); //Enable focus for MouseWheel
        }

        private void PictureBoxZ_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
                Mouse_LocationOld = e.Location;
        }

        private void PictureBoxZ_MouseMove_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
                MoveImage(e.Location);
        }

        private void PictureBoxZ_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (Zoom_Index < Zoom_List.Count - 1)
                    Zoom_Index++;
            }
            else if (e.Delta < 0)
            {
                if (Zoom_Index > 0)
                    Zoom_Index--;
            }

            MoveImage(Mouse_LocationOld);
        }

        private void MoveImage(Point NewMouseCoords)
        {
            if (ClientSize.Width == 0 || ClientSize.Height == 0)
                return;

            Bitmap NewImage = new Bitmap(ClientSize.Width, ClientSize.Height);

            #region Draw Image
            if (Image_Original != null && ClientSize.Width > 0 && ClientSize.Height > 0)
            {
                using (Graphics G = Graphics.FromImage(NewImage))
                {
                    G.InterpolationMode = Image_InterpolationMode;
                    G.PixelOffsetMode = Image_PixelOffsetMode;

                    #region Zoom_CenterPoint
                    Zoom_CenterPoint = new PointF(
                        Zoom_CenterPoint.X - (NewMouseCoords.X - Mouse_LocationOld.X) / GetZoom,
                        Zoom_CenterPoint.Y - (NewMouseCoords.Y - Mouse_LocationOld.Y) / GetZoom);
                    Mouse_LocationOld = NewMouseCoords;

                    float WidthZ = ClientSize.Width / GetZoom;
                    float HeightZ = ClientSize.Height / GetZoom;
                    float Half_WidthZ = WidthZ * 0.5f;
                    float Half_HeightZ = HeightZ * 0.5f;

                    if (Image_Original.Width > WidthZ)
                    {
                        if (Zoom_CenterPoint.X - Half_WidthZ < 0f)
                            Zoom_CenterPoint.X = Half_WidthZ;

                        if (Zoom_CenterPoint.X + Half_WidthZ > Image_Original.Width)
                            Zoom_CenterPoint.X = Image_Original.Width - Half_WidthZ;
                    }
                    else
                        Zoom_CenterPoint.X = Image_Original.Width * 0.5f;

                    if (Image_Original.Height > HeightZ)
                    {
                        if (Zoom_CenterPoint.Y - Half_HeightZ < 0f)
                            Zoom_CenterPoint.Y = Half_HeightZ;

                        if (Zoom_CenterPoint.Y + Half_HeightZ > Image_Original.Height)
                            Zoom_CenterPoint.Y = Image_Original.Height - Half_HeightZ;
                    }
                    else
                        Zoom_CenterPoint.Y = Image_Original.Height * 0.5f;
                    #endregion

                    G.DrawImage(Image_Original,
                        ClientRectangle,
                        new RectangleF(
                            Zoom_CenterPoint.X - Half_WidthZ,
                            Zoom_CenterPoint.Y - Half_HeightZ,
                            WidthZ,
                            HeightZ),
                        GraphicsUnit.Pixel);
                }
            }
            #endregion

            if (base.BackgroundImage != null)
                base.BackgroundImage.Dispose();

            base.BackgroundImage = NewImage;
        }

        #region Zoom
        [Category("Zoom")]
        public PointF ZoomCenterPoint
        {
            get
            {
                if (Image_Original == null)
                    return PointF.Empty;

                return new PointF(Zoom_CenterPoint.X / Image_Original.Width, Zoom_CenterPoint.Y / Image_Original.Height);
            }
            set
            {
                if (Image_Original == null)
                    return;

                Zoom_CenterPoint.X = value.X * Image_Original.Width;
                Zoom_CenterPoint.Y = value.Y * Image_Original.Height;
                MoveImage(Mouse_LocationOld);
            }
        }

        [Category("Zoom")]
        public List<float> ZoomList
        {
            get { return Zoom_List; }
            set
            {
                Zoom_List = value;

                if (Zoom_Index > Zoom_List.Count - 1)
                    Zoom_Index = Zoom_List.Count - 1;
            }
        }

        [Category("Zoom")]
        public int ZoomListIndex
        {
            get { return Zoom_Index; }
            set
            {
                if (value < 0)
                    Zoom_Index = 0;
                else if (value > Zoom_List.Count - 1)
                    Zoom_Index = Zoom_List.Count - 1;
                else
                    Zoom_Index = value;
            }
        }

        [Category("Zoom")]
        public float GetZoom
        {
            get
            {
                return Zoom_List[Zoom_Index];
            }
        }

        [Category("Zoom")]
        public float GetStretchZoom
        {
            get { return Stretch_Zoom; }
        }

        [Category("Zoom")]
        public void ZoomUpdate(bool RestoreLastZoom = false)
        {
            if (Image_Original != null)
            {
                float MinZoom = 0.25f,
                    MaxZoom = 32f,
                    LastZoom = 1f;

                if (RestoreLastZoom && Zoom_List.Count > Zoom_Index && Zoom_Index >= 0)  // Save Old Zoom!
                    LastZoom = Zoom_List[Zoom_Index];

                Zoom_List.Clear();
                Zoom_Index = 0;

                // 1, 2, 4, 8 ... MaxZoom
                //for (float i = 1f; i <= MaxZoom; i *= 2f)
                //    ZoomList.Add(i);
                for (float i = 1f; i <= MaxZoom; i *= 2f)
                    Zoom_List.Add(i);

                // 0.5, 0.25, 0.125 ... MinZoom
                for (float i = 0.5f; i >= MinZoom; i *= 0.5f)
                    Zoom_List.Insert(0, i);

                // StretchZoom
                float DeltaW = ClientSize.Width / (float)Image_Original.Width;
                float DeltaH = ClientSize.Height / (float)Image_Original.Height;

                if (DeltaW > DeltaH)
                    Stretch_Zoom = DeltaH;
                else
                    Stretch_Zoom = DeltaW;

                Zoom_List.Add(Stretch_Zoom);
                Zoom_List.Sort();

                // Restore Last Zoom
                if (RestoreLastZoom)
                {
                    int MinDeltaIndex = 0;
                    float MinDelta = Zoom_List[MinDeltaIndex];
                    for (int i = 1; i < Zoom_List.Count; i++)
                    {
                        float Delta = Math.Abs(Zoom_List[i] - LastZoom);
                        if (Delta < MinDelta)
                        {
                            MinDeltaIndex = i;
                            MinDelta = Delta;
                        }
                    }
                    Zoom_Index = MinDeltaIndex;
                }
                else
                    Zoom_Index = Zoom_List.IndexOf(Stretch_Zoom);
            }

            MoveImage(Mouse_LocationOld);
        }
        #endregion

        [Category("Appearance")]
        public Image Image
        {
            get { return Image_Original; }
            set
            {
                if (value == null)
                {
                    if (Image_Original != null)
                    {
                        Image_Original.Dispose();
                        Image_Original = null;
                    }
                }
                else
                {
                    Image_Original = (Image)value.Clone();
                    Zoom_CenterPoint = new PointF(Image_Original.Width * 0.5f, Image_Original.Height * 0.5f);
                }
                ZoomUpdate(false);
            }
        }

        [Category("Appearance")]
        [DefaultValue(PixelOffsetMode.Default)]
        public InterpolationMode InterpolationMode
        {
            get { return Image_InterpolationMode; }
            set
            {
                if (value == InterpolationMode.Invalid)
                    value = InterpolationMode.Default;

                Image_InterpolationMode = value;
                MoveImage(Mouse_LocationOld);
            }
        }

        [Category("Appearance")]
        [DefaultValue(PixelOffsetMode.Default)]
        public PixelOffsetMode PixelOffsetMode
        {
            get { return Image_PixelOffsetMode; }
            set
            {
                if (value == PixelOffsetMode.Invalid)
                    value = PixelOffsetMode.Default;

                Image_PixelOffsetMode = value;
                MoveImage(Mouse_LocationOld);
            }
        }
    }
}