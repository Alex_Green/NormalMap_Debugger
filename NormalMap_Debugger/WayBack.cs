﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace NormalMap_Debugger
{
    /// <summary>
    /// WayBack class can provide <see cref="Undo(T)"/>/<see cref="Redo"/> functions.
    /// </summary>
    /// <typeparam name="T">Type of data for <see cref="Undo(T)"/>/<see cref="Redo"/>.</typeparam>
    public class WayBack<T> : IDisposable where T : IDisposable
    {
        List<T> States = new List<T>();
        int L_NextIndex = 0;
        uint maxCapacity = 1;
        public ToolStripButton UndoButton, RedoButton;

        public WayBack()
        {
        }

        public WayBack(uint MaxCapacity)
        {
            this.MaxCapacity = MaxCapacity;
        }

        public WayBack(ToolStripButton Button_Undo, ToolStripButton Button_Redo)
        {
            UndoButton = Button_Undo;
            RedoButton = Button_Redo;
        }

        public WayBack(uint MaxCapacity, ToolStripButton Button_Undo, ToolStripButton Button_Redo)
        {
            this.MaxCapacity = MaxCapacity;
            UndoButton = Button_Undo;
            RedoButton = Button_Redo;
        }

        /// <summary>
        /// Max states (Min = 1).
        /// </summary>
        public uint MaxCapacity
        {
            get { return maxCapacity; }
            set
            {
                if (value < 1)
                    throw new ArgumentOutOfRangeException();
                else
                    maxCapacity = value;
            }
        }

        bool ButtonUndo_Enabled
        {
            set
            {
                if (UndoButton != null)
                    UndoButton.Enabled = value;
            }
        }

        bool ButtonRedo_Enabled
        {
            set
            {
                if (RedoButton != null)
                    RedoButton.Enabled = value;
            }
        }

        /// <summary>
        /// Undo changes. Get last old state of object.
        /// </summary>
        /// <param name="CurrentState">Current state of object.</param>
        /// <returns>Old state of object.</returns>
        public T Undo(T CurrentState)
        {
            if (L_NextIndex >= States.Count)
                States.Add(CurrentState);

            if (L_NextIndex > maxCapacity)
            {
                Garbage.Clear(States[0]);
                States.RemoveAt(0);
                L_NextIndex--;
            }

            L_NextIndex--;
            ButtonRedo_Enabled = true;
            if (L_NextIndex <= 0)
                ButtonUndo_Enabled = false;

            return States[L_NextIndex];
        }

        /// <summary>
        /// Redo changes. Gets last state of object before using <see cref="Undo(T)"/>.
        /// </summary>
        /// <returns>State of object before using <see cref="Undo(T)"/>.</returns>
        public T Redo()
        {
            L_NextIndex++;
            ButtonUndo_Enabled = true;
            if (L_NextIndex >= States.Count - 1)
                ButtonRedo_Enabled = false;

            return States[L_NextIndex];
        }

        /// <summary>
        /// Save current state of object.
        /// </summary>
        /// <param name="CurrentState">Current state of object.</param>
        public void AddState(T CurrentState)
        {
            ButtonUndo_Enabled = true;
            ButtonRedo_Enabled = false;
            
            if (L_NextIndex > maxCapacity)
            {
                Garbage.Clear(States[0]);
                States.RemoveAt(0);
                L_NextIndex--;
            }

            Garbage.Clear(States, L_NextIndex, States.Count - L_NextIndex);
            States.RemoveRange(L_NextIndex, States.Count - L_NextIndex);
            L_NextIndex++;

            States.Add(CurrentState);
        }

        /// <summary>
        /// Clear all states and resources.
        /// </summary>
        public void ClearStates()
        {
            ButtonUndo_Enabled = false;
            ButtonRedo_Enabled = false;
            Garbage.Clear(States);
            States.Clear();
            L_NextIndex = 0;
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                    ClearStates();
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~WayBack() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
